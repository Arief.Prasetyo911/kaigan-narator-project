/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/core/chat-and-loop.js":
/*!********************************************!*\
  !*** ./resources/js/core/chat-and-loop.js ***!
  \********************************************/
/***/ (() => {

$(document).ready(function () {
  //loop function
  function firstLoop(firstID, dataPosition, chatData, author, lastIndex) {
    //first loop
    if (firstID == lastIndex) {
      console.log('near end of chat (1)');

      var _loop = function _loop(aa) {
        // console.log(aa);
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          dataPosition = chatData[aa]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container"); //play sound

          playNotificationSound();
          console.log("loop-1"); // console.log('pos loop 1',dataPosition);

          if (aa == firstID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").addClass("displayFlex");
                $(".offline").addClass('hide');
                $(".choices").css('display', 'block');
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + firstID);
                $(".control_online").addClass('control_1st');
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var aa = 0; aa < lastIndex; aa++) {
        _loop(aa);
      }
    } else {
      console.log('continue (1)');

      var _loop2 = function _loop2(a) {
        // console.log('a',a );
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          dataPosition = chatData[a]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container"); //play sound

          playNotificationSound();
          console.log("loop-1");

          if (a == firstID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").addClass("displayFlex");
                $(".offline").addClass('hide');
                $(".choices").css('display', 'block');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".control").addClass('control_1st');
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var a = 0; a < firstID; a++) {
        _loop2(a);
      }
    }
  }

  function secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex) {
    if (secondID == lastIndex) {
      console.log('near end of chat (2)');

      var _loop3 = function _loop3(bb) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[bb]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-2");

          if (bb == secondID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").addClass("displayFlex").removeClass('hide');
                $(".offline").addClass('hide').removeClass("displayFlex");
                $(".choices").removeClass('hide').addClass('displayBlock');
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + secondID);
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var bb = firstID; bb < lastIndex; bb++) {
        _loop3(bb);
      }
    } else {
      console.log('continue (2)');

      var _loop4 = function _loop4(b) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[b]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-2");
          var chatHeight = $(".justify-content-start").height();

          if (b == secondID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              $(".chat-box").addClass('move-top');
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").addClass("displayFlex").removeClass('hide');
                $(".offline").addClass('hide').removeClass("displayFlex");
                $(".choices").removeClass('hide').addClass('block', function () {
                  $(".chat-box").fadeIn().animate({
                    top: '-26%'
                  }, 500);
                });
                $(".opt-btn").addClass('option_' + dataPosition.id);
              });
              chatHeightFunc(chatHeight); // console.log('action button fade in');

              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var b = firstID; b < secondID; b++) {
        _loop4(b);
      }
    }
  }

  function thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex) {
    if (thirdID == lastIndex) {
      console.log('near end of chat (3)');

      var _loop5 = function _loop5(cc) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[cc]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-3");

          if (cc == thirdID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + thirdID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var cc = secondID; cc < lastIndex; cc++) {
        _loop5(cc);
      }
    } else {
      console.log('continue (3)');

      var _loop6 = function _loop6(c) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[c]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-3");

          if (c == thirdID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide").addClass('displayBlock');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var c = secondID; c < thirdID; c++) {
        _loop6(c);
      }
    }
  }

  function fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex) {
    if (fourthID == lastIndex) {
      console.log('near end of chat (4)');

      var _loop7 = function _loop7(dd) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[dd]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log('loop-4');

          if (dd == fourthID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + fourthID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var dd = thirdID; dd < lastIndex; dd++) {
        _loop7(dd);
      }
    } else {
      console.log('continue (4)');

      var _loop8 = function _loop8(d) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[d]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log('loop-4');

          if (d == fourthID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide").addClass('displayBlock');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var d = thirdID; d < fourthID; d++) {
        _loop8(d);
      }
    }
  }

  function fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex) {
    if (fivethID == lastIndex) {
      console.log('near end of chat (5)');

      var _loop9 = function _loop9(ee) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[ee]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-5");

          if (ee == fivethID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + fivethID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var ee = fourthID; ee < lastIndex; ee++) {
        _loop9(ee);
      }
    } else {
      console.log('continue (5)');

      var _loop10 = function _loop10(e) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[e]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-5");

          if (e == fivethID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide").addClass('displayBlock');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var e = fourthID; e < fivethID; e++) {
        _loop10(e);
      }
    }
  }

  function sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex) {
    if (sixthID == lastIndex) {
      console.log('near end of chat (6)');

      var _loop11 = function _loop11(ee) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[ee]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-6");

          if (ee == sixthID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + sixthID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayFlex');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var ee = fivethID; ee < lastIndex; ee++) {
        _loop11(ee);
      }
    } else {
      console.log('continue (6)');

      var _loop12 = function _loop12(e) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[e]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-6");

          if (e == sixthID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").addClass('hide').removeClass('displayFlex');
                $(".choices").removeClass("hide").addClass('displayFlex').addClass("displayBlock");
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayFlex');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var e = fivethID; e < sixthID; e++) {
        _loop12(e);
      }
    }
  }

  function seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex) {
    if (seventhID == lastIndex) {
      console.log('near end of chat (7)');

      var _loop13 = function _loop13(ff) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[ff]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-7");

          if (ff == seventhID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + seventhID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var ff = sixthID; ff < lastIndex; ff++) {
        _loop13(ff);
      }
    } else {
      console.log('continue (7)');

      var _loop14 = function _loop14(f) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[f]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-7");

          if (f == seventhID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").addClass('hide').removeClass('displayFlex');
                $(".choices").removeClass("hide").addClass('displayFlex');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var f = sixthID; f < seventhID; f++) {
        _loop14(f);
      }
    }
  }

  function eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex) {
    if (eighthID == lastIndex) {
      console.log('near end of chat (8)');

      var _loop15 = function _loop15(ff) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[ff]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-8");

          if (ff == eighthID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + eighthID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var ff = seventhID; ff < lastIndex; ff++) {
        _loop15(ff);
      }
    } else {
      console.log('continue (8)');

      var _loop16 = function _loop16(f) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[f]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-8");

          if (f == eighthID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").addClass('hide').removeClass('displayFlex');
                $(".choices").removeClass("hide").addClass('displayFlex');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
            });
          } //call the function


          scrollChat();
        });
      };

      for (var f = seventhID; f < eighthID; f++) {
        _loop16(f);
      }
    }
  }

  function ninethLoop(eighthID, ninethID, dataPosition, chatData, author, lastIndex) {
    if (ninethID == lastIndex) {
      console.log('near end of chat (9)');

      var _loop17 = function _loop17(ff) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[ff]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-9 (near end)");

          if (ff == ninethID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").removeClass('displayFlex').addClass('hide');
                $(".choices").removeClass("hide");
                $(".opt-btn").addClass('option_' + dataPosition.id).addClass('optionEnd_' + ninethID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
              console.log('option 9 appear (near end)');
            });
          } //call the function


          scrollChat();
        });
      };

      for (var ff = eighthID; ff < lastIndex; ff++) {
        _loop17(ff);
      }
    } else {
      console.log('continue (9)');

      var _loop18 = function _loop18(f) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[f]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-9");

          if (f == ninethID - 1) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").addClass('hide').removeClass('displayFlex');
                $(".choices").removeClass("hide").addClass('displayFlex');
                $(".opt-btn").addClass('option_' + dataPosition.id);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayBlock');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
              console.log('option 9 appear');
            });
          } //call the function


          scrollChat();
        });
      };

      for (var f = eighthID; f < ninethID; f++) {
        _loop18(f);
      }
    }
  }

  function tenthLoop(ninethID, tenthID, dataPosition, chatData, author, lastIndex, chatLength) {
    if (tenthID == lastIndex) {
      console.log('end of chat (10)');

      var _loop19 = function _loop19(ff) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
          var dataPosition = chatData[ff]; // append chat

          var appendQuestion = "\n                    <div class=\"justify-content-start\">\n                        <div class=\"col-12 pl-0 pr-0\">\n                            <div class=\"message-wrapper\">\n                                <div class=\"message\">\n                                    <div class=\"text\">\n                                        <span class=\"player-name\">" + author + "</span><br>\n                                        <p class=\"mt-0 mb-0 message\">\n                                            " + dataPosition.message + "\n                                        </p>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    ";
          var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
          playNotificationSound();
          console.log("loop-10 (end)");

          if (ff == tenthID) {
            $(".action-button").delay(500).fadeIn(function () {
              var optionData = dataPosition.options; // append the options btn

              $.each(optionData, function (i, val) {
                options = "<button type=\"button\" class=\"btn btn-light btn-block opt-btn\" data-click=\"" + val.message + "\">" + val.message + "</button>";
                $(".imessage").append(options);
                $(".online").removeClass('hide').addClass("displayFlex");
                $(".offline").addClass('hide').removeClass('displayFlex');
                $(".choices").removeClass("hide").addClass('displayFlex');
                $(".opt-btn").addClass('optionEnd_' + tenthID);
                $(".online").on('click', function () {
                  $(".choices").toggle();
                  $(".choices").removeClass('displayFlex');
                });
                $(".online").on('click', function () {
                  $(".choices").toggle();
                });
              });
              $(".chat-box").css({
                'top': '-26%'
              });
              playOptionsSound();
              console.log('option 10 appear');
            });
          } //call the function


          scrollChat();
        });
      };

      for (var ff = ninethID; ff <= lastIndex; ff++) {
        _loop19(ff);
      }
    } else {
      console.log('this the maximum option');
    }
  } //typing animation


  function typingAnimation(time_milisecond) {
    // $(".col-typing-wrapper").removeClass('hide').delay(10000).fadeIn("normal").delay(10000).fadeOut();
    $(".typing").css({
      "display": 'flex',
      "margin-bottom": '5px'
    });
    $(".col-typing-wrapper").removeClass('hide').delay(time_milisecond).fadeIn("normal").delay(time_milisecond).fadeOut();
  }

  function lastMessage(dataPosition, chatData, author, lastIndex, chatLength) {
    var _loop20 = function _loop20(i) {
      typingAnimation(dataPosition.time_milisecond);
      $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
        var dataPosition = chatData[i]; // append chat

        var appendQuestion = "\n                <div class=\"justify-content-start\">\n                    <div class=\"col-12 pl-0 pr-0\">\n                        <div class=\"message-wrapper\">\n                            <div class=\"message\">\n                                <div class=\"text\">\n                                    <span class=\"player-name\">" + author + "</span><br>\n                                    <p class=\"mt-0 mb-0 message\">\n                                        " + dataPosition.message + "\n                                    </p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                ";
        var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
        playNotificationSound();

        if (dataPosition.id == chatLength - 1) {
          console.log('end here (10)');
          $(".eoc-wrapper").show();
        }

        scrollChat();
      });
      console.log('last message', i);
      $(".chat-box").fadeIn().css({
        top: '-3px'
      }); //scroll chat

      scrollChat();
    };

    for (var i = lastIndex + 1; i < chatLength; i++) {
      _loop20(i);
    }
  } //scroll function


  function scrollChat() {
    $(".chat-box").stop().animate({
      scrollTop: $(".chat-box")[0].scrollHeight
    }, 1000);
    console.log('scroll');
  }

  function chatHeightFunc(chatHeight) {
    // return chatHeight;
    var minHeight = chatHeight * 8;
    $(".chat-box").removeClass('move-top').css({
      'position': 'relative',
      'top': minHeight + 'px !important'
    }); // console.log('chatheiight called', minHeight);
  }
});

/***/ }),

/***/ "./resources/js/core/core.js":
/*!***********************************!*\
  !*** ./resources/js/core/core.js ***!
  \***********************************/
/***/ (() => {

$(document).ready(function () {
  console.log('%cCore Logic loaded', 'color: burlywood; font-weight: bold;');
  console.info('%cApp Ready', 'color: green; font-weight: bold;'); //clear screen

  $(".col-typing-wrapper").addClass('hide');
  $(".col-message-wrapper").addClass('hide');
  $('#example').DataTable();
  $('.justify-content-start').animate({
    scrollTop: $('.chat-box').get(0).scrollHeight
  }, 1500);
  $(".control").on('click', function () {
    $(".choices").slideToggle("normal", function () {
      if ($(this).is(":visible")) {
        $(".chat-box").fadeIn().css({
          position: 'relative'
        }).animate({
          top: '-26%'
        }, 500);
      } else {
        $(".chat-box").fadeIn().css({
          position: 'relative'
        }).animate({
          top: '-10px'
        }, 500);
      }
    });
  });
  $('.btn-play').on('click', function (ev) {
    $(".btn-play").prop('disabled', true);
    var JSONname = $(this).data('name'); // console.log('button play clicked', JSONname);

    $.get("/play-json/" + JSONname, function (data) {
      var fileName = data[0].filename;
      var author = data[0].author;
      var chatLength = Object.keys(data[0].chat).length;
      var chatData = data[0].chat;
      var dataPosition, haveOptions;
      var arrOPtion = [];
      var lastIndex;
      var firstID,
          secondID,
          thirdID,
          fourthID,
          fivethID,
          sixthID,
          seventhID,
          eighthID,
          ninethID,
          tenthID = new Array(); //top phone

      $(".chat-group-title").empty();
      $('.chat-group-title').append(fileName + ".json");
      $('.mt-user-text').empty();
      $(".mt-user-text").append("Online - " + author); //get data with "options" key

      for (var i = 0; i < chatLength; i++) {
        dataPosition = chatData[i];
        haveOptions = dataPosition.hasOwnProperty("options");

        if (haveOptions == true) {
          arrOPtion.push(dataPosition.id); //get data ID

          firstID = arrOPtion[0];
          secondID = arrOPtion[1];
          thirdID = arrOPtion[2];
          fourthID = arrOPtion[3];
          fivethID = arrOPtion[4];
          sixthID = arrOPtion[5];
          seventhID = arrOPtion[6];
          eighthID = arrOPtion[7];
          ninethID = arrOPtion[8];
          tenthID = arrOPtion[9];
        }
      }

      lastIndex = arrOPtion.pop();
      console.log('last index at', lastIndex);
      console.log('chat length', chatLength); //call first loop

      firstLoop(firstID, dataPosition, chatData, author, lastIndex); // when click first option
      //=======================

      $(document).on("click", '.optionEnd_' + firstID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (firstID != chatLength) {
          var _loop = function _loop(_i) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (1)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i = lastIndex; _i < chatLength; _i++) {
            _loop(_i);
          }
        } else {
          console.log('end here (1)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 2nd round

      $(document).on("click", '.option_' + firstID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + firstID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayBlock").addClass('hide');
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex);
      }); //====================
      // when click 2nd option
      //====================

      $(document).on("click", '.optionEnd_' + secondID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (secondID != chatLength) {
          var _loop2 = function _loop2(_i2) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i2]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (2)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i2 = lastIndex; _i2 < chatLength; _i2++) {
            _loop2(_i2);
          }
        } else {
          console.log('end here (2)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 3rd round

      $(document).on("click", '.option_' + secondID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + secondID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayBlock").addClass('hide');
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
          scrollChat();
        });
        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex);
      }); //====================
      // when click 3rd option
      //====================

      $(document).on("click", '.optionEnd_' + thirdID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (thirdID != chatLength) {
          var _loop3 = function _loop3(_i3) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i3]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (3)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i3 = lastIndex; _i3 < chatLength; _i3++) {
            _loop3(_i3);
          }
        } else {
          console.log('end here (3)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 4th round

      $(document).on("click", '.option_' + thirdID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + thirdID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayBlock").addClass('hide');
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex);
      }); //====================
      // when click 4th option
      //====================

      $(document).on("click", '.optionEnd_' + fourthID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (fourthID != chatLength) {
          console.log('still continue');

          var _loop4 = function _loop4(_i4) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i4]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (4)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i4 = lastIndex; _i4 < chatLength; _i4++) {
            _loop4(_i4);
          }
        } else {
          console.log('end here (4)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 5th round

      $(document).on("click", '.option_' + fourthID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + fourthID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex);
      }); //====================
      //when click 5th option
      //====================

      $(document).on("click", '.optionEnd_' + fivethID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (fivethID != chatLength) {
          console.log('still continue');

          var _loop5 = function _loop5(_i5) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i5]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (5)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i5 = lastIndex; _i5 < chatLength; _i5++) {
            _loop5(_i5);
          }
        } else {
          console.log('end here (5)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 5th round

      $(document).on("click", '.option_' + fivethID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + fivethID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayBlock").addClass('hide');
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex);
      }); //====================
      //when click 6th option
      //====================

      $(document).on("click", '.optionEnd_' + sixthID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (sixthID != chatLength) {
          console.log('still continue');

          var _loop6 = function _loop6(_i6) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i6]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (6)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i6 = lastIndex; _i6 < chatLength; _i6++) {
            _loop6(_i6);
          }
        } else {
          console.log('end here (6)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 5th round

      $(document).on("click", '.option_' + sixthID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + sixthID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex);
      }); //====================
      //when click 7th option
      //====================

      $(document).on("click", '.optionEnd_' + seventhID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (seventhID != chatLength) {
          console.log('still continue');

          var _loop7 = function _loop7(_i7) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i7]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (7)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i7 = lastIndex; _i7 < chatLength; _i7++) {
            _loop7(_i7);
          }
        } else {
          console.log('end here (7)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 5th round

      $(document).on("click", '.option_' + seventhID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + seventhID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex);
      }); //====================
      //when click 8th option
      //====================

      $(document).on("click", '.optionEnd_' + eighthID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (eighthID != chatLength) {
          console.log('still continue');

          var _loop8 = function _loop8(_i8) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i8]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (8)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i8 = lastIndex; _i8 < chatLength; _i8++) {
            _loop8(_i8);
          }
        } else {
          console.log('end here (8)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 5th round

      $(document).on("click", '.option_' + eighthID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + eighthID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayFlex").addClass('hide');
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        ninethLoop(eighthID, ninethID, dataPosition, chatData, author, lastIndex);
      }); //====================
      //when click 9th option
      //====================

      $(document).on("click", '.optionEnd_' + ninethID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (ninethID != chatLength) {
          console.log('still continue');

          var _loop9 = function _loop9(_i9) {
            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(function () {
              var dataPosition = chatData[_i9]; // append chat

              var appendQuestion = "\n                                <div class=\"justify-content-start\">\n                                    <div class=\"col-12 pl-0 pr-0\">\n                                        <div class=\"message-wrapper\">\n                                            <div class=\"message\">\n                                                <div class=\"text\">\n                                                    <span class=\"player-name\">" + author + "</span><br>\n                                                    <p class=\"mt-0 mb-0 message\">\n                                                        " + dataPosition.message + "\n                                                    </p>\n                                                </div>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                                ";
              var append = $(appendQuestion).addClass('chat_' + dataPosition.id).appendTo(".chat-box-container");
              $(".chat-box").fadeIn().css({
                position: 'relative',
                top: '-3px'
              }).animate({
                top: '-3px'
              }, 500);

              if (dataPosition.id == chatLength) {
                console.log('end here (9)');
                $(".eoc-wrapper").show();
              }
            });
          };

          for (var _i9 = lastIndex; _i9 < chatLength; _i9++) {
            _loop9(_i9);
          }
        } else {
          console.log('end here (9)');
          $(".eoc-wrapper").show();
        }
      }); //displayed choosen option to screen and run 5th round

      $(document).on("click", '.option_' + ninethID, function () {
        //display selected option
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();
        $(".typing").hide().fadeOut(function () {
          //append message
          var appendAnswer = "\n                            <div class=\"row pr5 justify-content-end\">\n                                <div class=\"col-8 pl-0 pr-0\">\n                                    <div class=\"reply float-right\">\n                                        <div class=\"text text-left\">\n                                            " + clickedBtn + "\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        ";
          var append = $(appendAnswer).addClass('chat_' + ninethID).appendTo(".chat-box-container");
          $(".online").removeClass('displayFlex').addClass("hide");
          $(".offline").addClass('displayFlex').removeClass('hide');
          $(".choices").removeClass("displayFlex").addClass('hide');
          $(".chat-box").fadeIn().css({
            position: 'relative',
            top: '-3px'
          }).animate({
            top: '-3px'
          }, 500);
          playReplySound();
        });
        tenthLoop(ninethID, tenthID, dataPosition, chatData, author, lastIndex, chatLength); // console.log("Last loop");
      }); //====================
      //when click 10th option
      //====================

      $(document).on("click", '.optionEnd_' + tenthID, function () {
        var clickedBtn = $(this).data('click'); //empty action-box

        $(".imessage").empty();

        if (tenthID != chatLength) {
          //display selected option
          var clickedBtn = $(this).data('click'); //empty action-box

          $(".imessage").empty();
          $(".typing").hide().fadeOut(function () {
            //append message
            var appendAnswer = "\n                                <div class=\"row pr5 justify-content-end\">\n                                    <div class=\"col-8 pl-0 pr-0\">\n                                        <div class=\"reply float-right\">\n                                            <div class=\"text text-left\">\n                                                " + clickedBtn + "\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>\n                            ";
            var append = $(appendAnswer).addClass('chat_' + ninethID).appendTo(".chat-box-container");
            $(".online").removeClass('displayFlex').addClass("hide");
            $(".offline").addClass('displayFlex').removeClass('hide');
            $(".choices").removeClass("displayFlex").addClass('hide');
            $(".chat-box").fadeIn().css({
              position: 'relative',
              top: '-3px'
            }).animate({
              top: '-3px'
            }, 500);
            playReplySound();
          }); //disini

          lastMessage(dataPosition, chatData, author, lastIndex, chatLength);
        } else {
          console.log('end here (10)');
          $(".eoc-wrapper").show();
        }
      }); //====================
    });
  });
});

/***/ }),

/***/ "./resources/js/core/sound.js":
/*!************************************!*\
  !*** ./resources/js/core/sound.js ***!
  \************************************/
/***/ (() => {

$(document).ready(function () {
  function playNotificationSound() {
    var notif = $('.chat_notif');
    notif.get(0).play();
  }

  function playReplySound() {
    var reply = $('.option_reply');
    reply.get(0).play();
  }

  function playOptionsSound() {
    var option = $('.option_popup');
    option.get(0).play();
    console.log('option appear');
  }

  function muteOptionSound() {
    var option = $('.option_popup');
    var reply = $('.option_reply');
    var notif = $('.chat_notif');
    $(notif).prop('muted', true);
    $(reply).prop('muted', true);
    $(option).prop('muted', true); // console.log('chat sound muted');
  }

  $('.sound-icon a').click(function () {
    var option = $('.option_popup');
    var reply = $('.option_reply');
    var notif = $('.chat_notif');
    $(notif).prop('muted', false);
    $(reply).prop('muted', false);
    $(option).prop('muted', false);
    $('.s-icon').toggleClass('fa-volume-up fa-volume-mute');

    if ($(".fa-volume-up").is(":visible")) {// console.log('have sound');
    } else {
      if ($(notif).prop('muted', false) && $(reply).prop('muted', false) && $(option).prop('muted', false)) {
        // console.log('muted');
        muteOptionSound();
      }
    }
  });
});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*********************************************!*\
  !*** ./resources/js/core/core-new-index.js ***!
  \*********************************************/
__webpack_require__(/*! ./core */ "./resources/js/core/core.js");

__webpack_require__(/*! ./chat-and-loop */ "./resources/js/core/chat-and-loop.js");

__webpack_require__(/*! ./sound */ "./resources/js/core/sound.js");
})();

/******/ })()
;
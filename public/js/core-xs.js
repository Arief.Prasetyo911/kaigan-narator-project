$(".online-xs").on('click', () => {
    $('.online-xs').css('bottom', '10px')
    $(".choices-xs").slideToggle( "normal", function() {
        if($(this).is(":visible")){
            $(".chat-box-xs").fadeIn().css({
                position: 'relative'
            }).animate({top: '-195px'}, 500);

            $('.online-xs').css('bottom', '5px');
        }
        else {
            $(".chat-box-xs").fadeIn().css({
                position: 'relative',
            }).animate({top: '0'}, 500);

            $('.online-xs').css('bottom', '15px');
        }
    });
});

function playNotificationSound(){
    let notif = $('.chat_notif_xs');
    notif.get(0).play();
}

function playReplySound(){
    let reply = $('.option_reply_xs');
    reply.get(0).play();
}

function playOptionsSound(){
    let option = $('.option_popup_xs');
    option.get(0).play();
    // console.log('option appear');
}

function muteOptionSound(){
    let option = $('.option_popup_xs');
    let reply = $('.option_reply_xs');
    let notif = $('.chat_notif_xs');

    $(notif).prop('muted', true);
    $(reply).prop('muted', true);
    $(option).prop('muted', true);
    // console.log('chat sound muted');
}

$('.btn-play-xs').on('click', function(ev){
    //check media file
    let JSONFileName = $(this).data('name');
    let JsonID = $(this).data('jsonid');
    //change media source
    $.get('/check-media-audio/'+JsonID, (data) => {
       let AudioFileName = data[0].audio_name;
        // console.log('audio media xs', data);
        $(".media__audio__blade").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
    })

    //check media picture
    $.get('/check-media-picture/'+JsonID, (data) => {
        let PictureFileName = data[0].picture_name;
        // console.log('picture media xs', data);
        $(".media__picture__blade").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
    })

    // //==============================================//

    // $(".phone-ui, .btn__back").css('display', 'block');
    $('.detail-page').removeClass('mt-4')
    $(".content-wrapper").addClass('hide');
    $('.visible-only-xs').css('display', 'block');
    $("body").css('background-color', '#333');
    $("#header-text").addClass('hide')
    $(".show-xs").addClass('hide').removeClass('d-block');
    $(".show-sm").addClass('hide').removeClass('d-sm-block');
    $(".show-md").addClass('hide').removeClass('d-md-block');
    //hide EOC
    $('.eoc-wrapper-xs').removeClass('d-flex');
    //==============================================//

    $(".btn-play-xs").prop('disabled', true);
    let JSONname = $(this).data('name')
    // console.log('button play clicked', JSONname);
    $.get("/play-json/"+JSONname, (data) => {
        let fileName = data[0].filename;
        let author = data[0].author;
        let chatLength = Object.keys(data[0].chat).length;
        let chatData = data[0].chat;
        var dataPosition, haveOptions;
        var arrOPtion = [];
        var lastIndex;
        var firstID, secondID, thirdID, fourthID, fivethID, sixthID, seventhID, eighthID, ninethID, tenthID = new Array();
        //for media
        var firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType;
        var arrAudio = [];
        var arrVideo = [];
        var arrPics  = [];

        //top phone
        $(".chat-group-title").empty();
        $('.chat-group-title').append(fileName+".json");
        $('.mt-user-text').empty();
        $(".mt-user-text").append( `Online - `+ author);

        //get data with "options" key
        for (let i = 0; i < chatLength; i++) {
            dataPosition = chatData[i];
            haveOptions = dataPosition.hasOwnProperty("options");
            if(haveOptions == true){
                arrOPtion.push(dataPosition.id);
                //get data ID
                firstID = arrOPtion[0];
                secondID = arrOPtion[1];
                thirdID = arrOPtion[2];
                fourthID = arrOPtion[3];
                fivethID = arrOPtion[4];
                sixthID = arrOPtion[5];
                seventhID = arrOPtion[6];
                eighthID = arrOPtion[7];
                ninethID = arrOPtion[8];
                tenthID = arrOPtion[9];
            }

            //mutimedia files
            haveAudio = dataPosition.hasOwnProperty('browse_audio');
            haveVideo = dataPosition.hasOwnProperty('browse_video');
            havePicture = dataPosition.hasOwnProperty('browse_picture');
            //check if the chat have multimedia file
            if(haveAudio == true){
                arrAudio.push(dataPosition.id);
                //get data ID
                firstBrowseAudio = arrAudio[0];
            } else if(haveVideo == true){
                arrVideo.push(dataPosition.id);
                //get data ID
                firstBrowseVideo = arrVideo[0];
            } else if(havePicture == true){
                arrPics.push(dataPosition.id);
                //get data ID
                firstBrowsePicture = arrPics[0];
            } else {
                console.log("Don't have any media");
            }
        }

        lastIndex = arrOPtion.pop();
        console.log('last index at', lastIndex);
        console.log('chat length', chatLength);
        console.log('have audio, position', firstBrowseAudio);
        console.log('have video, position', firstBrowseVideo);
        console.log('have picture, position', firstBrowsePicture);

        //call first loop
        // firstLoop(firstID, dataPosition, chatData, author, lastIndex);
        firstLoop(firstID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, JsonID);

        // when click first option
        //=======================
            $(document).on("click", '.optionEnd_'+firstID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(firstID != chatLength){
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (1)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (1)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 1st round
            $(document).on("click", '.option_'+firstID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+firstID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayBlock").addClass('hide');
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);

                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    // console.log('loop 1 have video');
                    secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    // console.log('loop 1 have audio');
                    mediaType = 'audio';
                    secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    // console.log('loop 1 have picture');
                    mediaType = 'picture';
                    secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    // console.log("loop 1 don't have any media");
                    // mediaType = null;
                    secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        // when click 2nd option
        //====================
            $(document).on("click", '.optionEnd_'+secondID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(secondID != chatLength){
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (2)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (2)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 2nd round
            $(document).on("click", '.option_'+secondID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+secondID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayBlock").addClass('hide');
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                    scrollChat();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        // when click 3rd option
        //====================
            $(document).on("click", '.optionEnd_'+thirdID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(thirdID != chatLength){
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (3)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (3)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 3th round
            $(document).on("click", '.option_'+thirdID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+thirdID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayBlock").addClass('hide');
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            })
        //====================

        // when click 4th option
        //====================
            $(document).on("click", '.optionEnd_'+fourthID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(fourthID != chatLength){
                    console.log('still continue');
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (4)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (4)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 4th round
            $(document).on("click", '.option_'+fourthID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+fourthID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            })
        //====================

        //when click 5th option
        //====================
            $(document).on("click", '.optionEnd_'+fivethID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(fivethID != chatLength){
                    console.log('still continue');
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);


                            if(dataPosition.id == chatLength){
                                console.log('end here (5)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (5)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 5th round
            $(document).on("click", '.option_'+fivethID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+fivethID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayBlock").addClass('hide');
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        //when click 6th option
        //====================
            $(document).on("click", '.optionEnd_'+sixthID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(sixthID != chatLength){
                    console.log('still continue');
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (6)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (6)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 6th round
            $(document).on("click", '.option_'+sixthID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+sixthID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        //when click 7th option
        //====================
            $(document).on("click", '.optionEnd_'+seventhID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(seventhID != chatLength){
                    console.log('still continue');
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (7)');
                                $(".eoc-wrapper-xs").show();
                            }
                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (7)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 7th round
            $(document).on("click", '.option_'+seventhID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+seventhID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        //when click 8th option
        //====================
            $(document).on("click", '.optionEnd_'+eighthID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(eighthID != chatLength){
                    console.log('still continue');
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (8)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (8)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 8th round
            $(document).on("click", '.option_'+eighthID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+eighthID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayFlex").addClass('hide');
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        //when click 9th option
        //====================
            $(document).on("click", '.optionEnd_'+ninethID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(ninethID != chatLength){
                    console.log('still continue');
                    for (let i = lastIndex; i < chatLength; i++) {
                        $(".col-typing-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                            let dataPosition = chatData[i];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper-xs">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");

                            $(".chat-box-xs").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);

                            if(dataPosition.id == chatLength){
                                console.log('end here (9)');
                                $(".eoc-wrapper-xs").show();
                            }

                            playNotificationSound();
                            scrollChat();
                        })
                    }
                } else {
                    console.log('end here (9)');
                    $(".eoc-wrapper-xs").show();
                }
            });

            //displayed choosen option to screen and run 9th round
            $(document).on("click", '.option_'+ninethID, function(){
                //display selected option
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                $(".typing-xs").hide().fadeOut(()=> {
                    //append message
                    var appendAnswer =
                        `
                        <div class="row pr5 justify-content-end">
                            <div class="col-8 pl-0 pr-0">
                                <div class="reply float-right">
                                    <div class="text text-left">
                                        `+clickedBtn+`
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo("#chat-box-container-xs");

                    $(".online-xs").removeClass('displayFlex').addClass("hide");
                    $(".offline-xs").addClass('displayFlex').removeClass('hide');
                    $(".choices-xs").removeClass("displayFlex").addClass('hide');
                    $(".chat-box-xs").fadeIn().css({
                        position: 'relative',
                        top: '-3px'
                    }).animate({top: '-3px'}, 500);
                    playReplySound();
                });

                //check media
                if($(this).hasClass('haveVideo') == true){
                    mediaType = 'video';
                    tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveAudio') == true){
                    mediaType = 'audio';
                    tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('havePicture') == true) {
                    mediaType = 'picture';
                    tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }

                if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                    tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                }
            });
        //====================

        //when click 10th option
        //====================
            $(document).on("click", '.optionEnd_'+tenthID, function(){
                var clickedBtn = $(this).data('click');
                //empty action-box
                $(".imessage-xs").empty();
                if(tenthID != chatLength){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage-xs").empty();
                    $(".typing-xs").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo("#chat-box-container-xs");

                        $(".online-xs").removeClass('displayFlex').addClass("hide");
                        $(".offline-xs").addClass('displayFlex').removeClass('hide');
                        $(".choices-xs").removeClass("displayFlex").addClass('hide');
                        $(".chat-box-xs").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //disini
                    lastMessage(dataPosition, chatData, author, lastIndex, chatLength);

                } else {
                    console.log('end here (10)');
                    $(".eoc-wrapper-xs").show();
                }
            });
        //====================
    });
});

//loop function
function firstLoop(firstID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, JsonID){
    //first loop
    if(firstID == lastIndex){
        console.log('%c Near End of Chat (1) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let aa = 0; aa < lastIndex; aa++) {
            // console.log(aa);
            typingAnimation(dataPosition.time_milisecond);
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                dataPosition = chatData[aa];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                //play sound
                playNotificationSound();
                console.log("loop-1");
                // console.log('pos loop 1',dataPosition);
                if(aa == firstID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").addClass("displayFlex");
                            $(".offline-xs").addClass('hide');
                            $(".choices-xs").css('display', 'block');
                            $(".opt-btn-xs").addClass('option_'+dataPosition.id).addClass('optionEnd_'+firstID);
                            $(".control_online").addClass('control_1st');
                        });
                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.info('%c APP START ', 'color: white; font-weight: bold; background-color: green;');
        if(firstBrowseAudio < firstID){
            console.log('%c Audio Found (Loop 1) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = 0; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    let AudioFileName = data;
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name);
                    console.log('success change audio src', AudioFileName);
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < firstID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == firstID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '0%'
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo < firstID){
            console.log('%c Video Found (Loop 1) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = 0; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button 
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_video_file_xs');
                $('.browse-xs').removeClass('displayFlex').addClass('hide')
                $(".offline_xs").removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < firstID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper_xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();

                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == firstID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '0%'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture < firstID){
            console.log('%c Picture Found (Loop 1) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = 0; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $('.browse-xs').removeClass('displayFlex').addClass('hide')
                $(".offline-xs").removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < firstID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == firstID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '0%'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 1) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let a = 0; a < firstID; a++) {
                // console.log('a',a );
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[a];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    let chatHeight = $(".message-wrapper-xs").height();
                    //play sound
                    playNotificationSound();
                    if(a == firstID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            // $(".chat-box-xs").addClass('move-top')
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").addClass("displayFlex").addClass('control_1st');
                                $(".offline-xs").addClass('hide');
                                $(".choices-xs").css('display', 'block');
                                $(".opt-btn").addClass('option_'+dataPosition.id);
                            });
                            playOptionsSound();
                            chatHeightFunc(chatHeight);
                            // console.log('action button fade in');
                            $(".chat-box-xs").css({
                                'top': '-7.5vh'
                            });

                            $('.online-xs').css('bottom', '0');
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }
}

function secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, prevLoopMedia, JsonID){
    if(secondID == lastIndex){
        console.log('%c Near End of Chat (2) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let bb = firstID; bb < lastIndex; bb++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[bb];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                console.log("loop-2");
                if(bb == secondID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").addClass("displayFlex").removeClass('hide');
                            $(".offline-xs").addClass('hide').removeClass("displayFlex");
                            $(".choices-xs").removeClass('hide').addClass('displayBlock');
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+secondID);
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (2)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > firstID && firstBrowseAudio < secondID){
            console.log('%c Audio Found (Loop 2) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = firstID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_audio-xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    let AudioFileName = data;
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name);
                    console.log('success change audio src', AudioFileName);
                })

                //after the control_select button clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < secondID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == secondID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })

        } else if(firstBrowseVideo > firstID && firstBrowseVideo < secondID){
            console.log('%c Video Found (Loop 2) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = firstID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1')
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the control_select clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < secondID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == secondID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })

        } else if(firstBrowsePicture > firstID && firstBrowsePicture < secondID){
            console.log('%c Picture Found (Loop 2) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = firstID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the control_select clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < secondID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == secondID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 2) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let b = firstID; b < secondID; b++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[b];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();
                    let chatHeight = $(".message-wrapper-xs").height();

                    if(b == secondID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            $(".chat-box-xs").addClass('move-top')
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").addClass("displayFlex").removeClass('hide');
                                $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                $(".choices-xs").removeClass('hide').addClass('block', () => {
                                    $(".chat-box-xs").fadeIn().animate({top: '-34vh'}, 500);
                                });
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                if($(".chat-box-xs").hasClass('position-0') == true){
                                    $(".chat-box-xs").removeClass('position-0')
                                }
                            });

                            chatHeightFunc(chatHeight);
                            // console.log('action button fade in');
                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }
}

function thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, prevLoopMedia, JsonID){
    if(thirdID == lastIndex){
        console.log('%c Near End of Chat (3) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let cc = secondID; cc < lastIndex; cc++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[cc];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                console.log("loop-3");
                if(cc == thirdID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+thirdID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayBlock');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (3)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > secondID && firstBrowseAudio < thirdID){
            console.log('%c Audio Found (Loop 3) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = secondID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                console.log('btn_select_media_audio click');
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio');

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    let AudioFileName = data;
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name);
                    console.log('success change audio src', AudioFileName);
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < thirdID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == thirdID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > secondID && firstBrowseVideo < thirdID){
            console.log('%c Video Found (Loop 3) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = secondID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < thirdID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == thirdID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > secondID && firstBrowsePicture < thirdID){
            console.log('%c Picture Found (Loop 3) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = secondID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < thirdID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == thirdID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 3) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let c = secondID; c < thirdID; c++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[c];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();
                    // let chatHeight = $(".message-wrapper-xs").height();

                    if(c == thirdID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").removeClass('displayFlex').addClass('hide');
                                $(".choices-xs").removeClass("hide").addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayBlock');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }
}

function fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, prevLoopMedia, JsonID){
    if(fourthID == lastIndex){
        console.log('%c Near End of Chat (4) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let dd = thirdID; dd < lastIndex; dd++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[dd];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                console.log('loop-4');
                if(dd == fourthID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+fourthID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayBlock');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (4)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > thirdID && firstBrowseAudio < fourthID){
            console.log('%c Audio Found (Loop 4) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = thirdID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    // console.log('audio media', data);
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < fourthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == fourthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > thirdID && firstBrowseVideo < fourthID){
            console.log('%c Video Found (Loop 4) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = thirdID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < fourthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == fourthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > thirdID && firstBrowsePicture < fourthID){
            console.log('%c Picture Found (Loop 4) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = thirdID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    // console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < fourthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();
                        // console.log('data', dataPosition.id);
                        if(b == fourthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 4) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let d = thirdID; d < fourthID; d++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[d];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();
                    if(d == fourthID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").removeClass('displayFlex').addClass('hide');
                                $(".choices-xs").removeClass("hide").addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayBlock');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }
}

function fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
    if(fivethID == lastIndex){
        console.log('%c Near End of Chat (5) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let ee = fourthID; ee < lastIndex; ee++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[ee];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                // console.log("loop-5");
                if(ee == fivethID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+fivethID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayBlock');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (5)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > fourthID && firstBrowseAudio < fivethID){
            console.log('%c Audio Found (Loop 5) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = fourthID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    // console.log('audio media', data);
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < fivethID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == fivethID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > fourthID && firstBrowseVideo < fivethID){
            console.log('%c Video Found (Loop 5) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = fourthID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1')
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < fivethID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == fivethID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > fourthID && firstBrowsePicture < fivethID){
            console.log('%c Picture Found (Loop 5) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = fourthID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    // console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < fivethID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();
                        console.log('data', dataPosition.id);
                        if(b == fivethID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 5) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let e = fourthID; e < fivethID; e++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[e];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();

                    if(e == fivethID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").removeClass('displayFlex').addClass('hide');
                                $(".choices-xs").removeClass("hide").addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayBlock');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }
}

function sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
    if(sixthID == lastIndex){
        console.log('%c Near End of Chat (6) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let ee = fivethID; ee < lastIndex; ee++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[ee];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                console.log("loop-6");
                if(ee == sixthID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+sixthID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayFlex');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (6)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > fivethID && firstBrowseAudio < sixthID){
            console.log('%c Audio Found (Loop 6) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = fivethID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    // console.log('audio media', data);
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < sixthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == sixthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > fivethID && firstBrowseVideo < sixthID){
            console.log('%c Video Found (Loop 6) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = fivethID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < sixthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == sixthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > fivethID && firstBrowsePicture < sixthID){
            console.log('%c Picture Found (Loop 6) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = fivethID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    // console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < sixthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();
                        console.log('data', dataPosition.id);
                        if(b == sixthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 6) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let e = fivethID; e < sixthID; e++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[e];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();

                    if(e == sixthID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".choices-xs").removeClass("hide").addClass('displayFlex').addClass("displayBlock");
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayFlex');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }
}

function seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID) {
    if(seventhID == lastIndex){
        console.log('%c Near End of Chat (7) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let ff = sixthID; ff < lastIndex; ff++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[ff];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                // console.log("loop-7");
                if(ff == seventhID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+seventhID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayBlock');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (7)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > sixthID && firstBrowseAudio < seventhID){
            console.log('%c Audio Found (Loop 7) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = sixthID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    // console.log('audio media', data);
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < seventhID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == seventhID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > sixthID && firstBrowseVideo < seventhID){
            console.log('%c Video Found (Loop 7) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = sixthID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < seventhID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == seventhID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > sixthID && firstBrowsePicture < seventhID){
            console.log('%c Picture Found (Loop 7) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = sixthID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    // console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < seventhID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();
                        console.log('data', dataPosition.id);
                        if(b == seventhID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 7) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let f = sixthID; f < seventhID; f++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[f];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();

                    if(f == seventhID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".choices-xs").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayBlock');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }
}

function eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
    if(eighthID == lastIndex){
        console.log('%c Near End of Chat (8) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let ff = seventhID; ff < lastIndex; ff++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[ff];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                console.log("loop-8");
                if(ff == eighthID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+eighthID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayBlock');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (8)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > seventhID && firstBrowseAudio < eighthID){
            console.log('%c Audio Found (Loop 8) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = seventhID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    // console.log('audio media', data);
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < eighthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == eighthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > seventhID && firstBrowseVideo < eighthID){
            console.log('%c Video Found (Loop 8) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = seventhID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < eighthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == eighthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > seventhID && firstBrowsePicture < eighthID){
            console.log('%c Picture Found (Loop 8) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = seventhID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    // console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < eighthID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();
                        console.log('data', dataPosition.id);
                        if(b == eighthID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 8) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let f = seventhID; f < eighthID; f++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[f];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();

                    if(f == eighthID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".choices-xs").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayBlock');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }
}

function ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
    if(ninethID == lastIndex){
        console.log('%c Near End of Chat (9) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let ff = eighthID; ff < lastIndex; ff++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[ff];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();
                console.log("loop-9 (near end)");
                if(ff == ninethID-1){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").removeClass('displayFlex').addClass('hide');
                            $(".choices-xs").removeClass("hide");
                            $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+ninethID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayBlock');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();

                        console.log('option 9 appear (near end)');
                    })
                }

                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('continue chat (9)');
        console.log('=================');

        if(window.localStorage.getItem('audioMediaDetected') !== null){
            firstBrowseAudio = 0;
        }

        if(window.localStorage.getItem('videoMediaDetected') !== null){
            firstBrowseVideo = 0;
        }

        if(window.localStorage.getItem('pictureMediaDetected') !== null){
            firstBrowsePicture = 0;
        }

        console.log('firstBrowseAudio',firstBrowseAudio);
        console.log('firstBrowseVideo',firstBrowseVideo);
        console.log('firstBrowsePicture',firstBrowsePicture);

        if(firstBrowseAudio > eighthID && firstBrowseAudio < ninethID){
            console.log('%c Audio Found (Loop 9) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = eighthID; m < firstBrowseAudio; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseAudio-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_audio;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").addClass('displayFlex').removeClass('hide');
                                $(".btn-browse").addClass('browse_audio_file_xs');
                            })
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_audio_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
                console.log('back button click');
            })

            $('.browse-xs').on('click', '.browse_audio_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
                console.log('browse audio after click back button');
            })

            $(".control_select").on("click", '.btn_select_media_audio', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_audio_xs").addClass('hide')
                $(".btn-browse").removeClass('browse_audio_file_xs');
                $('.offline-xs').removeClass('hide');
                $(".back_audio").removeClass('btn_back_audio_1');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_audio')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <audio controls src="" class="media__audio" style="width: 225px;">
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container-xs");

                $.get('/check-media-audio/'+JsonID, (data) => {
                    // console.log('audio media', data);
                    $(".media__audio").attr('src', 'http://localhost:8000/multimedia/audio/'+data[0].audio_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseAudio; b < ninethID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == ninethID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    } else {
                                        $(".chat-box-xs").css({
                                            'top': '-34vh'
                                        });
                                    }
                                });

                                let audioMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                chatHeightFunc(chatHeight);
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowseVideo > eighthID && firstBrowseVideo < ninethID){
            console.log('%c Video Found (Loop 9) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = eighthID; m < firstBrowseVideo; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowseVideo-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_video;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_video_file_xs');
                                $(".control_browse ").removeClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_video_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_video_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_video', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_video_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_video").removeClass('btn_back_video_1');
                $(".btn-browse").removeClass('browse_video_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_video')

                // append video
                var appendQuestion =
                `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <video  width="75%" height="100%" controls>
                                <source src="" class="media__video" type="video/mp4">
                            </video>
                        </div>
                    </div>
                `;
                $(appendQuestion).addClass('chat_media_video').appendTo("#chat-box-container-xs");

                //check media video
                $.get('/check-media-video/'+JsonID, (data) => {
                    // console.log('video media', data);
                    $(".media__video").attr('src', 'https://narrator.kaigangames.com/multimedia/videos/'+data[0].video_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowseVideo; b < ninethID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();

                        if(b == ninethID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    // $(".chat-box").removeClass("position-0");

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let videoMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else if(firstBrowsePicture > eighthID && firstBrowsePicture < ninethID){
            console.log('%c Picture Found (Loop 9) ', 'color: blue; font-weight: bold; background-color: aqua;');
            for (let m = eighthID; m < firstBrowsePicture; m++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[m];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    //play sound
                    playNotificationSound();
                    if(m == firstBrowsePicture-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let mediaData = dataPosition.browse_picture;
                            $.each(mediaData, (i, val) => {
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".browse-xs").css('display', 'flex');
                                $(".btn-browse").addClass('browse_picture_file_xs');
                                $('.browse-xs').addClass('displayFlex').removeClass('hide');
                                $('.offline-xs').addClass('hide');
                            })
                            playOptionsSound();
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }

            //when click back button
            $(".control_select").on("click", '.btn_back_picture_1',() => {
                $('.justify-content-start').removeClass('hide')
                $('.justify-content-end').removeClass('hide')
                $(".justify-content-end").removeClass('hide')

                $('.control_browse').css('display', 'flex');
                $('.browse_media_xs').css('display', 'none');
            })

            $('.browse-xs').on('click', '.browse_picture_file_xs', () => {            
                $('.browse_media_xs').css('display', 'block');
                $('.control_browse').css('display', 'none');
            })

            $(".control_select").on("click", '.btn_select_media_picture', () => {
                $('.justify-content-start').removeClass('hide')
                $(".justify-content-end").removeClass('hide')
                $(".media_picture_xs").addClass('hide')
                $(".online-xs").removeClass('displayFlex').addClass('hide')
                $('.browse-xs').addClass('hide').removeClass('displayFlex');
                $('.offline-xs').removeClass('hide');
                $(".back_picture").removeClass('btn_back_picture_1');
                $(".btn-browse").removeClass('browse_picture_file_xs');
                $(".control_select").addClass('hide');
                $('.btn-media').removeClass('btn_select_media_picture')

                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <img src="" class="w-50 media__picture" alt="Pic">
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container-xs");

                //check media picture
                $.get('/check-media-picture/'+JsonID, (data) => {
                    // console.log('picture media', data);
                    $(".media__picture").attr('src', 'http://localhost:8000/multimedia/pictures/'+data[0].picture_name)
                })

                //after the btn_back_audio_1 clicked, continue the chat until reach the option
                for (let b = firstBrowsePicture; b < ninethID; b++) {
                    typingAnimation(dataPosition.time_milisecond,)
                    $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper-xs">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                        playNotificationSound();
                        let chatHeight = $(".message-wrapper-xs").height();
                        console.log('data', dataPosition.id);
                        if(b == ninethID-1){
                            $(".action-button-xs").delay(500).fadeIn(() => {
                                $(".chat-box-xs").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage-xs").append(options);
                                    $(".browse-xs").addClass('hide');
                                    $(".online-xs").addClass("displayFlex").removeClass('hide');
                                    $(".offline-xs").addClass('hide').removeClass("displayFlex");
                                    $(".choices-xs").removeClass('hide').css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                    if($(".chat-box-xs").hasClass('position-0') == true){
                                        $(".chat-box-xs").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box-xs").css({
                                    'top': '-34vh'
                                });

                                let pictureMedia = {
                                    position: dataPosition.id,
                                    status: true
                                }
                                //save media to localStorage
                                window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            })
        } else {
            console.log('%c No Media Found (Loop 9) ', 'color: aqua; font-weight: bold; background-color: grey;');
            for (let f = eighthID; f < ninethID; f++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[f];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper-xs">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                    playNotificationSound();

                    if(f == ninethID-1){
                        $(".action-button-xs").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage-xs").append(options);
                                $(".online-xs").removeClass('hide').addClass("displayFlex");
                                $(".offline-xs").addClass('hide').removeClass('displayFlex');
                                $(".choices-xs").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                    $(".choices-xs").removeClass('displayBlock');
                                })

                                $(".online-xs").on('click', ()=>{
                                    $(".choices-xs").toggle();
                                })
                            });

                            $(".chat-box-xs").css({
                                'top': '-34vh'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }
}

function tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
    if(tenthID == lastIndex){
        console.log('%c No Media Found (Loop 10) ', 'color: aqua; font-weight: bold; background-color: grey;');
        console.log('%c End of Chat (10) ', 'color: red; font-weight: bold; background-color: grey;');
        for (let ff = ninethID; ff <= lastIndex; ff++) {
            typingAnimation(dataPosition.time_milisecond)
            $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                let dataPosition = chatData[ff];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper-xs">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
                playNotificationSound();

                if(ff == tenthID){
                    $(".action-button-xs").delay(500).fadeIn(() => {
                        let optionData = dataPosition.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                            $("#imessage-xs").append(options);
                            $(".online-xs").removeClass('hide').addClass("displayFlex");
                            $(".offline-xs").addClass('hide').removeClass('displayFlex');
                            $(".choices-xs").removeClass("hide").addClass('displayFlex');
                            $(".opt-btn").addClass('optionEnd_'+tenthID);

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                                $(".choices-xs").removeClass('displayFlex');
                            })

                            $(".online-xs").on('click', ()=>{
                                $(".choices-xs").toggle();
                            })
                        });

                        $(".chat-box-xs").css({
                            'top': '-34vh'
                        });

                        playOptionsSound();
                    })
                }
                //call the function
                scrollChat();
            });
        }
    } else {
        console.log('%c This the Maximum Option ', 'color: orange; font-weight: bold; background-color: grey;');
    }
}

function lastMessage(dataPosition, chatData, author, lastIndex, chatLength){
    for (let i = lastIndex+1; i < chatLength; i++) {
        typingAnimation(dataPosition.time_milisecond);
        $(".col-message-wrapper-xs").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
            let dataPosition = chatData[i];
            // append chat
            var appendQuestion =
            `
            <div class="justify-content-start">
                <div class="col-12 pl-0 pr-0">
                    <div class="message-wrapper-xs">
                        <div class="message">
                            <div class="text">
                                <span class="player-name">`+author+`</span><br>
                                <p class="mt-0 mb-0 message">
                                    `+dataPosition.message+`
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `;
            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container-xs");
            playNotificationSound();
            if(dataPosition.id == chatLength-1){
                console.log('%c End Here (10) ', 'color: red; font-weight: bold; background-color: white;');
                $(".eoc-wrapper-xs").show();
                $('.eoc-wrapper-xs').addClass('d-flex');
                $('.offline-xs').removeClass('displayFlex').css('display', 'none')
            }

            scrollChat();
        })

        $(".chat-box-xs").fadeIn().css({
            top: '0'
        });
        //scroll chat
        scrollChat();
    }
}

//typing animation
function typingAnimation(time_milisecond){
    $(".typing-xs").css({
        "display" : 'flex',
        "margin-bottom" : '5px'
    })

    $(".col-typing-wrapper-xs").removeClass('hide').delay(time_milisecond).fadeIn("normal").delay(time_milisecond).fadeOut();
}

//scroll function
function scrollChat(){
    $(".chat-box-xs").stop().animate({ scrollTop: 20000}, 1000);
    let sh = $('.chat-box-xs').get(0).scrollHeight;
    console.log('scroll chat');
}

function chatHeightFunc(chatHeight){
    // return chatHeight;
    let minHeight = chatHeight * 6;

    $(".chat-box-xs").removeClass('move-top').css({
        'position': 'relative',
        'top': minHeight+'px !important'
    })

    console.log('minheight called', minHeight);
    console.log('chat-height called', chatHeight);
}

$('.sound-icon-xs a').click(() =>{
    let option = $('.option_popup_xs');
    let reply = $('.option_reply_xs');
    let notif = $('.chat_notif_xs');

    $(notif).prop('muted', false);
    $(reply).prop('muted', false);
    $(option).prop('muted', false);

    $('.s-icon').toggleClass('fa-volume-up fa-volume-mute');

    if($(".fa-volume-up").is(":visible")){
        // console.log('have sound');
    } else {
        if($(notif).prop('muted', false) && $(reply).prop('muted', false) && $(option).prop('muted', false)){
            // console.log('muted');
            muteOptionSound();
        }
    }
})


$(".btn-select-audio_confirm").on('click', () => {
    console.log('button audio confirm clicked');
    let isChecked = $('.selected_audio').is(":checked");

    if (isChecked) {
        $(".select_item-xs").css('display', 'flex')

        //disabled back button when confirm checkbox is checkced
        if($('.select_item-xs').is(':visible')){
            $('.back_audio').prop('disabled', true);
        }
    } else {
        $(".select_item-xs").css('display', 'none')

        //disabled back button when confirm checkbox is checkced
        if($('.select_item-xs').is(':hidden')){
            $('.back_audio').prop('disabled', false);
        }
    }
})

$(".btn-select-video_confirm").on('click', () => {
    console.log('button video confirm clicked');
    let isCheckedVideo = $(".selected_video").is(":checked");

    if (isCheckedVideo) {
        $(".select_item-xs").css('display', 'flex').removeClass('hide')

        //disabled back button when confirm checkbox is checkced
        if($('.select_item-xs').is(':visible')){
            $('.back_video').prop('disabled', true);
        }
    } else {
        $(".select_item-xs").css('display', 'none')

        //disabled back button when confirm checkbox is checkced
        if($('.select_item-xs').is(':hidden')){
            $('.back_video').prop('disabled', false);
        }
    }
})

$(".btn-select-picture_confirm").on('click', () => {
    console.log('button picture confirm clicked');
    let isCheckedPicture = $(".selected_picture").is(":checked");

    if (isCheckedPicture) {
        $(".select_item-xs").css('display', 'flex').removeClass('hide')

        //disabled back button when confirm checkbox is checkced
        if($('.select_item-xs').is(':visible')){
            $('.back_picture').prop('disabled', true);
        }
    } else {
        $(".select_item-xs").css('display', 'none')

        //disabled back button when confirm checkbox is checkced
        if($('.select_item-xs').is(':hidden')){
            $('.back_picture').prop('disabled', false);
        }
    }
})

//browse audio file.
$('.browse-xs').on('click', '.browse_audio_file_xs', () => {
    // console.log('browse audio klik');
    $(".justify-content-start, .justify-content-end, .choices-xs").addClass('hide');
    $(".back_audio").addClass('btn_back_audio_1')
    $('.control_browse').removeClass('displayFlex')
    $(".chat-box-xs").addClass('position-0')
    $('.browse-xs').removeClass('browse_audio_file_xs')
    $('.btn-media').addClass('btn_select_media_audio')
    $('.media_audio_xs').removeClass('hide');
    $('.confirm_select_audio_item-xs').css('display', 'block');

    //check class for media, hide media picture
    if($(".media_picture_xs").hasClass('hide') == true){
        //next
    } else {
        $(".media_picture_xs").addClass('hide');
    }

    //check class for media, hide media video
    if($(".media_video_xs").hasClass('hide') == true){
        //next
    } else {
        $(".media_video-xs").addClass('hide');
    }
})

//browse video file
$(".browse-xs").on('click', '.browse_video_file_xs', () => {
    $('.justify-content-start, .justify-content-end, .choices-xs').addClass('hide')
    $(".media_video_xs").removeClass('hide')
    $(".back_video").addClass('btn_back_video_1')
    $('.control_browse').removeClass('displayFlex').css('display', 'none')
    $(".chat-box-xs").addClass('position-0')
    $('.browse-xs').removeClass('browse_video_file_xs')
    $('.btn-media').addClass('btn_select_media_video')
    $('.confirm_select_video_item-xs').css('display', 'block').removeClass('hide')

    //check class for media, hide media audio
    if($(".media_audio_xs").hasClass('hide') == true){
        //next
    } else {
        $(".media_audio_xs").addClass('hide');
    }

    //check class for media, hide media picture
    if($(".media_picture_xs").hasClass('hide') == true){
        //next
    } else {
        $(".media_picture_xs").addClass('hide');
    }
})

//browse picture file
$(".browse-xs").on('click', '.browse_picture_file_xs', () => {
    $('.justify-content-start, .justify-content-end').addClass('hide')
    // $('.justify-content-end').addClass('hide')
    $(".media_picture_xs").removeClass('hide')
    $(".media_video_xs").addClass('hide')
    $(".back_picture").addClass('btn_back_picture_1')
    $('.browse-xs').removeClass('browse_picture_file_xs')
    $('.control_browse').removeClass('displayFlex').css('display', 'none')
    $('.btn-media').addClass('btn_select_media_picture')
    $('.confirm_select_picture_item-xs').removeClass('hide').css('display', 'flex')

    //check class for media, hide media audio
    if($(".media_audio_xs").hasClass('hide') == true){
        //next
    } else {
        $(".media_audio_xs").addClass('hide');
    }

    //check class for media, hide media video
    if($(".media_video_xs").hasClass('hide') == true){
        //next
    } else {
        $(".media_video_xs").addClass('hide');
    }
})

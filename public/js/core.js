$(document).ready(() => {
    // console.log('%cCore Logic loaded', 'color: burlywood; font-weight: bold;');
    console.info('%cApp Ready', 'color: green; font-weight: bold;');
    //clear screen
    $(".col-typing-wrapper").addClass('hide');
    $(".col-message-wrapper").addClass('hide');

    $('.table').DataTable();

    $(".selectJSON").select2({
        placeholder: "Select a JSON file",
        ajax: {
            url: '/get-json',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
              return {
                results:  $.map(data, function (item) {
                    console.log(item);
                      return {
                          text: item.json_name,
                          id: item.json_name
                      }
                  })
              };
            },
            cache: true
          }
    })

    //select setting on medium screen devices
    $(".selectJSONMD").on('change', () => {
        let select2Val = $(".selectJSONMD").val();
        $(".process-select-md").attr('data-name', select2Val);

        $.get('/get-json-id/'+select2Val, (data) => {
            let jsonIDValue = data[0].id;
            $(".process-select-md").attr('data-jsonid', jsonIDValue);
        })

        if($('.selectJSONMD').val() != ''){
            $('.process-select-md').prop('disabled', false)
        }
    })

    //select setting on xtra-small screen devices
    $(".selectJSONSM").on('change', () => {
        let select2Val = $(".selectJSONSM").val();
        $(".process-select-sm").attr('data-name', select2Val);

        $.get('/get-json-id/'+select2Val, (data) => {
            let jsonIDValue = data[0].id;
            $(".process-select-sm").attr('data-jsonid', jsonIDValue);
        })

        if($('.selectJSONSM').val() != ''){
            $('.process-select-sm').prop('disabled', false)
        }
    })

    //select setting on xtra-small screen devices
    $(".selectJSONXS").on('change', () => {
        let select2Val = $(".selectJSONXS").val();
        $(".process-select-xs").attr('data-name', select2Val);

        $.get('/get-json-id/'+select2Val, (data) => {
            let jsonIDValue = data[0].id;
            $(".process-select-xs").attr('data-jsonid', jsonIDValue);
        })

        if($('.selectJSONXS').val() != ''){
            $('.process-select-xs').prop('disabled', false)
        }
    })

    $('.justify-content-start').animate({
        scrollTop: $('.chat-box').get(0).scrollHeight
    }, 1500);

    $(".control_online").on('click', () => {
        $(".choices").slideToggle( "normal", function() {
            if($(this).is(":visible")){
                $(".chat-box").fadeIn().css({
                    position: 'relative'
                }).animate({top: '-26%'}, 500);
            }
            else {
                $(".chat-box").fadeIn().css({
                    position: 'relative',
                }).animate({top: '-10px'}, 500);
            }
        });
    });

    function playNotificationSound(){
        let notif = $('.chat_notif');
        notif.get(0).play();
    }

    function playReplySound(){
        let reply = $('.option_reply');
        reply.get(0).play();
    }

    function playOptionsSound(){
        let option = $('.option_popup');
        option.get(0).play();
    }

    function muteOptionSound(){
        let option = $('.option_popup');
        let reply = $('.option_reply');
        let notif = $('.chat_notif');

        $(notif).prop('muted', true);
        $(reply).prop('muted', true);
        $(option).prop('muted', true);
    }

    $('.btn-play-lg').on('click', function(ev){
        $('.btn-play-lg').prop('disabled', true);
        //check media file
        // let JSONFileName = $(this).data('name');
        let JsonID = $(this).data('jsonid');
        //change media source
        $.get('/check-media-audio/'+JsonID, (data) => {
           let AudioFileName = data[0].audio_name;
            // console.log('audio media', AudioFileName);
            $(".media__audio__blade").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+AudioFileName)
        })

        // check media picture
        $.get('/check-media-picture/'+JsonID, (data) => {
            let PictureFileName = data[0].picture_name;
            // console.log('picture media', data);
            $(".media__picture__blade").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+PictureFileName)
        })

        //==============================================//

        $(".btn-play").prop('disabled', true);
        let JSONname = $(this).data('name')
        // console.log('button play clicked', JSONname);
        $.get("/play-json/"+JSONname, (data) => {
            let fileName = data[0].filename;
            let author = data[0].author;
            let chatLength = Object.keys(data[0].chat).length;
            let chatData = data[0].chat;
            var dataPosition, haveOptions;
            var arrOPtion = [];
            var lastIndex;
            var firstID, secondID, thirdID, fourthID, fivethID, sixthID, seventhID, eighthID, ninethID, tenthID = new Array();
            //for media
            var firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType;
            var arrAudio = [];
            var arrVideo = [];
            var arrPics  = [];

            //top phone
            $(".chat-group-title").empty();
            $('.chat-group-title').append(fileName+".json");
            $('.mt-user-text').empty();
            $(".mt-user-text").append( `Online - `+ author);

            //get data with "options" key
            for (let i = 0; i < chatLength; i++) {
                dataPosition = chatData[i];
                haveOptions = dataPosition.hasOwnProperty("options");
                if(haveOptions == true){
                    arrOPtion.push(dataPosition.id);
                    //get data ID
                    firstID = arrOPtion[0];
                    secondID = arrOPtion[1];
                    thirdID = arrOPtion[2];
                    fourthID = arrOPtion[3];
                    fivethID = arrOPtion[4];
                    sixthID = arrOPtion[5];
                    seventhID = arrOPtion[6];
                    eighthID = arrOPtion[7];
                    ninethID = arrOPtion[8];
                    tenthID = arrOPtion[9];
                }

                //mutimedia files
                haveAudio = dataPosition.hasOwnProperty('browse_audio');
                haveVideo = dataPosition.hasOwnProperty('browse_video');
                havePicture = dataPosition.hasOwnProperty('browse_picture');
                //check if the chat have multimedia file
                if(haveAudio == true){
                    arrAudio.push(dataPosition.id);
                    //get data ID
                    firstBrowseAudio = arrAudio[0];
                } else if(haveVideo == true){
                    arrVideo.push(dataPosition.id);
                    //get data ID
                    firstBrowseVideo = arrVideo[0];
                } else if(havePicture == true){
                    arrPics.push(dataPosition.id);
                    //get data ID
                    firstBrowsePicture = arrPics[0];
                } else {
                    // console.log("Don't have any media");
                }
            }

            lastIndex = arrOPtion.pop();
            console.log('last index at', lastIndex);
            console.log('chat length', chatLength);
            console.log('have audio, position', firstBrowseAudio);
            console.log('have video, position', firstBrowseVideo);
            console.log('have picture, position', firstBrowsePicture);

            //call first loop
            firstLoop(firstID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, JsonID);

            // when click first option
            //=======================
                $(document).on("click", '.optionEnd_'+firstID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(firstID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (1)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (1)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 2nd round
                $(document).on("click", '.option_'+firstID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+firstID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);

                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        // console.log('loop 1 have video');
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        // console.log('loop 1 have audio');
                        mediaType = 'audio';
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        // console.log('loop 1 have picture');
                        mediaType = 'picture';
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        // console.log("loop 1 don't have any media");
                        // mediaType = null;
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            // when click 2nd option
            //====================
                $(document).on("click", '.optionEnd_'+secondID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(secondID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (2)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (2)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 3rd round
                $(document).on("click", '.option_'+secondID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+secondID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                        scrollChat();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            // when click 3rd option
            //====================
                $(document).on("click", '.optionEnd_'+thirdID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(thirdID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (3)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (3)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 4th round
                $(document).on("click", '.option_'+thirdID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+thirdID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                })
            //====================

            // when click 4th option
            //====================
                $(document).on("click", '.optionEnd_'+fourthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(fourthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (4)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (4)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+fourthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+fourthID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    
                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                })
            //====================

            //when click 5th option
            //====================
                $(document).on("click", '.optionEnd_'+fivethID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(fivethID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);


                                if(dataPosition.id == chatLength){
                                    console.log('end here (5)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (5)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+fivethID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+fivethID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    
                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 6th option
            //====================
                $(document).on("click", '.optionEnd_'+sixthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(sixthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (6)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (6)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+sixthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+sixthID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 7th option
            //====================
                $(document).on("click", '.optionEnd_'+seventhID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(seventhID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (7)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (7)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+seventhID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+seventhID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 8th option
            //====================
                $(document).on("click", '.optionEnd_'+eighthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(eighthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (8)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (8)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+eighthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+eighthID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 9th option
            //====================
                $(document).on("click", '.optionEnd_'+ninethID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(ninethID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (9)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (9)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+ninethID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 10th option
            //====================
                $(document).on("click", '.optionEnd_'+tenthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(tenthID != chatLength){
                        //display selected option
                        var clickedBtn = $(this).data('click');
                        //empty action-box
                        $(".imessage").empty();
                        $(".typing").hide().fadeOut(()=> {
                            //append message
                            var appendAnswer =
                                `
                                <div class="row pr5 justify-content-end">
                                    <div class="col-8 pl-0 pr-0">
                                        <div class="reply float-right">
                                            <div class="text text-left">
                                                `+clickedBtn+`
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                            var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo(".chat-box-container");

                            $(".online").removeClass('displayFlex').addClass("hide");
                            $(".offline").addClass('displayFlex').removeClass('hide');
                            $(".choices").removeClass("displayFlex").addClass('hide');
                            $(".chat-box").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);
                            playReplySound();
                        });

                        //disini
                        lastMessage(dataPosition, chatData, author, lastIndex, chatLength);

                    } else {
                        console.log('end here (10)');
                        $(".eoc-wrapper").show();
                    }
                });
            //====================
        })
    })

    $('.btn-play-md').on('click', function(ev){
        //check media file
        let JSONFileName = $(this).data('name');
        let JsonID = $(this).data('jsonid');
        //change media source
        $.get('/check-media-audio/'+JsonID, (data) => {
           let AudioFileName = data[0].audio_name;
            // console.log('audio media', data);
            $(".media__audio__blade").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
        })

        //check media picture
        $.get('/check-media-picture/'+JsonID, (data) => {
            let PictureFileName = data[0].picture_name;
            // console.log('picture media', data);
            $(".media__picture__blade").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
        })

        //==============================================//

        $(".phone-ui, .btn__back").css('display', 'block');
        $(".json_table").addClass('hide');
        $("body").css('background-color', '#333');
        $("#header-text").addClass('hide')
        $(".show-xs").addClass('hide').removeClass('d-block');
        $(".show-sm").addClass('hide').removeClass('d-sm-block');
        $(".btn-play-md").prop('disbled', true);
        //==============================================//

        $(".btn-play").prop('disabled', true);
        let JSONname = $(this).data('name')
        $.get("/play-json/"+JSONname, (data) => {
            let fileName = data[0].filename;
            let author = data[0].author;
            let chatLength = Object.keys(data[0].chat).length;
            let chatData = data[0].chat;
            var dataPosition, haveOptions;
            var arrOPtion = [];
            var lastIndex;
            var firstID, secondID, thirdID, fourthID, fivethID, sixthID, seventhID, eighthID, ninethID, tenthID = new Array();
            //for media
            var firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType;
            var arrAudio = [];
            var arrVideo = [];
            var arrPics  = [];

            //top phone
            $(".chat-group-title").empty();
            $('.chat-group-title').append(fileName+".json");
            $('.mt-user-text').empty();
            $(".mt-user-text").append( `Online - `+ author);

            //get data with "options" key
            for (let i = 0; i < chatLength; i++) {
                dataPosition = chatData[i];
                haveOptions = dataPosition.hasOwnProperty("options");
                if(haveOptions == true){
                    arrOPtion.push(dataPosition.id);
                    //get data ID
                    firstID = arrOPtion[0];
                    secondID = arrOPtion[1];
                    thirdID = arrOPtion[2];
                    fourthID = arrOPtion[3];
                    fivethID = arrOPtion[4];
                    sixthID = arrOPtion[5];
                    seventhID = arrOPtion[6];
                    eighthID = arrOPtion[7];
                    ninethID = arrOPtion[8];
                    tenthID = arrOPtion[9];
                }

                //mutimedia files
                haveAudio = dataPosition.hasOwnProperty('browse_audio');
                haveVideo = dataPosition.hasOwnProperty('browse_video');
                havePicture = dataPosition.hasOwnProperty('browse_picture');
                //check if the chat have multimedia file
                if(haveAudio == true){
                    arrAudio.push(dataPosition.id);
                    //get data ID
                    firstBrowseAudio = arrAudio[0];
                } else if(haveVideo == true){
                    arrVideo.push(dataPosition.id);
                    //get data ID
                    firstBrowseVideo = arrVideo[0];
                } else if(havePicture == true){
                    arrPics.push(dataPosition.id);
                    //get data ID
                    firstBrowsePicture = arrPics[0];
                } else {
                    console.log("Don't have any media");
                }
            }

            lastIndex = arrOPtion.pop();
            console.log('last index at', lastIndex);
            console.log('chat length', chatLength);
            console.log('have audio, position', firstBrowseAudio);
            console.log('have video, position', firstBrowseVideo);
            console.log('have picture, position', firstBrowsePicture);

            //call first loop
            firstLoop(firstID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, JsonID);

            // when click first option
            //=======================
                $(document).on("click", '.optionEnd_'+firstID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(firstID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (1)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (1)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 2nd round
                $(document).on("click", '.option_'+firstID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+firstID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);

                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        // console.log('loop 1 have video');
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        // console.log('loop 1 have audio');
                        mediaType = 'audio';
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        // console.log('loop 1 have picture');
                        mediaType = 'picture';
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        // console.log("loop 1 don't have any media");
                        // mediaType = null;
                        secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            // when click 2nd option
            //====================
                $(document).on("click", '.optionEnd_'+secondID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(secondID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (2)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (2)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 3rd round
                $(document).on("click", '.option_'+secondID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+secondID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                        scrollChat();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            // when click 3rd option
            //====================
                $(document).on("click", '.optionEnd_'+thirdID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(thirdID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (3)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (3)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 4th round
                $(document).on("click", '.option_'+thirdID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+thirdID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                })
            //====================

            // when click 4th option
            //====================
                $(document).on("click", '.optionEnd_'+fourthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(fourthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (4)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (4)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+fourthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+fourthID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                })
            //====================

            //when click 5th option
            //====================
                $(document).on("click", '.optionEnd_'+fivethID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(fivethID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);


                                if(dataPosition.id == chatLength){
                                    console.log('end here (5)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (5)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+fivethID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+fivethID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 6th option
            //====================
                $(document).on("click", '.optionEnd_'+sixthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(sixthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (6)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (6)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+sixthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+sixthID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 7th option
            //====================
                $(document).on("click", '.optionEnd_'+seventhID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(seventhID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (7)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (7)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+seventhID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+seventhID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 8th option
            //====================
                $(document).on("click", '.optionEnd_'+eighthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(eighthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (8)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (8)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+eighthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+eighthID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 9th option
            //====================
                $(document).on("click", '.optionEnd_'+ninethID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(ninethID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (9)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (9)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+ninethID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo("#chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });

                    //check media
                    if($(this).hasClass('haveVideo') == true){
                        mediaType = 'video';
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveAudio') == true){
                        mediaType = 'audio';
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('havePicture') == true) {
                        mediaType = 'picture';
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }

                    if($(this).hasClass('haveVideo') == false && $(this).hasClass('haveAudio') == false && $(this).hasClass('havePicture') == false){
                        tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID);
                    }
                });
            //====================

            //when click 10th option
            //====================
                $(document).on("click", '.optionEnd_'+tenthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(tenthID != chatLength){
                        //display selected option
                        var clickedBtn = $(this).data('click');
                        //empty action-box
                        $(".imessage").empty();
                        $(".typing").hide().fadeOut(()=> {
                            //append message
                            var appendAnswer =
                                `
                                <div class="row pr5 justify-content-end">
                                    <div class="col-8 pl-0 pr-0">
                                        <div class="reply float-right">
                                            <div class="text text-left">
                                                `+clickedBtn+`
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                            var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo("#chat-box-container");

                            $(".online").removeClass('displayFlex').addClass("hide");
                            $(".offline").addClass('displayFlex').removeClass('hide');
                            $(".choices").removeClass("displayFlex").addClass('hide');
                            $(".chat-box").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);
                            playReplySound();
                        });

                        //disini
                        lastMessage(dataPosition, chatData, author, lastIndex, chatLength);

                    } else {
                        console.log('end here (10)');
                        $(".eoc-wrapper").show();
                    }
                });
            //====================
        })
    });

    //loop function
    function firstLoop(firstID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, JsonID){
        //first loop
        if(firstID == lastIndex){
            console.log('near end of chat (1)');
            for (let aa = 0; aa < lastIndex; aa++) {
                // console.log(aa);
                typingAnimation(dataPosition.time_milisecond);
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[aa];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    //play sound
                    playNotificationSound();
                    console.log("loop-1");
                    // console.log('pos loop 1',dataPosition);
                    if(aa == firstID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").addClass("displayFlex");
                                $(".offline").addClass('hide');
                                $(".choices").css('display', 'block');
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+firstID);
                                $(".control_online").addClass('control_1st');
                            });
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.info('%c APP START ', 'color: white; font-weight: bold; background-color: green;');
            if(firstBrowseAudio < firstID){
                console.log('%c Audio Found (Loop 1) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = 0; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                // when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <audio controls src="" class="media__audio" style="width: 225px;">
                                    Your browser does not support the
                                    <code>audio</code> element.
                                </audio>
                            </div>
                        </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        let AudioFileName = data;
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name);
                        console.log('success change audio src', AudioFileName);
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < firstID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == firstID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '0%'
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo < firstID){
                console.log('%c Video Found (Loop 1) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = 0; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".btn-browse").removeClass('browse_video_file');
                    $('.browse').removeClass('displayFlex').addClass('hide')
                    $(".offline").removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < firstID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();

                            let chatHeight = $(".justify-content-start").height();

                            if(b == firstID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '0%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture < firstID){
                console.log('%c Picture Found (Loop 1) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = 0; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".btn-browse").removeClass('browse_picture_file');
                    $('.browse').removeClass('displayFlex').addClass('hide')
                    $(".offline").removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <img src="" class="w-50 media__picture" alt="Pic">
                            </div>
                        </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < firstID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == firstID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '0%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 1) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let a = 0; a < firstID; a++) {
                    // console.log('a',a );
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[a];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(a == firstID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").addClass("displayFlex");
                                    $(".offline").addClass('hide');
                                    $(".choices").css('display', 'block');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);
                                    $(".control").addClass('control_1st');
                                });
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            }
        }
    }

    function secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, prevLoopMedia, JsonID){
        if(secondID == lastIndex){
            console.log('near end of chat (2)');
            for (let bb = firstID; bb < lastIndex; bb++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[bb];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log("loop-2");
                    if(bb == secondID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").addClass("displayFlex").removeClass('hide');
                                $(".offline").addClass('hide').removeClass("displayFlex");
                                $(".choices").removeClass('hide').addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+secondID);
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (2)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > firstID && firstBrowseAudio < secondID){
                console.log('%c Audio Found (Loop 2) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = firstID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        let AudioFileName = data;
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name);
                        console.log('success change audio src', AudioFileName);
                    })

                    //after the control_select button clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < secondID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == secondID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })

            } else if(firstBrowseVideo > firstID && firstBrowseVideo < secondID){
                console.log('%c Video Found (Loop 2) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = firstID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1')
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the control_select clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < secondID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == secondID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })

            } else if(firstBrowsePicture > firstID && firstBrowsePicture < secondID){
                console.log('%c Picture Found (Loop 2) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = firstID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the control_select clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < secondID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == secondID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 2) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let b = firstID; b < secondID; b++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[b];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();
                        let chatHeight = $(".justify-content-start").height();

                        if(b == secondID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                $(".chat-box").addClass('move-top')
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").addClass("displayFlex").removeClass('hide');
                                    $(".offline").addClass('hide').removeClass("displayFlex");
                                    $(".choices").removeClass('hide').addClass('block', () => {
                                        $(".chat-box").fadeIn().animate({top: '-26%'}, 500);
                                    });
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    if($(".chat-box").hasClass('position-0') == true){
                                        $(".chat-box").removeClass('position-0')
                                    }
                                });
                                chatHeightFunc(chatHeight);
                                // console.log('action button fade in');
                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            }
        }
    }

    function thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, prevLoopMedia, JsonID){
        if(thirdID == lastIndex){
            console.log('near end of chat (3)');
            for (let cc = secondID; cc < lastIndex; cc++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[cc];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log("loop-3");
                    if(cc == thirdID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+thirdID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (3)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > secondID && firstBrowseAudio < thirdID){
                console.log('%c Audio Found (Loop 3) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = secondID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')

                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })

                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                // after confirm button
                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio');

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        let AudioFileName = data;
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name);
                        // console.log('success change audio src', AudioFileName);
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < thirdID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();

                            let chatHeight = $(".justify-content-start").height();

                            if(b == thirdID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > secondID && firstBrowseVideo < thirdID){
                console.log('%c Video Found (Loop 3) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = secondID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < thirdID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == thirdID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > secondID && firstBrowsePicture < thirdID){
                console.log('%c Picture Found (Loop 3) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = secondID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < thirdID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == thirdID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 3) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let c = secondID; c < thirdID; c++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[c];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();
                        if(c == thirdID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").removeClass('displayFlex').addClass('hide');
                                    $(".choices").removeClass("hide").addClass('displayBlock');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayBlock');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            }
        }
    }

    function fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, prevLoopMedia, JsonID){
        if(fourthID == lastIndex){
            console.log('near end of chat (4)');
            for (let dd = thirdID; dd < lastIndex; dd++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[dd];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log('loop-4');
                    if(dd == fourthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+fourthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (4)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > thirdID && firstBrowseAudio < fourthID){
                console.log('%c Audio Found (Loop 4) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = thirdID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        // console.log('audio media', data);
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < fourthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == fourthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > thirdID && firstBrowseVideo < fourthID){
                console.log('%c Video Found (Loop 4) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = thirdID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < fourthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == fourthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > thirdID && firstBrowsePicture < fourthID){
                console.log('%c Picture Found (Loop 4) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = thirdID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        // console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < fourthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();
                            console.log('data', dataPosition.id);
                            if(b == fourthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 4) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let d = thirdID; d < fourthID; d++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[d];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();
                        if(d == fourthID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").removeClass('displayFlex').addClass('hide');
                                    $(".choices").removeClass("hide").addClass('displayBlock');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayBlock');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            }
        }
    }

    function fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
        if(fivethID == lastIndex){
            console.log('near end of chat (5)');
            for (let ee = fourthID; ee < lastIndex; ee++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ee];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log("loop-5");
                    if(ee == fivethID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+fivethID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (5)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > fourthID && firstBrowseAudio < fivethID){
                console.log('%c Audio Found (Loop 5) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = fourthID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        // console.log('audio media', data);
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < fivethID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == fivethID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > fourthID && firstBrowseVideo < fivethID){
                console.log('%c Video Found (Loop 5) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = fourthID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1')
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video');
                    
                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < fivethID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == fivethID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > fourthID && firstBrowsePicture < fivethID){
                console.log('%c Picture Found (Loop 5) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = fourthID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        // console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < fivethID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();
                            console.log('data', dataPosition.id);
                            if(b == fivethID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 5) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let e = fourthID; e < fivethID; e++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[e];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();

                        if(e == fivethID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").removeClass('displayFlex').addClass('hide');
                                    $(".choices").removeClass("hide").addClass('displayBlock');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayBlock');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });

                }
            }
        }
    }

    function sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
        if(sixthID == lastIndex){
            console.log('near end of chat (6)');
            for (let ee = fivethID; ee < lastIndex; ee++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ee];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log("loop-6");
                    if(ee == sixthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+sixthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayFlex');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (6)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > fivethID && firstBrowseAudio < sixthID){
                console.log('%c Audio Found (Loop 6) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = fivethID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        // console.log('audio media', data);
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < sixthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == sixthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > fivethID && firstBrowseVideo < sixthID){
                console.log('%c Video Found (Loop 6) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = fivethID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < sixthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == sixthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > fivethID && firstBrowsePicture < sixthID){
                console.log('%c Picture Found (Loop 6) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = fivethID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        // console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < sixthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();
                            console.log('data', dataPosition.id);
                            if(b == sixthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 6) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let e = fivethID; e < sixthID; e++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[e];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();

                        if(e == sixthID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".choices").removeClass("hide").addClass('displayFlex').addClass("displayBlock");
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayFlex');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            }
        }
    }

    function seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID) {
        if(seventhID == lastIndex){
            console.log('near end of chat (7)');
            for (let ff = sixthID; ff < lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    if(ff == seventhID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+seventhID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (7)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > sixthID && firstBrowseAudio < seventhID){
                console.log('%c Audio Found (Loop 7) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = sixthID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        // console.log('audio media', data);
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < seventhID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == seventhID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > sixthID && firstBrowseVideo < seventhID){
                console.log('%c Video Found (Loop 7) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = sixthID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < seventhID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == seventhID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > sixthID && firstBrowsePicture < seventhID){
                console.log('%c Picture Found (Loop 7) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = sixthID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        // console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < seventhID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();
                            console.log('data', dataPosition.id);
                            if(b == seventhID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 7) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let f = sixthID; f < seventhID; f++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[f];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();

                        if(f == seventhID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".choices").removeClass("hide").addClass('displayFlex');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayBlock');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });

                }
            }
        }
    }

    function eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
        if(eighthID == lastIndex){
            console.log('near end of chat (8)');
            for (let ff = seventhID; ff < lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log("loop-8");
                    if(ff == eighthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+eighthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (8)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > seventhID && firstBrowseAudio < eighthID){
                console.log('%c Audio Found (Loop 8) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = seventhID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        // console.log('audio media', data);
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < eighthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == eighthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > seventhID && firstBrowseVideo < eighthID){
                console.log('%c Video Found (Loop 8) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = seventhID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < eighthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == eighthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > seventhID && firstBrowsePicture < eighthID){
                console.log('%c Picture Found (Loop 8) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = seventhID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        // console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < eighthID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();
                            console.log('data', dataPosition.id);
                            if(b == eighthID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 8) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let f = seventhID; f < eighthID; f++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[f];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();

                        if(f == eighthID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".choices").removeClass("hide").addClass('displayFlex');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayBlock');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });

                }
            }
        }
    }

    function ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
        if(ninethID == lastIndex){
            console.log('near end of chat (9)');
            for (let ff = eighthID; ff < lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();
                    console.log("loop-9 (near end)");
                    if(ff == ninethID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+ninethID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();

                            console.log('option 9 appear (near end)');
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue chat (9)');
            console.log('=================');

            if(window.localStorage.getItem('audioMediaDetected') !== null){
                firstBrowseAudio = 0;
            }

            if(window.localStorage.getItem('videoMediaDetected') !== null){
                firstBrowseVideo = 0;
            }

            if(window.localStorage.getItem('pictureMediaDetected') !== null){
                firstBrowsePicture = 0;
            }

            console.log('firstBrowseAudio',firstBrowseAudio);
            console.log('firstBrowseVideo',firstBrowseVideo);
            console.log('firstBrowsePicture',firstBrowsePicture);

            if(firstBrowseAudio > eighthID && firstBrowseAudio < ninethID){
                console.log('%c Audio Found (Loop 9) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = eighthID; m < firstBrowseAudio; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseAudio-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_audio;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").addClass('displayFlex').removeClass('hide');
                                    $(".btn-browse").addClass('browse_audio_file');
                                })
                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_audio_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_audio_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse audio after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_audio', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_audio").addClass('hide')
                    $(".btn-browse").removeClass('browse_audio_file');
                    $('.offline').removeClass('hide');
                    $(".back_audio").removeClass('btn_back_audio_1');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_audio')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <audio controls src="" class="media__audio" style="width: 225px;">
                                Your browser does not support the
                                <code>audio</code> element.
                            </audio>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_audio').appendTo("#chat-box-container");

                    $.get('/check-media-audio/'+JsonID, (data) => {
                        // console.log('audio media', data);
                        $(".media__audio").attr('src', 'https://narrator.kaigangames.com/multimedia/audio/'+data[0].audio_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseAudio; b < ninethID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == ninethID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveAudio');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        } else {
                                            $(".chat-box").css({
                                                'top': '-26%'
                                            });
                                        }
                                    });

                                    let audioMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('audioMediaDetected', JSON.stringify(audioMedia));

                                    chatHeightFunc(chatHeight);
                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowseVideo > eighthID && firstBrowseVideo < ninethID){
                console.log('%c Video Found (Loop 9) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = eighthID; m < firstBrowseVideo; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowseVideo-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_video;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_video_file');
                                    $(".control_browse ").removeClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_video_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_video_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                })

                $(".control_select").on("click", '.btn_select_media_video', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_video").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_video").removeClass('btn_back_video_1');
                    $(".btn-browse").removeClass('browse_video_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_video')

                    // append video
                    var appendVideo =
                    `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <video width="75%" height="100%" controls>
                                    <source src="" class="media__video" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    `;

                    $(appendVideo).find('video').attr('src', 'https://narrator.kaigangames.com/check-media-video/'+JsonID).end().appendTo("#chat-box-container").addClass('chat_media_video');

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowseVideo; b < ninethID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();

                            if(b == ninethID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('haveVideo');
                                        // $(".chat-box").removeClass("position-0");

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let videoMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('videoMediaDetected', JSON.stringify(videoMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else if(firstBrowsePicture > eighthID && firstBrowsePicture < ninethID){
                console.log('%c Picture Found (Loop 9) ', 'color: blue; font-weight: bold; background-color: aqua;');
                for (let m = eighthID; m < firstBrowsePicture; m++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        dataPosition = chatData[m];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        //play sound
                        playNotificationSound();
                        if(m == firstBrowsePicture-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let mediaData = dataPosition.browse_picture;
                                $.each(mediaData, (i, val) => {
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".browse").css('display', 'flex');
                                    $(".btn-browse").addClass('browse_picture_file');
                                    $('.browse').addClass('displayFlex').removeClass('hide');
                                    $('.offline').addClass('hide');
                                })
                                playOptionsSound();
                            })
                        }

                        //call the function
                        scrollChat();
                    });
                }

                //when click back button
                $(".control_select").on("click", '.btn_back_picture_1',() => {
                    $('.justify-content-start').removeClass('hide')
                    $('.justify-content-end').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
    
                    $('.control_browse').css('display', 'flex');
                    $('.browse_media').css('display', 'none');
                })
    
                $('.browse').on('click', '.browse_picture_file', () => {            
                    $('.browse_media').css('display', 'block');
                    $('.control_browse').css('display', 'none');
                    console.log('browse picture after click back button');
                })

                $(".control_select").on("click", '.btn_select_media_picture', () => {
                    $('.justify-content-start').removeClass('hide')
                    $(".justify-content-end").removeClass('hide')
                    $(".media_picture").addClass('hide')
                    $(".online").removeClass('displayFlex').addClass('hide')
                    $('.browse').addClass('hide').removeClass('displayFlex');
                    $('.offline').removeClass('hide');
                    $(".back_picture").removeClass('btn_back_picture_1');
                    $(".btn-browse").removeClass('browse_picture_file');
                    $(".control_select").addClass('hide').css('display', '');
                    $('.btn-media').removeClass('btn_select_media_picture')

                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <img src="" class="w-50 media__picture" alt="Pic">
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_media_picture').appendTo("#chat-box-container");

                    //check media picture
                    $.get('/check-media-picture/'+JsonID, (data) => {
                        // console.log('picture media', data);
                        $(".media__picture").attr('src', 'https://narrator.kaigangames.com/multimedia/pictures/'+data[0].picture_name)
                    })

                    //after the btn_back_audio_1 clicked, continue the chat until reach the option
                    for (let b = firstBrowsePicture; b < ninethID; b++) {
                        typingAnimation(dataPosition.time_milisecond,)
                        $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                            let dataPosition = chatData[b];
                            // append chat
                            var appendQuestion =
                            `
                            <div class="justify-content-start">
                                <div class="col-12 pl-0 pr-0">
                                    <div class="message-wrapper">
                                        <div class="message">
                                            <div class="text">
                                                <span class="player-name">`+author+`</span><br>
                                                <p class="mt-0 mb-0 message">
                                                    `+dataPosition.message+`
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                            var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                            playNotificationSound();
                            let chatHeight = $(".justify-content-start").height();
                            console.log('data', dataPosition.id);
                            if(b == ninethID-1){
                                $(".action-button").delay(500).fadeIn(() => {
                                    $(".chat-box").addClass('move-top')
                                    let optionData = dataPosition.options;
                                    // append the options btn
                                    $.each(optionData, (i, val) => {
                                        options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                        $("#imessage").append(options);
                                        $(".browse").addClass('hide');
                                        $(".online").addClass("displayFlex").removeClass('hide');
                                        $(".offline").addClass('hide').removeClass("displayFlex");
                                        $(".choices").removeClass('hide').css('display', 'block');
                                        $(".opt-btn").addClass('option_'+dataPosition.id).addClass('havePicture');

                                        if($(".chat-box").hasClass('position-0') == true){
                                            $(".chat-box").removeClass('position-0')
                                        }
                                    });
                                    chatHeightFunc(chatHeight);
                                    // console.log('action button fade in');
                                    $(".chat-box").css({
                                        'top': '-26%'
                                    });

                                    let pictureMedia = {
                                        position: dataPosition.id,
                                        status: true
                                    }
                                    //save media to localStorage
                                    window.localStorage.setItem('pictureMediaDetected', JSON.stringify(pictureMedia));

                                    playOptionsSound();
                                })
                            }
                            //call the function
                            scrollChat();
                        });
                    }
                })
            } else {
                console.log('%c No Media Found (Loop 9) ', 'color: aqua; font-weight: bold; background-color: grey;');
                for (let f = eighthID; f < ninethID; f++) {
                    typingAnimation(dataPosition.time_milisecond)
                    $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                        let dataPosition = chatData[f];
                        // append chat
                        var appendQuestion =
                        `
                        <div class="justify-content-start">
                            <div class="col-12 pl-0 pr-0">
                                <div class="message-wrapper">
                                    <div class="message">
                                        <div class="text">
                                            <span class="player-name">`+author+`</span><br>
                                            <p class="mt-0 mb-0 message">
                                                `+dataPosition.message+`
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                        playNotificationSound();

                        if(f == ninethID-1){
                            $(".action-button").delay(500).fadeIn(() => {
                                let optionData = dataPosition.options;
                                // append the options btn
                                $.each(optionData, (i, val) => {
                                    options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                    $("#imessage").append(options);
                                    $(".online").removeClass('hide').addClass("displayFlex");
                                    $(".offline").addClass('hide').removeClass('displayFlex');
                                    $(".choices").removeClass("hide").addClass('displayFlex');
                                    $(".opt-btn").addClass('option_'+dataPosition.id);

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                        $(".choices").removeClass('displayBlock');
                                    })

                                    $(".online").on('click', ()=>{
                                        $(".choices").toggle();
                                    })
                                });

                                $(".chat-box").css({
                                    'top': '-26%'
                                });

                                playOptionsSound();
                            })
                        }
                        //call the function
                        scrollChat();
                    });
                }
            }
        }
    }

    function tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength, firstBrowseAudio, firstBrowseVideo, firstBrowsePicture, mediaType, JsonID){
        if(tenthID == lastIndex){
            console.log('%c No Media Found (Loop 10) ', 'color: aqua; font-weight: bold; background-color: grey;');
            console.log('end of chat (10)');
            for (let ff = ninethID; ff <= lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                    playNotificationSound();

                    if(ff == tenthID){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $("#imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").addClass('hide').removeClass('displayFlex');
                                $(".choices").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('optionEnd_'+tenthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayFlex');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('this the maximum option');
        }
    }

    function lastMessage(dataPosition, chatData, author, lastIndex, chatLength){
        for (let i = lastIndex+1; i < chatLength; i++) {
            typingAnimation(dataPosition.time_milisecond);
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                let dataPosition = chatData[i];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo("#chat-box-container");
                playNotificationSound();
                if(dataPosition.id == chatLength-1){
                    console.log('end here (10)');
                    $(".eoc-wrapper").show();
                    $('.offline').removeClass('displayFlex').addClass('hide');
                }

                scrollChat();
            })

            $(".chat-box").fadeIn().css({
                top: '-3px'
            });
            //scroll chat
            scrollChat();
        }
    }

    //typing animation
    function typingAnimation(time_milisecond){
        $(".typing").css({
            "display" : 'flex',
            "margin-bottom" : '5px'
        })

        $(".col-typing-wrapper").removeClass('hide').delay(time_milisecond).fadeIn("normal").delay(time_milisecond).fadeOut();
    }

    //scroll function
    function scrollChat(){
        $(".chat-box").stop().animate({ scrollTop: $(".chat-box")[0].scrollHeight}, 1000);
    }

    function chatHeightFunc(chatHeight){
        // return chatHeight;
        let minHeight = chatHeight * 8;

        $(".chat-box").removeClass('move-top').css({
            'position': 'relative',
            'top': minHeight+'px !important'
        })
    }

    $('.sound-icon a').on('click', () =>{
        let option = $('.option_popup');
        let reply = $('.option_reply');
        let notif = $('.chat_notif');

        $(notif).prop('muted', false);
        $(reply).prop('muted', false);
        $(option).prop('muted', false);

        $('.s-icon').toggleClass('fa-volume-up fa-volume-mute');

        if($(".fa-volume-up").is(":visible")){
            // console.log('have sound');
        } else {
            if($(notif).prop('muted', false) && $(reply).prop('muted', false) && $(option).prop('muted', false)){
                // console.log('muted');
                muteOptionSound();
            }
        }
    })

    //browse audio file. After click back to loop (before back button clicked) and continue the chat until option appeared
    $('.browse').on('click', '.browse_audio_file', () => {
        $('.justify-content-start').addClass('hide')
        $('.justify-content-end').addClass('hide')
        $(".media_audio").removeClass('hide')
        $(".back_audio").addClass('btn_back_audio_1')
        $(".choices").addClass('hide')
        $('.control_browse').removeClass('displayFlex')
        $(".chat-box").addClass('position-0')
        $('.browse').removeClass('browse_audio_file')
        $('.btn-media').addClass('btn_select_media_audio')

        //check class for media, hide media picture
        if($(".media_picture").hasClass('hide') == true){
            //next
        } else {
            $(".media_picture").addClass('hide');
        }

        //check class for media, hide media video
        if($(".media_video").hasClass('hide') == true){
            //next
        } else {
            $(".media_video").addClass('hide');
        }
    })

    //browse video file
    $(".browse").on('click', '.browse_video_file', () => {
        $('.justify-content-start').addClass('hide')
        $('.justify-content-end').addClass('hide')
        $(".media_video").removeClass('hide')
        $(".back_video").addClass('btn_back_video_1')
        $(".choices").addClass('hide')
        $('.control_browse').removeClass('displayFlex').css('display', 'none')
        $(".chat-box").addClass('position-0')
        $('.browse').removeClass('browse_video_file')
        $('.btn-media').addClass('btn_select_media_video')
        $(".confirm_select_video_item").removeClass('hide');
        $(".btn-select-video_confirm_lg_md").addClass('displayBlock');

        //check class for media, hide media audio
        if($(".media_audio").hasClass('hide') == true){
            //next
        } else {
            $(".media_audio").addClass('hide');
        }

        //check class for media, hide media picture
        if($(".media_picture").hasClass('hide') == true){
            //next
        } else {
            $(".media_picture").addClass('hide');
        }
    })

    //browse picture file
    $(".browse").on('click', '.browse_picture_file', () => {
        $('.justify-content-start').addClass('hide')
        $('.justify-content-end').addClass('hide')
        $(".media_picture").removeClass('hide')
        $(".media_video").addClass('hide')
        $(".back_picture").addClass('btn_back_picture_1')
        $('.browse').removeClass('browse_picture_file')
        $('.control_browse').removeClass('displayFlex').css('display', 'none')
        $('.btn-media').addClass('btn_select_media_picture')
        $(".confirm_select_picture_item").removeClass('hide');
        $(".btn-select-picture_confirm_lg_md").addClass('displayBlock');

        //check class for media, hide media audio
        if($(".media_audio").hasClass('hide') == true){
            //next
        } else {
            $(".media_audio").addClass('hide');
        }

        //check class for media, hide media video
        if($(".media_video").hasClass('hide') == true){
            //next
        } else {
            $(".media_video").addClass('hide');
        }
    })

    //confirm button
    $(".btn-select-audio_confirm_lg_md").on('click', () => {
        let isChecked = $('.selected_audio').is(":checked");

        if (isChecked) {
            $(".select_item").css('display', 'flex')

            //disabled back button when confirm checkbox is checkced
            if($('.select_item').is(':visible')){
                $('.back_audio').prop('disabled', true);
            }
        } else {
            $(".select_item").css('display', 'none')

            //disabled back button when confirm checkbox is checkced
            if($('.select_item').is(':hidden')){
                $('.back_audio').prop('disabled', false);
            }
        }
    })

    $(".btn-select-video_confirm_lg_md").on('click', () => {
        let isCheckedVideo = $(".selected_video").is(":checked");

        if (isCheckedVideo) {
            $(".select_item").css('display', 'flex').removeClass('hide')

            //disabled back button when confirm checkbox is checkced
            if($('.select_item').is(':visible')){
                $('.back_video').prop('disabled', true);
            }
        } else {
            $(".select_item").css('display', 'none')

            //disabled back button when confirm checkbox is checkced
            if($('.select_item').is(':hidden')){
                $('.back_video').prop('disabled', false);
            }
        }
    })

    $(".btn-select-picture_confirm_lg_md").on('click', () => {
        let isCheckedPicture = $(".selected_picture").is(":checked");

        if (isCheckedPicture) {
            $(".select_item").css('display', 'flex').removeClass('hide');

            //disabled back button when confirm checkbox is checkced
            if($('.select_item').is(':visible')){
                $('.back_picture').prop('disabled', true);
            }
        } else {
            $(".select_item").css('display', 'none');

            //disabled back button when confirm checkbox is checkced
            if($('.select_item').is(':hidden')){
                $('.back_picture').prop('disabled', false);
            }
        }
    })
})
$( window ).bind('beforeunload', function(){
   console.log('It is going to be refreshed');
   window.localStorage.clear();
});

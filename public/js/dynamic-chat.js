// console.log('dymanic chat enabled');
function playNotificationSound(){
    let notif = $('.chat_notif');
    notif.get(0).play();
}

function playReplySound(){
    let reply = $('.option_reply');
    reply.get(0).play();
}

function playOptionsSound(){
    let option = $('.option_popup');
    option.get(0).play();
}

function muteOptionSound(){
    let option = $('.option_popup');
    let reply = $('.option_reply');
    let notif = $('.chat_notif');

    $(notif).prop('muted', true);
    $(reply).prop('muted', true);
    $(option).prop('muted', true);
}

$('.play-dynamic-chat').on('click', function(){
    $("#play-dynamic-chat").prop('disabled', true);
    let JSONname = $(this).data('name')
    let JsonID = $(this).data('jsonid');

    $(".content-wrapper, .hr-wrapper, .phone-row").addClass('hide');
    $(".dynamic-chat-field").removeClass('hide');

    $.get("/play-json/"+JSONname, (data) => {
        // console.log(data);
        let fileName = data[0].filename;
        let author = data[0].author;
        let chatLength = Object.keys(data[0].chat).length;
        let chatData = data[0].chat;
        var dataPosition, haveOptions;
        var arrOPtion = [];
        var arrOptionID = [];
        var lastIndex;
        //for media
        var BrowsePicture, BrowseOption, BrowseOptionID, pictureIndex;
        var getIndex;
        var arrPics  = [];

        //get data with "options" key
        for (let i = 0; i < chatLength; i++) {
            dataPosition = chatData[i];
            haveOptions = dataPosition.hasOwnProperty("options");
            if(haveOptions == true){
                arrOPtion.push(dataPosition.id);
                BrowseOption = arrOPtion;
            }

            //mutimedia files
            image_media = dataPosition.hasOwnProperty('image_media');
            //check if the chat have multimedia file
            if(image_media == true){
                arrPics.push(dataPosition.id);
                //get data ID
                BrowsePicture = arrPics;
            }
        }

        //check for option_id
        for (let i = 0; i < chatLength; i++) {
            dataPosition = chatData[i];
            haveOptions = dataPosition.hasOwnProperty("option_id");
            if(haveOptions == true){
                arrOptionID.push(dataPosition.id);
                BrowseOptionID = arrOptionID;
                arrOptionLength = BrowseOptionID.length;
            }
        }
        lastIndex = arrOPtion.pop();

        //apend author data
        $(".author-append").html(author);
        
        firstLoop(BrowseOption, BrowsePicture, chatData, author, lastIndex, chatLength, BrowseOptionID, arrOptionLength);
        // console.log('last index', lastIndex);
        // console.log('chat data',chatData);
        console.log('ini',chatData[chatData.length-1]);
        //start index
        var getIndex = BrowseOption.shift();

        $(document).on("click", ".optionIndex_"+getIndex,function() {
            var clickData = $(this).data('selected');
            var clickDataOptionIndex = $(this).data('option_id_index');
            var clickDataIndex = $(this).data('index')
            $(".append-options").empty();
            var appendReply = 
                `
                <div class="col-lg-12 title-section mt-2 mb-2">
                    <div class='row'>
                        <div class="col-lg-1 col-md-1 col-sm-2 col-2">
                            <img src="https://images.ctfassets.net/hrltx12pl8hq/5zMNRtdFXh9wkSmmZdMnuX/58d92197f39712d70d493411d67a4514/abstract-shutterstock_117556003.jpg?fit=fill&w=1024&h=683&fm=webp" class="circle-shadow-2">
                        </div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-10">
                            <div class="col-lg-12 pl-0">
                                You choose the option:
                            </div>
                            <div class="user-reply">
                                <p>`+clickData+`</p>
                            </div>
                        </div>
                    </div>
                </div>
                `;

            $(appendReply).addClass('user_reply').addClass('reply_'+clickDataOptionIndex).appendTo(".dynamic-message-wrapper");

            BrowsePicture.shift();
            nextLoop(chatData, clickDataOptionIndex, chatLength, BrowseOption);
        })

        $(document).on("click", ".next-response", function(){
            let browseIDData = $(this).data("boi");
            let clickData = $(this).data("clickoi");
            let selectedData = $(this).data("selected");
            let browseIDData_1 = browseIDData-1;
            // console.log('data boi', browseIDData_1);

            $(".dynamic-user-options-append").addClass('hide');
            $(".append-user-option-message").empty();
            $(".append-options").empty();
            var appendReply = 
                `
                <div class="col-lg-12 title-section mt-2 mb-2">
                    <div class='row'>
                        <div class="col-lg-1 col-md-1 col-sm-2 col-2">
                            <img src="https://images.ctfassets.net/hrltx12pl8hq/5zMNRtdFXh9wkSmmZdMnuX/58d92197f39712d70d493411d67a4514/abstract-shutterstock_117556003.jpg?fit=fill&w=1024&h=683&fm=webp" class="circle-shadow-2">
                        </div>
                        <div class="col-lg-11 col-md-11 col-sm-10 col-10">
                            <div class="col-lg-12 pl-0">
                                You choose the option:
                            </div>
                            <div class="user-reply">
                                <p>`+selectedData+`</p>
                            </div>
                        </div>
                    </div>
                </div>
                `;

            $(appendReply).addClass('user_reply').addClass('reply_'+clickData).appendTo(".dynamic-message-wrapper");
            scrollChat();
            //reset position
            $(".dynamic-message-wrapper").css('top', '0');
            //next loop
            nextLoop(chatData, clickData, chatLength, BrowseOption, browseIDData_1);
        })
    });
});

//loop function
function firstLoop(BrowseOption, BrowsePicture, chatData, author, lastIndex, chatLength, BrowseOptionID, arrOptionLength){
    //check if first loop have image or not
    if(BrowsePicture == null || BrowsePicture == ''){
        for (let i = 0; i < BrowseOption[0]; i++) {
            timeData = chatData[i].time_milisecond;
            typingAnimation(timeData)
            $(".dynamic-message").removeClass('hide').delay(timeData).fadeIn("normal").delay(timeData).fadeOut(() => {
                textData = chatData[i];
                
                if(textData.message != ""){
                    appendChatData =
                    `
                    <div class="justify-content-start dynamic_chat">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-title mb-0" style="font-size: 0.9rem;">
                                    <span class="badge badge-info">
                                    `+textData.player+`
                                    </span>
                                </h6>
                                <div class="card-text">
                                    `+textData.message+`
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    $(appendChatData).addClass('chat_'+textData.id).appendTo(".dynamic-message-wrapper");
                    //play sound
                    playNotificationSound();
                }

                if(textData.hasOwnProperty("options")){
                    $(".action-button").delay(500).fadeIn(() => {
                        let optionData = textData.options;
                        $.each(optionData, (i, val) => {
                            listData = `
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-sm mt-2 next-response optionIndex_${BrowseOption[0]} optionIDIndex_${val.id}" data-boi="`+BrowseOption[0]+`" data-clickOI="${val.id}" data-selected="`+val.message+`">`+val.message+`</button>
                                        </div>
                                        `;
                            $(".append-user-option-message").append(listData);
                            $(".dynamic-user-options-append").removeClass('hide');
                        });
                        
                        playOptionsSound();
                    })
                }
            });
        }
    } else {
        var currentPictureIndex = BrowsePicture[0];
        pictureIndex = [];
        var dataChat, timeData, textData, appendChatData, appendImage;
        if(BrowsePicture[0] == lastIndex){
            console.log('last');
        } else {
            // console.log('browseop', BrowseOption);
            for (let index = 0; index < BrowseOption[0]; index++) {
                dataChat = chatData[index];
                havePicture = dataChat.hasOwnProperty("image_media");
                if(havePicture == true){
                    pictureIndex.push(dataChat.id);

                    for (let i = 0; i < pictureIndex; i++) {
                        timeData = chatData[i].time_milisecond;
                        typingAnimation(timeData)
                        $(".dynamic-message").removeClass('hide').delay(timeData).fadeIn("normal").delay(timeData).fadeOut(() => {
                            textData = chatData[i];
                            
                            if(textData.message != ""){
                                appendChatData =
                                `
                                <div class="justify-content-start dynamic_chat">
                                    <div class="card">
                                        <div class="card-body">
                                            <h6 class="card-title mb-0" style="font-size: 0.9rem;">
                                                <span class="badge badge-info">
                                                `+textData.player+`
                                                </span>
                                            </h6>
                                            <div class="card-text">
                                                `+textData.message+`
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                $(appendChatData).addClass('chat_'+textData.id).appendTo(".dynamic-message-wrapper");
                                //play sound
                                playNotificationSound();
                            }

                            //append picture
                            if(i == currentPictureIndex-1){
                                // console.log('pic', i);
                                // console.log('pic data', textData.image_media[0].file_path);
                                appendImage =
                                `
                                <div class="justify-content-start dynamic_chat">
                                    <div class="col-12 pl-0 pr-0">
                                        <img src="${textData.image_media[0].file_path}" class="w-25 media__picture" alt="Picture Media">
                                    </div>
                                </div>
                                `;
                                $(appendImage).addClass('chat_'+textData.id).addClass('chat_media_picture').appendTo(".dynamic-message-wrapper");
                                $(".chat_media_picture").attr('src', '');
                                //play sound
                                playNotificationSound();
                            }
                        });
                    }
                }
            }

            for (let next = currentPictureIndex; next < BrowseOption[0]; next++) {
                timeData = chatData[next].time_milisecond;
                typingAnimation(timeData);
                $(".dynamic-message").removeClass('hide').delay(timeData).fadeIn("normal").delay(timeData).fadeOut(() => {
                    var textData2 = chatData[next];
                    appendChatData =
                    `
                    <div class="justify-content-start dynamic_chat">
                        <div class="card">
                            <div class="card-body">
                            <h6 class="card-title mb-0" style="font-size: 0.9rem;">
                                <span class="badge badge-info">
                                `+textData2.player+
                                `</span>
                            </h6>
                                <div class="card-text">
                                    `+textData2.message+`
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    $(appendChatData).addClass('chat_'+textData2.id).appendTo(".dynamic-message-wrapper");
                    //play sound
                    playNotificationSound();
                    //get append chat height
                    let chatHeight = document.querySelector(".dynamic_chat").offsetHeight;
                    
                    if(textData2.hasOwnProperty("options")){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = textData2.options;
                            $.each(optionData, (i, val) => {
                                listData = `
                                            <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sm mt-2 next-response optionIndex_${BrowseOption[0]} optionIDIndex_${val.id}" data-boi="`+BrowseOption[0]+`" data-clickOI="${val.id}" data-selected="`+val.message+`">`+val.message+`</button>
                                            </div>
                                            `;
                                $(".append-user-option-message").append(listData);
                                $(".dynamic-user-options-append").removeClass('hide');
                            });
                            playOptionsSound();
                        })
                    }
                });
            }
        }
    }
}

function nextLoop(chatData, clickDataOptionIndex, chatLength, BrowseOption, browseIDData_1){
    if(BrowseOption[0] == chatLength){
        console.log("end");
    } else {

        //shift array
        BrowseOption.shift();
        var results = [];
        var searchField = "option_id";
        var searchVal = clickDataOptionIndex;
        // console.log('click index', searchVal);
        for (let i=0 ; i < chatData.length ; i++)
        {
            if (chatData[i][searchField] == searchVal) {
                results.push(chatData[i]);
            }
        }
        
        //append
        results.forEach(element => {
            let timeData = element.time_milisecond;
            typingAnimation(timeData);
            $(".dynamic-message").removeClass('hide').delay(timeData).fadeIn("normal").delay(timeData).fadeOut(() => {                
                appendChatData =
                `
                <div class="justify-content-start dynamic_chat">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="card-title mb-0" style="font-size: 0.9rem;">
                                <span class="badge badge-info">
                                `+element.player+`
                                </span>
                            </h6>
                            <div class="card-text">
                                `+element.message+`
                            </div>
                        </div>
                    </div>
                </div>
                `;
                $(appendChatData).addClass('chat_'+element.id).appendTo(".dynamic-message-wrapper");
                //play sound
                playNotificationSound();
                scrollChat();
                //get append chat height
                let chatHeight = document.querySelector(".dynamic_chat").offsetHeight;
        
                if(element.hasOwnProperty("options")){
                    $(".action-button").delay(timeData).fadeIn(() => {
                        let optionData = element.options;
                        // append the options btn
                        $.each(optionData, (i, val) => {
                            listData = `
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-sm mt-2 next-response optionIndex_${BrowseOption[0]} optionIDIndex_${val.id}" data-boi="`+BrowseOption[0]+`" data-clickOI="${val.id}" data-selected="`+val.message+`">`+val.message+`</button>
                                        </div>
                                        `;
                            $(".append-user-option-message").append(listData);
                            $(".dynamic-user-options-append").removeClass('hide');
                        });
                        playOptionsSound();
                        scrollChat();
                    })
                }

                if(element.hasOwnProperty('image_media')){
                    //don'directly append the image. Adjust the order with the json id (ascending)
                    appendImage =
                        `
                        <div class="justify-content-start dynamic_chat">
                            <div class="col-12 pl-0 pr-0">
                                <img src="${element.image_media[0].file_path}" class="w-25 media__picture" alt="Pic">
                            </div>
                        </div>
                        `;
                    $(appendImage).addClass('chat_media_picture').appendTo(".dynamic-message-wrapper");
                    $(".chat_media_picture").attr('src', '');
                    //play sound
                    playNotificationSound();
                    scrollChat();
                    //get append chat height
                    let chatHeight = document.querySelector(".dynamic_chat").offsetHeight;
                }
            });
        });
    }
}

//scroll function
function scrollChat(){
    $('.dynamic-message-wrapper').animate({scrollTop: $('.dynamic-message-wrapper').get(0).scrollHeight}, 1000); 
}

//typing animation
function typingAnimation(time_milisecond){
    $(".typing").css({
        "display" : 'flex',
        "margin-bottom" : '5px'
    })

    $(".col-typing-wrapper").removeClass('hide').delay(time_milisecond).fadeIn("normal").delay(time_milisecond).fadeOut();
}
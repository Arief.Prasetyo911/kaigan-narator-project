$(document).ready(() => {
    function detectScreenRes(){
        const width = Math.max(
            document.documentElement.clientWidth,
            window.innerWidth || 0
        );

        const height = Math.max(
            document.documentElement.clientHeight,
            window.innerHeight || 0
        )

        if (width <= 768){
            $('.process-select-sm').prop('disabled', true);
            $('footer').removeClass('mt-auto').css('margin-top', '2.5rem');

            window.addEventListener("orientationchange", function() {
                // alert("the orientation of the device is now " + screen.orientation.angle);
                if(screen.orientation.angle == 0){
                    console.log('enter portrait mode');
                    $('.btn-play-xs, .btn-play-md').prop('disabled', false);
                } else {
                    console.log('enter landscape mode');
                    $('.btn-play-xs, .btn-play-md').prop('disabled', true);
                }
            });
        }
    }

    function pixelRatio(){
        let pr = window.devicePixelRatio;
        console.log('device pixel ratio', pr);
    }

    //call the function
    detectScreenRes();
    pixelRatio();
    
    if (window.matchMedia("(orientation: landscape)").matches) {
        $('.btn-play-xs, .btn-play-md').prop('disabled', true);
        $('.landcape-alert').html('Sorry, the JSON only can played in <strong>Portrait Mode</strong>')
    }
});

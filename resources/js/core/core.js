$(document).ready(() => {
    console.log('%cCore Logic loaded', 'color: burlywood; font-weight: bold;');
    console.info('%cApp Ready', 'color: green; font-weight: bold;');
    //clear screen
    $(".col-typing-wrapper").addClass('hide');
    $(".col-message-wrapper").addClass('hide');

    $('#example').DataTable();

    $('.justify-content-start').animate({
        scrollTop: $('.chat-box').get(0).scrollHeight
    }, 1500);

    $(".control").on('click', () => {
        $(".choices").slideToggle( "normal", function() {
            if($(this).is(":visible")){
                $(".chat-box").fadeIn().css({
                    position: 'relative'
                }).animate({top: '-26%'}, 500);
            }
            else {
                $(".chat-box").fadeIn().css({
                    position: 'relative',
                }).animate({top: '-10px'}, 500);
            }
        });
    });

    $('.btn-play').on('click', function(ev){
        $(".btn-play").prop('disabled', true);
        let JSONname = $(this).data('name')
        // console.log('button play clicked', JSONname);
        $.get("/play-json/"+JSONname, (data) => {
            let fileName = data[0].filename;
            let author = data[0].author;
            let chatLength = Object.keys(data[0].chat).length;
            let chatData = data[0].chat;
            var dataPosition, haveOptions;
            var arrOPtion = [];
            var lastIndex;
            var firstID, secondID, thirdID, fourthID, fivethID, sixthID, seventhID, eighthID, ninethID, tenthID = new Array();

            //top phone
            $(".chat-group-title").empty();
            $('.chat-group-title').append(fileName+".json");
            $('.mt-user-text').empty();
            $(".mt-user-text").append( `Online - `+ author);

            //get data with "options" key
            for (let i = 0; i < chatLength; i++) {
                dataPosition = chatData[i];
                haveOptions = dataPosition.hasOwnProperty("options");
                if(haveOptions == true){
                    arrOPtion.push(dataPosition.id);
                    //get data ID
                    firstID = arrOPtion[0];
                    secondID = arrOPtion[1];
                    thirdID = arrOPtion[2];
                    fourthID = arrOPtion[3];
                    fivethID = arrOPtion[4];
                    sixthID = arrOPtion[5];
                    seventhID = arrOPtion[6];
                    eighthID = arrOPtion[7];
                    ninethID = arrOPtion[8];
                    tenthID = arrOPtion[9];
                }
            }

            lastIndex = arrOPtion.pop();
            console.log('last index at', lastIndex);
            console.log('chat length', chatLength);

            //call first loop
            firstLoop(firstID, dataPosition, chatData, author, lastIndex);

            // when click first option
            //=======================
                $(document).on("click", '.optionEnd_'+firstID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(firstID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (1)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (1)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 2nd round
                $(document).on("click", '.option_'+firstID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+firstID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);

                        playReplySound();
                    });
                    secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex);
                });
            //====================

            // when click 2nd option
            //====================
                $(document).on("click", '.optionEnd_'+secondID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(secondID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (2)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (2)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 3rd round
                $(document).on("click", '.option_'+secondID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+secondID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                        scrollChat();
                    });
                    thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex);
                });
            //====================

            // when click 3rd option
            //====================
                $(document).on("click", '.optionEnd_'+thirdID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(thirdID != chatLength){
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (3)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (3)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 4th round
                $(document).on("click", '.option_'+thirdID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+thirdID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex);
                })
            //====================

            // when click 4th option
            //====================
                $(document).on("click", '.optionEnd_'+fourthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(fourthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (4)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (4)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+fourthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+fourthID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex)
                })
            //====================

            //when click 5th option
            //====================
                $(document).on("click", '.optionEnd_'+fivethID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(fivethID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);


                                if(dataPosition.id == chatLength){
                                    console.log('end here (5)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (5)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+fivethID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+fivethID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayBlock").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex);
                });
            //====================

            //when click 6th option
            //====================
                $(document).on("click", '.optionEnd_'+sixthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(sixthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (6)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (6)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+sixthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+sixthID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex)
                });
            //====================

            //when click 7th option
            //====================
                $(document).on("click", '.optionEnd_'+seventhID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(seventhID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (7)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (7)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+seventhID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+seventhID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide').removeClass("displayBlock");
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex)
                });
            //====================

            //when click 8th option
            //====================
                $(document).on("click", '.optionEnd_'+eighthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(eighthID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (8)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (8)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+eighthID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+eighthID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex)
                });
            //====================

            //when click 9th option
            //====================
                $(document).on("click", '.optionEnd_'+ninethID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(ninethID != chatLength){
                        console.log('still continue');
                        for (let i = lastIndex; i < chatLength; i++) {
                            $(".col-typing-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut();
                            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                                let dataPosition = chatData[i];
                                // append chat
                                var appendQuestion =
                                `
                                <div class="justify-content-start">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="message-wrapper">
                                            <div class="message">
                                                <div class="text">
                                                    <span class="player-name">`+author+`</span><br>
                                                    <p class="mt-0 mb-0 message">
                                                        `+dataPosition.message+`
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                `;
                                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");

                                $(".chat-box").fadeIn().css({
                                    position: 'relative',
                                    top: '-3px'
                                }).animate({top: '-3px'}, 500);

                                if(dataPosition.id == chatLength){
                                    console.log('end here (9)');
                                    $(".eoc-wrapper").show();
                                }
                            })
                        }
                    } else {
                        console.log('end here (9)');
                        $(".eoc-wrapper").show();
                    }
                });

                //displayed choosen option to screen and run 5th round
                $(document).on("click", '.option_'+ninethID, function(){
                    //display selected option
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    $(".typing").hide().fadeOut(()=> {
                        //append message
                        var appendAnswer =
                            `
                            <div class="row pr5 justify-content-end">
                                <div class="col-8 pl-0 pr-0">
                                    <div class="reply float-right">
                                        <div class="text text-left">
                                            `+clickedBtn+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                        `;
                        var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo(".chat-box-container");

                        $(".online").removeClass('displayFlex').addClass("hide");
                        $(".offline").addClass('displayFlex').removeClass('hide');
                        $(".choices").removeClass("displayFlex").addClass('hide');
                        $(".chat-box").fadeIn().css({
                            position: 'relative',
                            top: '-3px'
                        }).animate({top: '-3px'}, 500);
                        playReplySound();
                    });
                    tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength)
                    // console.log("Last loop");
                });
            //====================

            //when click 10th option
            //====================
                $(document).on("click", '.optionEnd_'+tenthID, function(){
                    var clickedBtn = $(this).data('click');
                    //empty action-box
                    $(".imessage").empty();
                    if(tenthID != chatLength){
                        //display selected option
                        var clickedBtn = $(this).data('click');
                        //empty action-box
                        $(".imessage").empty();
                        $(".typing").hide().fadeOut(()=> {
                            //append message
                            var appendAnswer =
                                `
                                <div class="row pr5 justify-content-end">
                                    <div class="col-8 pl-0 pr-0">
                                        <div class="reply float-right">
                                            <div class="text text-left">
                                                `+clickedBtn+`
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                            var append = $(appendAnswer).addClass('chat_'+ninethID).appendTo(".chat-box-container");

                            $(".online").removeClass('displayFlex').addClass("hide");
                            $(".offline").addClass('displayFlex').removeClass('hide');
                            $(".choices").removeClass("displayFlex").addClass('hide');
                            $(".chat-box").fadeIn().css({
                                position: 'relative',
                                top: '-3px'
                            }).animate({top: '-3px'}, 500);
                            playReplySound();
                        });

                        //disini
                        lastMessage(dataPosition, chatData, author, lastIndex, chatLength);

                    } else {
                        console.log('end here (10)');
                        $(".eoc-wrapper").show();
                    }
                });
            //====================
        })
    });

})

$(document).ready(() => {
    function playNotificationSound(){
        let notif = $('.chat_notif');
        notif.get(0).play();
    }

    function playReplySound(){
        let reply = $('.option_reply');
        reply.get(0).play();
    }

    function playOptionsSound(){
        let option = $('.option_popup');
        option.get(0).play();
        console.log('option appear');
    }

    function muteOptionSound(){
        let option = $('.option_popup');
        let reply = $('.option_reply');
        let notif = $('.chat_notif');

        $(notif).prop('muted', true);
        $(reply).prop('muted', true);
        $(option).prop('muted', true);
        // console.log('chat sound muted');
    }

    $('.sound-icon a').click(() =>{
        let option = $('.option_popup');
        let reply = $('.option_reply');
        let notif = $('.chat_notif');

        $(notif).prop('muted', false);
        $(reply).prop('muted', false);
        $(option).prop('muted', false);

        $('.s-icon').toggleClass('fa-volume-up fa-volume-mute');

        if($(".fa-volume-up").is(":visible")){
            // console.log('have sound');
        } else {
            if($(notif).prop('muted', false) && $(reply).prop('muted', false) && $(option).prop('muted', false)){
                // console.log('muted');
                muteOptionSound();
            }
        }
    })
})

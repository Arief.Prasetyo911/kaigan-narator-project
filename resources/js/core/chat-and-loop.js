$(document).ready(() => {
    //loop function
    function firstLoop(firstID, dataPosition, chatData, author, lastIndex){
        //first loop
        if(firstID == lastIndex){
            console.log('near end of chat (1)');
            for (let aa = 0; aa < lastIndex; aa++) {
                // console.log(aa);
                typingAnimation(dataPosition.time_milisecond);
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[aa];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    //play sound
                    playNotificationSound();
                    console.log("loop-1");
                    // console.log('pos loop 1',dataPosition);
                    if(aa == firstID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").addClass("displayFlex");
                                $(".offline").addClass('hide');
                                $(".choices").css('display', 'block');
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+firstID);
                                $(".control_online").addClass('control_1st');
                            });
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (1)');
            for (let a = 0; a < firstID; a++) {
                // console.log('a',a );
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    dataPosition = chatData[a];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    //play sound
                    playNotificationSound();
                    console.log("loop-1");
                    if(a == firstID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").addClass("displayFlex");
                                $(".offline").addClass('hide');
                                $(".choices").css('display', 'block');
                                $(".opt-btn").addClass('option_'+dataPosition.id);
                                $(".control").addClass('control_1st');
                            });
                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }

    function secondLoop(firstID, secondID, dataPosition, chatData, author, lastIndex){
        if(secondID == lastIndex){
            console.log('near end of chat (2)');
            for (let bb = firstID; bb < lastIndex; bb++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[bb];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-2");
                    if(bb == secondID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").addClass("displayFlex").removeClass('hide');
                                $(".offline").addClass('hide').removeClass("displayFlex");
                                $(".choices").removeClass('hide').addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+secondID);
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (2)');
            for (let b = firstID; b < secondID; b++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[b];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-2");
                    let chatHeight = $(".justify-content-start").height();

                    if(b == secondID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            $(".chat-box").addClass('move-top')
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").addClass("displayFlex").removeClass('hide');
                                $(".offline").addClass('hide').removeClass("displayFlex");
                                $(".choices").removeClass('hide').addClass('block', () => {
                                    $(".chat-box").fadeIn().animate({top: '-26%'}, 500);
                                });
                                $(".opt-btn").addClass('option_'+dataPosition.id);
                            });
                            chatHeightFunc(chatHeight);
                            // console.log('action button fade in');
                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }

    function thirdLoop(secondID, thirdID, dataPosition, chatData, author, lastIndex){
        if(thirdID == lastIndex){
            console.log('near end of chat (3)');
            for (let cc = secondID; cc < lastIndex; cc++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[cc];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-3");
                    if(cc == thirdID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+thirdID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (3)');
            for (let c = secondID; c < thirdID; c++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[c];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-3");
                    if(c == thirdID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide").addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }

    function fourthLoop(thirdID, fourthID, dataPosition, chatData, author, lastIndex){
        if(fourthID == lastIndex){
            console.log('near end of chat (4)');
            for (let dd = thirdID; dd < lastIndex; dd++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[dd];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log('loop-4');
                    if(dd == fourthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+fourthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (4)');
            for (let d = thirdID; d < fourthID; d++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[d];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log('loop-4');
                    if(d == fourthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide").addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }

    function fivethLoop(fourthID, fivethID, dataPosition, chatData, author, lastIndex){
        if(fivethID == lastIndex){
            console.log('near end of chat (5)');
            for (let ee = fourthID; ee < lastIndex; ee++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ee];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-5");
                    if(ee == fivethID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+fivethID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (5)');
            for (let e = fourthID; e < fivethID; e++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[e];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-5");
                    if(e == fivethID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide").addClass('displayBlock');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }

    function sixthLoop(fivethID, sixthID, dataPosition, chatData, author, lastIndex){
        if(sixthID == lastIndex){
            console.log('near end of chat (6)');
            for (let ee = fivethID; ee < lastIndex; ee++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ee];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-6");
                    if(ee == sixthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+sixthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayFlex');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (6)');
            for (let e = fivethID; e < sixthID; e++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[e];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-6");
                    if(e == sixthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").addClass('hide').removeClass('displayFlex');
                                $(".choices").removeClass("hide").addClass('displayFlex').addClass("displayBlock");
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayFlex');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }

    function seventhLoop(sixthID, seventhID, dataPosition, chatData, author, lastIndex) {
        if(seventhID == lastIndex){
            console.log('near end of chat (7)');
            for (let ff = sixthID; ff < lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-7");
                    if(ff == seventhID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+seventhID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (7)');
            for (let f = sixthID; f < seventhID; f++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[f];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-7");
                    if(f == seventhID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").addClass('hide').removeClass('displayFlex');
                                $(".choices").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }

    function eigthLoop(seventhID, eighthID, dataPosition, chatData, author, lastIndex){
        if(eighthID == lastIndex){
            console.log('near end of chat (8)');
            for (let ff = seventhID; ff < lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-8");
                    if(ff == eighthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+eighthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (8)');
            for (let f = seventhID; f < eighthID; f++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[f];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-8");
                    if(f == eighthID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").addClass('hide').removeClass('displayFlex');
                                $(".choices").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();
                        })
                    }
                    //call the function
                    scrollChat();
                });

            }
        }
    }

    function ninethLoop(eighthID, ninethID,  dataPosition, chatData, author, lastIndex){
        if(ninethID == lastIndex){
            console.log('near end of chat (9)');
            for (let ff = eighthID; ff < lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-9 (near end)");
                    if(ff == ninethID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").removeClass('displayFlex').addClass('hide');
                                $(".choices").removeClass("hide");
                                $(".opt-btn").addClass('option_'+dataPosition.id).addClass('optionEnd_'+ninethID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();

                            console.log('option 9 appear (near end)');
                        })
                    }

                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('continue (9)');
            for (let f = eighthID; f < ninethID; f++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[f];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-9");
                    if(f == ninethID-1){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").addClass('hide').removeClass('displayFlex');
                                $(".choices").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('option_'+dataPosition.id);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayBlock');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();

                            console.log('option 9 appear');
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        }
    }

    function tenthLoop(ninethID,  tenthID, dataPosition, chatData, author, lastIndex, chatLength){
        if(tenthID == lastIndex){
            console.log('end of chat (10)');
            for (let ff = ninethID; ff <= lastIndex; ff++) {
                typingAnimation(dataPosition.time_milisecond)
                $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(() => {
                    let dataPosition = chatData[ff];
                    // append chat
                    var appendQuestion =
                    `
                    <div class="justify-content-start">
                        <div class="col-12 pl-0 pr-0">
                            <div class="message-wrapper">
                                <div class="message">
                                    <div class="text">
                                        <span class="player-name">`+author+`</span><br>
                                        <p class="mt-0 mb-0 message">
                                            `+dataPosition.message+`
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                    playNotificationSound();
                    console.log("loop-10 (end)");
                    if(ff == tenthID){
                        $(".action-button").delay(500).fadeIn(() => {
                            let optionData = dataPosition.options;
                            // append the options btn
                            $.each(optionData, (i, val) => {
                                options = `<button type="button" class="btn btn-light btn-block opt-btn" data-click="`+val.message+`">`+val.message+`</button>`;
                                $(".imessage").append(options);
                                $(".online").removeClass('hide').addClass("displayFlex");
                                $(".offline").addClass('hide').removeClass('displayFlex');
                                $(".choices").removeClass("hide").addClass('displayFlex');
                                $(".opt-btn").addClass('optionEnd_'+tenthID);

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                    $(".choices").removeClass('displayFlex');
                                })

                                $(".online").on('click', ()=>{
                                    $(".choices").toggle();
                                })
                            });

                            $(".chat-box").css({
                                'top': '-26%'
                            });

                            playOptionsSound();

                            console.log('option 10 appear');
                        })
                    }
                    //call the function
                    scrollChat();
                });
            }
        } else {
            console.log('this the maximum option');
        }
    }

    //typing animation
    function typingAnimation(time_milisecond){
        // $(".col-typing-wrapper").removeClass('hide').delay(10000).fadeIn("normal").delay(10000).fadeOut();
        $(".typing").css({
            "display" : 'flex',
            "margin-bottom" : '5px'
        })

        $(".col-typing-wrapper").removeClass('hide').delay(time_milisecond).fadeIn("normal").delay(time_milisecond).fadeOut();
    }

    function lastMessage(dataPosition, chatData, author, lastIndex, chatLength){
        for (let i = lastIndex+1; i < chatLength; i++) {
            typingAnimation(dataPosition.time_milisecond);
            $(".col-message-wrapper").removeClass('hide').delay(dataPosition.time_milisecond).fadeIn("normal").delay(dataPosition.time_milisecond).fadeOut(()=>{
                let dataPosition = chatData[i];
                // append chat
                var appendQuestion =
                `
                <div class="justify-content-start">
                    <div class="col-12 pl-0 pr-0">
                        <div class="message-wrapper">
                            <div class="message">
                                <div class="text">
                                    <span class="player-name">`+author+`</span><br>
                                    <p class="mt-0 mb-0 message">
                                        `+dataPosition.message+`
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;
                var append = $(appendQuestion).addClass('chat_'+dataPosition.id).appendTo(".chat-box-container");
                playNotificationSound();
                if(dataPosition.id == chatLength-1){
                    console.log('end here (10)');
                    $(".eoc-wrapper").show();
                }

                scrollChat();
            })
            console.log('last message', i);
            $(".chat-box").fadeIn().css({
                top: '-3px'
            });
            //scroll chat
            scrollChat();
        }
    }
    //scroll function
    function scrollChat(){
        $(".chat-box").stop().animate({ scrollTop: $(".chat-box")[0].scrollHeight}, 1000);
        console.log('scroll');
    }

    function chatHeightFunc(chatHeight){
        // return chatHeight;
        let minHeight = chatHeight * 8;

        $(".chat-box").removeClass('move-top').css({
            'position': 'relative',
            'top': minHeight+'px !important'
        })

        // console.log('chatheiight called', minHeight);
    }
})

@extends('back-end.main.app')
@section('title')
    Dashboard
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="fas fa-hashtag"></i>
                    </div>
                    <div>Dashboard</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Popular Story</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Rank</th>
                                        <th>JSON File</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Have Media</th>
                                        <th>Visited (times)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1?>
                                    @foreach ($popularStory1 as $item)
                                        @if ($item->color == 'gold')
                                            <tr class="gold">
                                                <td class="text-center">
                                                    <i class="fas fa-crown"></i>
                                                    <span class="top-rank">{{$no++}}</span>
                                                </td>
                                                <td>{{$item->json_name}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->author}}</td>
                                                <td>{{$item->have_media}}</td>
                                                <td>{{$item->counter}}</td>
                                            </tr>
                                        @elseif($item->color == 'silver')
                                            <tr class="silver">
                                                <td class="text-center">
                                                    <i class="fas fa-crown"></i>
                                                    <span class="top-rank">{{$no++}}</span>
                                                </td>
                                                <td>{{$item->json_name}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->author}}</td>
                                                <td>{{$item->have_media}}</td>
                                                <td>{{$item->counter}}</td>
                                            </tr>
                                        @else
                                            <tr class="bronze">
                                                <td class="text-center">
                                                    <i class="fas fa-crown"></i>
                                                    <span class="top-rank">{{$no++}}</span>
                                                </td>
                                                <td>{{$item->json_name}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->author}}</td>
                                                <td>{{$item->have_media}}</td>
                                                <td>{{$item->counter}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    @foreach ($popularStory4 as $item)
                                    <tr class="second-row">
                                        <td class="text-center text-muted">
                                            <i class="fas fa-certificate"></i>
                                            <span class="mid-rank">{{$no++}}</span>
                                        </td>
                                        <td>{{$item->json_name}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->author}}</td>
                                        <td>{{$item->have_media}}</td>
                                        <td>{{$item->counter}}</td>
                                    </tr>
                                    @endforeach
                                    @foreach ($popularStory10 as $item)
                                    <tr class="normal">
                                        <td class="text-center text-muted">{{$no++}}</td>
                                        <td>{{$item->json_name}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->author}}</td>
                                        <td>{{$item->have_media}}</td>
                                        <td>{{$item->counter}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Your Story</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Rank</th>
                                        <th>JSON File</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Have Media</th>
                                        <th>Visited (times)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1?>
                                    @foreach ($myStory as $item)
                                        @if ($item->color == 'gold')
                                            <tr class="gold">
                                                <td class="text-center">
                                                    <i class="fas fa-crown"></i>
                                                    <span class="top-rank">{{$no++}}</span>
                                                </td>
                                                <td>{{$item->json_name}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->author}}</td>
                                                <td>{{$item->have_media}}</td>
                                                <td>{{$item->counter}}</td>
                                            </tr>
                                        @elseif($item->color == 'silver')
                                            <tr class="silver">
                                                <td class="text-center">
                                                    <i class="fas fa-crown"></i>
                                                    <span class="top-rank">{{$no++}}</span>
                                                </td>
                                                <td>{{$item->json_name}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->author}}</td>
                                                <td>{{$item->have_media}}</td>
                                                <td>{{$item->counter}}</td>
                                            </tr>
                                        @elseif(($item->color == 'bronze'))
                                            <tr class="bronze">
                                                <td class="text-center">
                                                    <i class="fas fa-crown"></i>
                                                    <span class="top-rank">{{$no++}}</span>
                                                </td>
                                                <td>{{$item->json_name}}</td>
                                                <td>{{$item->title}}</td>
                                                <td>{{$item->author}}</td>
                                                <td>{{$item->have_media}}</td>
                                                <td>{{$item->counter}}</td>
                                            </tr>
                                        @else
                                        <tr class="normal">
                                            <td class="text-center">
                                                <i class="fas fa-crown"></i>
                                                <span class="top-rank">{{$no++}}</span>
                                            </td>
                                            <td>{{$item->json_name}}</td>
                                            <td>{{$item->title}}</td>
                                            <td>{{$item->author}}</td>
                                            <td>{{$item->have_media}}</td>
                                            <td>{{$item->counter}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                All Rights Reserved &copy; {{date('Y')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://kaigangames.com" class="nav-link" target="__blank">
                                Kaigan Games Entertainment SDN BHD
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(() => {
            $('.table-hover').DataTable();
        })
    </script>
@endsection
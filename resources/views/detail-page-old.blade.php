@extends('layouts.master')
@section('main_content')
<div class="container detail-page mt-4">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 content-wrapper">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-12 phone-wrapper">
                <div class="card">
                    <div class="col-12 pl-0 pr-0">
                        <img class="card-img-top" src="{{asset('images/'.$detailData->image_name)}}" alt="Card image cap">
                        <h6 class="card-title mb-0">{{str_replace('_', ' ', $detailData->title)}}</h6>
                    </div>
                </div>
                <div class="col-12 text-center mt-3">
                    <div class="d-block d-sm-none show-xs">
                        <button class="btn btn-outline-success btn-sm process-select-xs btn-play-xs" data-name="{{$detailData->json_name}}" data-jsonid="{{$detailData->json_id}}">
                            <i class="far fa-play-circle"></i> Play XS
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-6 col-12 text-content">
                <div class="title-description">
                    <h5 class="text-white">{{$detailData->title}}</h5>
                    <h6 class="text-white">
                        {{$detailData->description}}
                    </h6>
                </div>
                <div class="author-date mt-4">
                    <h6 class="text-white mb-0">
                        Author : {{$detailData->author}}
                    </h6>
                    <h6 class="text-white mb-0">
                        Date : {{$carbonDate}}
                    </h6>
                </div>
                <div class="mt-4 float-left">
                    <div class="d-none d-lg-block d-xl-block show-large">
                        @if ($detailData->reply_type == 'dynamic')
                            <button class="btn btn-outline-success btn-block " id="play-dynamic-chat" data-name="{{$detailData->json_name}}" data-jsonid="{{$detailData->id}}">
                                <i class="far fa-play-circle"></i> Play
                            </button>
                        @else
                            <button class="btn btn-outline-success btn-block btn-play-lg" data-name="{{$detailData->json_name}}" data-jsonid="{{$detailData->id}}">
                                <i class="far fa-play-circle"></i> Play
                            </button>
                        @endif
                    </div>
                    <div class="d-none d-md-block d-lg-none show-md">
                        <button class="btn btn-outline-success btn-sm btn-block process-select-md btn-play-md" data-name="{{$detailData->json_name}}" data-jsonid="{{$detailData->id}}">
                            <i class="far fa-play-circle"></i> Play
                        </button>
                    </div>
                    <div class="d-none d-sm-block d-md-none show-sm">
                        <div class="col-sm-12 text-justify pl-0 pr-0">
                            <button class="btn btn-outline-success btn-sm btn-block process-select-sm btn-play-xs" data-name="{{$detailData->json_name}}" data-jsonid="{{$detailData->id}}">
                                <i class="far fa-play-circle"></i> Play
                            </button>

                            <div class="alert alert-info mt-3 landcape-alert text-center" role="alert">                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 hr-wrapper">
        <hr>
    </div>
    {{-- phone image --}}
    <div class="row phone-row hide">
        {{-- non xs --}}
        <div class="col-lg-12 col-md-12 col-sm-12 phone-ui text-center d-xl-block d-lg-block mt-3">
            <div class="marvel-device iphone-x">
                <div class="notch">
                    <div class="camera"></div>
                    <div class="speaker"></div>
                </div>
                <div class="top-bar"></div>
                <div class="sleep"></div>
                <div class="bottom-bar"></div>
                <div class="volume"></div>
                <div class="overflow">
                    <div class="shadow shadow--tr"></div>
                    <div class="shadow shadow--tl"></div>
                    <div class="shadow shadow--br"></div>
                    <div class="shadow shadow--bl"></div>
                </div>
                <div class="inner-shadow"></div>
                <div class="screen">
                    <!-- Content goes here -->
                    <div class="title-box">
                        <div class="col-lg-12 col-12">
                            <div class="row mt-2em">
                                <div class="col-lg-3 col-md-3 col-sm-4 col-4 pl-0 pr-0 user-icon">
                                    <img src="{{asset('assets/images/chat.png')}}" class="w-50" alt="Chat Avatar">
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-6 col-6 pl-0 pr-0 middle-text">
                                    <div class="col-lg-12 pr-0 pl-0">
                                        <h5 class="chat-group-title">JSON File</h5>
                                    </div>
                                    <div class="col-lg-12 pr-0">
                                        <div class="row">
                                            <span class="dot"></span>
                                            <p class="mb-0 mt-user-text">
                                                Online - Author
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 pl-0 pr-0 sound-icon">
                                    <a href="javascript:void(0)">
                                        <i class="fas fa-volume-up s-icon"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="chat-box">
                        <audio src="{{asset('sound/chat_notify.ogg')}}" type="audio/ogg" class="chat_notif"></audio>
                        <div class="container chat-box-container pl-0 pr-0" id="chat-box-container">
                            {{-- media --}}
                            <div class="browse_media">

                                <div class="media_audio hide">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-8 mt-2">
                                                <h5 class="mb-0">Audio Files</h5>
                                            </div>
                                        </div>
                                        <hr class="mt-1">
                                        <div class="col-lg-12 col-md-12 pl-0 pr-0">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <audio controls src="" class="media__audio__blade">
                                                        Your browser does not support the
                                                        <code>audio</code> element.
                                                    </audio>
                                                </div>
                                                <div class="col-lg-12 d-flex justify-content-center">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input selected_audio" id="audio_checkbox">
                                                        <label class="custom-control-label" for="audio_checkbox">Select</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- select button --}}
                                        <div class="row control_select confirm_select_audio_item mt-3">
                                            <div class="col-lg-12 col-12">
                                                <div class="d-flex justify-content-center">
                                                    <div class="col-lg-5 col-5">
                                                        <button class="btn btn-info btn-sm btn-block btn-select-audio_confirm_lg_md" type="button">
                                                            Confirm
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-5 col-5">
                                                        <button type="button" class="btn btn-warning btn-sm btn-block back_audio">
                                                            Cancel / Back
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="media_picture hide">
                                    <div class="col-lg-12 col-md-12 col-12">
                                        <div class="row">
                                            <div class="col-8">
                                                <h5 class="mb-0">Picture Files</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="mt-1">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <img src="" class="w-100 media__picture__blade" alt="Pic">
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input selected_picture" id="picture_checkbox">
                                                <label class="custom-control-label" for="picture_checkbox">Select</label>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- select button --}}
                                    <div class="row control_select confirm_select_picture_item pl-1 pr-1">
                                        <div class="col-lg-12 col-12">
                                            <div class="d-flex justify-content-center">
                                                <div class="col-lg-5 col-5">
                                                    <button class="btn btn-info btn-sm btn-block btn-select-picture_confirm_lg_md" type="button">
                                                        Confirm
                                                    </button>
                                                </div>
                                                <div class="col-lg-5 col-5">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block back_picture">
                                                        Cancel / Back
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="media_video hide">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="row">
                                            <div class="col-8">
                                                <h5 class="mb-0">Video Files</h5>
                                            </div>
                                        </div>
                                        <hr class="mt-1">
                                        @if ($detailData->have_media == 'yes')
                                            <video  width="100%" height="100%" controls>
                                                <source src="{{ route('checkMediaVideo', $detailVideo->json_id)  }}" class="media__video__blade" type="video/mp4">
                                            </video>
                                        @else
                                            <video  width="100%" height="100%" controls>
                                                <source src="" class="media__video__blade" type="video/mp4">
                                            </video>
                                        @endif
                                        <div class="d-flex justify-content-center mt-2">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input selected_video" id="video_checkbox">
                                                <label class="custom-control-label" for="video_checkbox">Select</label>
                                            </div>
                                        </div>
                                        {{-- select button --}}
                                        <div class="row control_select confirm_select_video_item pl-1 pr-1">
                                            <div class="col-lg-12 col-12">
                                                <div class="d-flex justify-content-center">
                                                    <div class="col-lg-5 col-5">
                                                        <button class="btn btn-info btn-sm btn-block btn-select-video_confirm_lg_md" type="button">
                                                            Confirm
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-5 col-5">
                                                        <button type="button" class="btn btn-warning btn-sm btn-block back_video">
                                                            Cancel / Back
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                {{-- append chat here --}}
                                <div class="col-message-wrapper" id="col-message-wrapper">
                                </div>
                            </div>
                        </div>
                        {{-- typing animation --}}
                        <div class="typing-animation">
                            {{-- typing animation will appended here --}}
                            <div class="justify-content-start">
                                <div class="col-12 col-typing-wrapper">
                                    <div class="typing-wrapper">
                                        <div class="row typing">
                                            <p class="typing-time mb-0 font-weight-bold">Typing</p>
                                            <div id="wave">
                                                <span class="dot"></span>
                                                <span class="dot"></span>
                                                <span class="dot"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 eoc-wrapper">
                            <div class="row justify-content-md-center">
                                <div class="col col-lg-4 col-md-4 eoc">
                                    End of Chat
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="action-button col-lg-12 pr-0 pl-0">
                        <audio src="{{asset('sound/chat_select.ogg')}}" type="audio/ogg" class="option_popup"></audio>
                        <audio src="{{asset('sound/chat_reply.ogg')}}" type="audio/ogg" class="option_reply"></audio>
                        <div class="row control control_online online">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" placeholder="Choose a reply" aria-label="Search" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-outline-success btn-block disabled" type="button">
                                    <img src="{{asset('assets/phone-assets/mes_chatbar_online_sendicon.png')}}" class="w-73" alt="Send">
                                </button>
                            </div>
                        </div>
                        <div class="row control control_offline offline">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" value="Offline" aria-label="Search" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-secondary btn-block disabled border-0" type="button">
                                    <img src="{{asset('assets/phone-assets/mes_chatbar_offline_sendicon_2.png')}}" class="w-23" alt="Send">
                                </button>
                            </div>
                        </div>
                        <div class="row control control_browse browse">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" value="Browse File ..." aria-label="Search" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-outline-success btn-block disabled border-0 btn-browse" type="button">
                                    <i class="far fa-folder-open"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row control control_select select_item">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" value="Send File" aria-label="Select" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-outline-success btn-block disabled border-0 btn-media" type="button">
                                    <i class="far fa-hand-pointer"></i>
                                </button>
                            </div>
                        </div>
                        {{-- append here --}}
                        <div class="choices">
                            <div class="header row" id="choice-btn-append">
                                <div class="col-2">
                                    <i class="far fa-comment-dots"></i>
                                </div>
                                <div class="col-10 pl-0">
                                    <p class="mb-0 float-left">Choose a reply</p>
                                </div>
                                <div class="col-12">
                                    <hr>
                                </div>
                                <div class="col-12 imessage" id="imessage">
                                    {{-- append here --}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="back-button">
                        <div class="col-lg-12">
                            <hr class="back">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- for XS only --}}
        <div class="visible-only-xs">
            <div class="iphone-xs-xs">
                <div class="title-box-xs">
                    <div class="col-12 col-12">
                        <div class="row mt-2em">
                            <div class="col-md-2 col-sm-3 col-3 pl-0 pr-0 user-icon">
                                <img src="{{asset('assets/images/chat.png')}}" class="w-50" alt="Chat Avatar">
                            </div>
                            <div class="col-md-8 col-sm-7 col-7 pl-0 pr-0 middle-text">
                                <div class="col-12 pr-0 pl-0">
                                    <h5 class="chat-group-title">JSON File</h5>
                                </div>
                                <div class="col-12 pr-0">
                                    <div class="row">
                                        <span class="dot"></span>
                                        <p class="mb-0 mt-user-text">
                                            Online - Author
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2 col-md-2 col-sm-2 col-2 pl-0 pr-0 sound-icon-xs">
                                <a href="javascript:void(0)">
                                    <i class="fas fa-volume-up s-icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="chat-box-xs" id="chat-box-xs">
                    <audio src="{{asset('sound/chat_notify.ogg')}}" type="audio/ogg" class="chat_notif_xs"></audio>
                    <div class="container chat-box-container-xs pl-0 pr-0" id="chat-box-container-xs">
                        {{-- media --}}
                        <div class="browse_media_xs">
                            <div class="media_audio_xs hide">
                                <div class="col-12">
                                    <div class="row mt-2">
                                        <div class="col-8">
                                            <h5 class="mb-0">Audio Files</h5>
                                        </div>
                                        <div class="col-4"></div>
                                    </div>
                                    <hr class="mt-1">
                                    <div class="col-12 pl-0 pr-0">
                                        <div class="row">
                                            <div class="col-9">
                                                <audio controls src="" class="media__audio__blade w-100">
                                                    Your browser does not support the
                                                    <code>audio</code> element.
                                                </audio>
                                            </div>
                                            <div class="col-3 pl-0">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input selected_audio" id="audio_checkbox_xs">
                                                    <label class="custom-control-label" for="audio_checkbox_xs">Select</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row control_select confirm_select_audio_item-xs mt-2">
                                        <div class="col-sm-12 col-12">
                                            <div class="d-flex justify-content-center">
                                                <div class="col-sm-5 col-5">
                                                    <button class="btn btn-info btn-sm btn-block btn-select-audio_confirm" type="button">
                                                        Confirm
                                                    </button>
                                                </div>
                                                <div class="col-sm-5 col-5">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block back_audio">
                                                        Cancel / Back
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="media_picture_xs hide">
                                <div class="col-lg-12 col-12">
                                    <div class="row mt-2">
                                        <div class="col-8">
                                            <h5 class="mb-0">Picture Files</h5>
                                        </div>
                                        <div class="col-4"></div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                                <div class="row">
                                    <div class="col-6">
                                        <img src="" class="w-100 media__picture__blade" alt="Pic">
                                    </div>
                                    <div class="col-6">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input selected_picture" id="picture_checkbox_xs">
                                            <label class="custom-control-label" for="picture_checkbox_xs">Select</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row control_select confirm_select_picture_item-xs mt-3">
                                    <div class="col-sm-12 col-12">
                                        <div class="d-flex justify-content-center">
                                            <div class="col-sm-5 col-5">
                                                <button class="btn btn-info btn-sm btn-block btn-select-picture_confirm" type="button">
                                                    Confirm
                                                </button>
                                            </div>
                                            <div class="col-sm-5 col-5">
                                                <button type="button" class="btn btn-warning btn-sm btn-block back_picture">
                                                    Cancel / Back
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="media_video_xs hide">
                                <div class="col-12">
                                    <div class="row mt-2">
                                        <div class="col-8">
                                            <h5 class="mb-0">Video Files</h5>
                                        </div>
                                        <div class="col-4"></div>
                                    </div>
                                    <hr class="mt-1">
                                    @if ($detailData->have_media == 'yes')
                                        <video  width="100%" height="100%" controls>
                                            <source src="{{ route('checkMediaVideo', $detailVideo->json_id)  }}" class="media__video__blade" type="video/mp4">
                                        </video>
                                    @else
                                        <video  width="100%" height="100%" controls>
                                            <source src="" class="media__video__blade" type="video/mp4">
                                        </video>
                                    @endif
                                    <div class="d-flex justify-content-center mt-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input selected_video" id="video_checkbox_xs">
                                            <label class="custom-control-label" for="video_checkbox_xs">Select</label>
                                        </div>
                                    </div>
                                    <div class="row control_select confirm_select_video_item-xs mt-3">
                                        <div class="col-sm-12 col-12">
                                            <div class="d-flex justify-content-center">
                                                <div class="col-sm-5 col-5">
                                                    <button class="btn btn-info btn-sm btn-block btn-select-video_confirm" type="button">
                                                        Confirm
                                                    </button>
                                                </div>
                                                <div class="col-sm-5 col-5">
                                                    <button type="button" class="btn btn-warning btn-sm btn-block back_video">
                                                        Cancel / Back
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            {{-- append chat here --}}
                            <div class="col-message-wrapper-xs" id="col-message-wrapper-xs">
                            </div>
                        </div>
                    </div>

                    {{-- typing animation --}}
                    <div class="typing-animation-xs">
                        {{-- typing animation will appended here --}}
                        <div class="justify-content-start">
                            <div class="col-12 col-typing-wrapper-xs">
                                <div class="typing-wrapper-xs">
                                    <div class="row typing-xs">
                                        <p class="typing-time mb-0">Typing</p>
                                        <div id="wave">
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                            <span class="dot"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 eoc-wrapper-xs d-flex justify-content-center mt-3">
                        <div class="row">
                            <div class="col col-lg-4 eoc">
                                End of Chat
                            </div>
                        </div>
                    </div>
                </div>

                <div class="action-button-wrapper-xs" id="action-button-wrapper-xs">
                    <div class="action-button-xs col-lg-12 pr-0 pl-0">
                        <audio src="{{asset('sound/chat_select.ogg')}}" type="audio/ogg" class="option_popup_xs"></audio>
                        <audio src="{{asset('sound/chat_reply.ogg')}}" type="audio/ogg" class="option_reply_xs"></audio>
                        <div class="row control_online online-xs">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" placeholder="Choose a reply" aria-label="Search" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-outline-success btn-block disabled" type="button">
                                    <img src="{{asset('assets/phone-assets/mes_chatbar_online_sendicon.png')}}" class="w-73" alt="Send">
                                </button>
                            </div>
                        </div>
                        <div class="row control_offline offline-xs">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" value="Offline" aria-label="Search" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-secondary btn-block disabled border-0" type="button">
                                    <img src="{{asset('assets/phone-assets/mes_chatbar_offline_sendicon_2.png')}}" class="w-23" alt="Send">
                                </button>
                            </div>
                        </div>
                        <div class="row control_browse browse-xs">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" value="Browse File ..." aria-label="Search" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-outline-success btn-block disabled border-0 btn-browse" type="button">
                                    <i class="far fa-folder-open"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row control_select select_item-xs">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-9 pr-0">
                                <input class="form-control" type="search" value="Send File" aria-label="Select" disabled>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 pl-0">
                                <button class="btn btn-outline-success btn-block border-0 btn-media" type="button">
                                    <i class="far fa-hand-pointer"></i>
                                </button>
                            </div>
                        </div>
                        {{-- append here --}}
                        <div class="choices-xs">
                            <div class="header row" id="choice-btn-append">
                                <div class="col-2 pr-0">
                                    <i class="far fa-comment-dots"></i>
                                </div>
                                <div class="col-10 pl-0">
                                    <p class="mb-0 float-left">Choose a reply</p>
                                </div>
                                <div class="col-12">
                                    <hr>
                                </div>
                                <div class="col-12 imessage-xs" id="imessage-xs">
                                    {{-- append here --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="back-button-xs">
                    <div class="col-lg-12">
                        <hr class="back">
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- for dynamic Chat --}}
    <div class="dynamic-chat-field hide">
        <div class="row">
            <div class="col-lg-10 col-10">
                <h4>
                    <i class="fas fa-hashtag"></i>
                    Welcome to {{Request::segment(3)}}
                </h4>
            </div>
            <div class="col-lg-2 col-12 sound-icon">
                <a href="javascript:void(0)">
                    <i class="fas fa-volume-up s-icon"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row title-section mt-4 mb-2">
                <div class="col-lg-1 pr-0">
                    <img src="{{asset('assets/narrator-img.png')}}" class="circle-shadow">
                </div>
                <div class="col-lg-11 pl-0">
                    <div class="col-lg-12 pl-0">
                        <span>Narrator</span> 
                        <span class="badge badge-info">Narrator</span>
                    </div>
                    <div class="col-lg-12 pl-0">
                        Author
                    </div>
                </div>
            </div>
        </div>
        <div class="dynamic-message-wrapper">
            <audio src="{{asset('sound/chat_notify.ogg')}}" type="audio/ogg" class="chat_notif"></audio>
            <div class="dynamic-message">
                {{-- append here --}}
            </div>
        </div>
        {{-- typing animation --}}
        <div class="typing-animation">
            {{-- typing animation will appended here --}}
            <div class="justify-content-start">
                <div class="col-12 col-typing-wrapper">
                    <div class="typing-wrapper">
                        <div class="row typing">
                            <p class="typing-time mb-0 font-weight-bold">Typing</p>
                            <div class="dot-pulse"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="dynamic-user-options hide">
                <audio src="{{asset('sound/chat_select.ogg')}}" type="audio/ogg" class="option_popup"></audio>
                <audio src="{{asset('sound/chat_reply.ogg')}}" type="audio/ogg" class="option_reply"></audio>
                <div class="select-option fixed-bottom">
                    <div class="option_message">
                        <ul class="list-group list-group-flush append-options">
                            {{-- <li class="list-group-item"><span class="badge badge-primary">1</span> Option 1</li>
                            <li class="list-group-item"><span class="badge badge-primary">2</span> Option 2</li>
                            <li class="list-group-item"><span class="badge badge-primary">3</span> Option 3</li> --}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

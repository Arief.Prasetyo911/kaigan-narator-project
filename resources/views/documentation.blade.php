@extends('layouts.master')
{{-- @section('styles')
<style>
    @media screen and (max-width: 600px) {
    #expander {
        top: 160px;
    }
    #cssmenu.expanded {
        width: 100%;
    }
    }
</style>
@endsection --}}
@section('main_content')
<div class="documentation">
    <button id="expander" class="btn btn-outline-secondary"><b>Jump to</b> <span class="glyphicon glyphicon-plus"></span></button>
    <div id="cssmenu">
    <ul id="top-menu">
        <li class="scroll-li text-center header">Sections</li>
        <li class="scroll-li"><a href="#json" class="scrollItem">JSON introduction</a></li>
        <li class="scroll-li"><a href="#json-data" class="scrollItem">JSON Data</a></li>
        <li class="scroll-li"><a href="#description" class="scrollItem">Description</a></li>
        <li id="unexpander"><a onclick="return false;"><span class="glyphicon glyphicon-minus"></span> Collapse</a>
        </li>
    </ul>
    </div>

    <div class="container scrollSpy-content">
        <section id="json" name="JSON introduction" class="breakpoint mt-3">
            <h3>#JSON (JavaScript Object Notation)</h3>
            <p>
                <strong>JSON</strong> (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate. It is based on a subset of the JavaScript Programming Language Standard ECMA-262 3rd Edition - December 1999. JSON is a text format that is completely language independent but uses conventions that are familiar to programmers of the C-family of languages, including C, C++, C#, Java, JavaScript, Perl, Python, and many others. These properties make JSON an ideal data-interchange language.
            </p>
            <p>
                JSON is built on two structures:
                <ul>
                    <li>A collection of name/value pairs. In various languages, this is realized as an object, record, struct, dictionary, hash table, keyed list, or associative array.</li>
                    <li>An ordered list of values. In most languages, this is realized as an array, vector, list, or sequence.</li>
                </ul>
            </p>
            <p>
                These are universal data structures. Virtually all modern programming languages support them in one form or another. It makes sense that a data format that is interchangeable with programming languages also be based on these structures.
            </p>
        </section>
        <hr>
        <section id="json-data" name="JSON Data" class="breakpoint mt-3">
            <h3>#JSON Chat Format</h3>
                <p>You can create your own chat conversation in JSON format and display it on this website. You just need to follow our JSON format and your own chat ready to displayed.</p>
                <p>Below is the JSON format we use : </p>
                <pre class="javascript">
    <code>
[
    {
        "filename": "Custom",
        "author": "Editor Test",
        "chat": [
            {
                "id": 1,
                "message": "Hello testing",
                "time_milisecond": 1000
            },
            {
                "id": 2,
                "message": "Test Test",
                "time_milisecond": 1000
            },
            {
                "id": 3,
                "message": "Great. It works",
                "time_milisecond": 1000
            },
            {
                "id": 4,
                "message": "I got a fresh murder case that's way over my head",
                "time_milisecond": 1000
            },
            {
                "id": 5,
                "message": "I need your help",
                "time_milisecond": 1000,
                "options": [
                    {
                        "question_id": 2,
                        "message": "This is above my pay grade..",
                    },
                    {
                        "question_id": 2,
                        "message": "But you're a senior officer",
                    },
                    {
                        "question_id": 2,
                        "message": "Why all the secrecy?",
                    }
                ]
            },
            {
                "id": 6,
                "message": "Well. I'm desperate",
                "time_milisecond": "1000"
            },
            {
                "id": 7,
                "message": "The chief closed the case in less than a week! Said it was an 'open and shut accidental death'.",
                "time_milisecond": "1500"
            }

            ..............
        ]
    }
]
    </code>
                </pre>
        </section>
        <hr>
        <section id="description" name="Description" class="breakpoint mt-3">
            <h3>#Description</h3>
                <table class="mb-0 table">
                    <thead>
                    <tr>
                        <th>Key</th>
                        <th>Type</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>filename</th>
                            <td>string</td>
                            <td>Filename for the JSON file. This will appear on simulator phone on top side</td>
                        </tr>
                        <tr>
                            <th>author</th>
                            <td>string</td>
                            <td>Author or creator JSON file. Will appear as <i><b>player name</b></i></td>
                        </tr>
                        <tr>
                            <th>chat</th>
                            <td>array</td>
                            <td>Contain data that will displayed as chat conversation</td>
                        </tr>
                        <tr>
                            <th>chat.id</th>
                            <td>number</td>
                            <td>Chat ID</td>
                        </tr>
                        <tr>
                            <th>chat.message</th>
                            <td>string</td>
                            <td>The chat message. This will displayed as <i><b>NPC</b></i> chat</td>
                        </tr>
                        <tr>
                            <th>chat.time_milisecond</th>
                            <td>number</td>
                            <td>The delay time before chat appear. Will used how long the <i><b>typing animation</b></i> displayed before the message appear</td>
                        </tr>
                        <tr>
                            <th>options</th>
                            <td>array</td>
                            <td>Contaion options that will be picked player</td>
                        </tr>
                        <tr>
                            <th>options.question_id</th>
                            <td>number</td>
                            <td>Option ID</td>
                        </tr>
                        <tr>
                            <th>options.message</th>
                            <td>string</td>
                            <td>Option message</td>
                        </tr>
                    </tbody>
                </table>
        </section>
    </div>
</div>
@endsection
@section('script')
<script>
    var expander = $('#expander');
    var expanderLeft = expander.attr('left');

    function expandMenu() {
        $('#expander').fadeOut('150');
        $('#cssmenu').animate({
            left: 0
        }, 200).addClass('expanded');
    }

    function unexpandMenu() {
        $('#cssmenu').animate({
            left: -250
        }, 200).removeClass('expanded');
        $('#expander').fadeIn('150');
    }

    $('#unexpander').on('click', function() {
        unexpandMenu();
    });

    expander.on('click', function() {
        expandMenu();
    });

    function realign() {
        var topMenuHeight = $('nav').first().height();
        $('#cssmenu').css('top', topMenuHeight + "px");
    }

    $(document).ready(function() {
        $(window).resize(function() {
            realign();
        })
    });

    $(document).ready(function() {
        var lastId,
            topMenu = $("#top-menu"),
            topMenuHeight = $('nav').first().height(),
            // All list items
            menuItems = topMenu.find("a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function() {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
            });

        // Bind click handler to menu items
        // so we can get a fancy scroll animation
        menuItems.click(function(e) {
            var href = $(this).attr("href");
            var offsetObj = $(href);
            var offsetTop = offsetObj.offset().top;
            var scrollTo = offsetTop - topMenuHeight;
            $('html, body').stop().animate({
            scrollTop: scrollTo
            }, 500);
            e.preventDefault();
        });

        // Bind to scroll

        function spyIt() {
            // Get container scroll position
            var fromTop = $(this).scrollTop() + topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function() {
            if ($(this).offset().top - 5 < fromTop) return this;
            });
            // Get the id of the current element
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems.parent().removeClass("active")
                .end().filter("[href=#" + id + "]").parent().addClass("active");
            }
        }

        $(window).on('scroll resize load', function() {
            spyIt();
        })
    });
</script>
@endsection

@extends('layouts.master')
@section('main_content')
<div class="container all-posts mt-4">
    <h5 class="text-white text-center">
        All User Posts
    </h5>
    <hr>
    <div class="post-data">
        {{-- display xl and lg --}}
        <div class="phone-wrapper">
            <div class="d-none d-lg-block d-xl-block">
                <div class="row">
                @foreach ($postData as $item)
                    <div class="col-lg-2 col-md-4 col-sm-4 col-6 mb-4">
                        <a href="{{route('detailData', [$item->json_id, strtolower(str_replace(' ', '-', $item->title))])}}">
                            <div class="card">
                                <div class="col-lg-12 pl-0 pr-0">
                                    <img class="card-img-top" src="{{asset('images/'.$item->image_name)}}" alt="Card image cap">
                                    <h6 class="card-title mb-0">{{str_replace('_', ' ', $item->title)}}</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 mt-4">
                    <div class="pagination d-flex justify-content-center">
                        {!! $postData->appends(Request::all())->links() !!}
                    </div>
                </div>
            </div>
        </div>

        {{-- diplay MD and SM --}}
        <div class="phone-wrapper">
            <div class="d-none d-md-block d-sm-block d-lg-none">
                <div class="row">
                @foreach ($postData_6 as $item)
                    <div class="col-lg-2 col-md-4 col-sm-4 col-6 mb-4">
                        <a href="{{route('detailData', [$item->json_id, strtolower(str_replace(' ', '-', $item->title))])}}">
                            <div class="card">
                                <div class="col-lg-12 pl-0 pr-0">
                                    <img class="card-img-top" src="{{asset('images/'.$item->image_name)}}" alt="Card image cap">
                                    <h6 class="card-title mb-0">{{str_replace('_', ' ', $item->title)}}</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 mt-4">
                    <div class="pagination d-flex justify-content-center">
                        {!! $postData_6->appends(Request::all())->links() !!}
                    </div>
                </div>
            </div>
        </div>

        {{-- display SM --}}
        <div class="phone-wrapper">
            <div class="d-block d-sm-none">
                <div class="row">
                @foreach ($postData_4 as $item)
                    <div class="col-lg-2 col-md-4 col-sm-4 col-6 mb-4">
                        <a href="{{route('detailData', [$item->json_id, strtolower(str_replace(' ', '-', $item->title))])}}">
                            <div class="card">
                                <div class="col-lg-12 pl-0 pr-0">
                                    <img class="card-img-top" src="{{asset('images/'.$item->image_name)}}" alt="Card image cap">
                                    <h6 class="card-title mb-0">{{str_replace('_', ' ', $item->title)}}</h6>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 mt-4">
                    <div class="pagination d-flex justify-content-center">
                        {!! $postData_4->appends(Request::all())->links() !!}
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="d-block d-sm-none">
            <div class="row h47-vh">
            @foreach ($dataXS as $item)
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 wrapper-phone-height">
                    <a href="{{route('detailData', [$item->author_id, $item->json_name])}}">
                        <img src="{{asset('assets/smartphone-design.png')}}" class="w-100 phone-image" alt="Top Trending">
                        <div class="phone-content">
                            <img src="{{asset('images/'.$item->image_name)}}" alt="JSON Image">
                            <p class="phone-text">
                                {{str_replace('_', ' ', $item->title)}}
                            </p>
                        </div>
                    </a>
                </div>
            @endforeach
            </div>
        </div> --}}
    </div>
</div>
@endsection

@extends('layouts.master')
@section('main_content')
<div class="container carousel-container mt-3">
    <div class="top-trending">
        <p class="text-white">Today's Feature</p>
    </div>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_960_720.jpg" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="https://images.pexels.com/photos/417074/pexels-photo-417074.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="https://images.pexels.com/photos/210186/pexels-photo-210186.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="d-block w-100" alt="...">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</div>
<div class="container content-container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        <hr>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 phone-wrapper">
        <div class="top-trending">
            <p>Top Trending</p>
        </div>
        {{-- display xl and lg --}}
        <div class="d-none d-lg-block d-xl-block">
            <div class="row h47-vh">
            @foreach ($data as $item)
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 wrapper-phone-height">
                    <a href="{{route('detailData', [$item->json_id, strtolower(str_replace(' ', '-', $item->title))])}}">
                        <div class="card">
                            <div class="col-lg-12 pl-0 pr-0">
                            <img class="card-img-top" src="{{asset('images/'.$item->image_name)}}" alt="Card image cap">
                                <h6 class="card-title mb-0">{{str_replace('_', ' ', $item->title)}}</h6>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            </div>
        </div>

        {{-- diplay MD and SM --}}
        <div class="d-none d-md-block d-sm-block d-lg-none">
            <div class="row h47-vh">
            @foreach ($dataMDSM as $item)
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 wrapper-phone-height">
                    <a href="{{route('detailData', [$item->author_id, strtolower(str_replace(' ', '-', $item->title))])}}">
                        <div class="card">
                            <div class="col-md-12 col-sm-12 pl-0 pr-0">
                                <img class="card-img-top" src="{{asset('images/'.$item->image_name)}}" alt="Card image cap">
                                <h6 class="card-title mb-0">{{str_replace('_', ' ', $item->title)}}</h6>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            </div>
        </div>

        {{-- display SM --}}
        <div class="d-block d-sm-none">
            <div class="row h47-vh">
            @foreach ($dataXS as $item)
                <div class="col-lg-2 col-md-4 col-sm-4 col-6 wrapper-phone-height">
                    <a href="{{route('detailData', [$item->author_id, strtolower(str_replace(' ', '-', $item->title))])}}">
                        <div class="card">
                            <div class="col-12 pl-0 pr-0">
                                <img class="card-img-top" src="{{asset('images/'.$item->image_name)}}" alt="Card image cap">
                                <h6 class="card-title mb-0">{{str_replace('_', ' ', $item->title)}}</h6>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            </div>
        </div>

        <div class="d-flex justify-content-center">
            <div class="col-6">
                <a class="nav-link" href="{{route('allPosts')}}">
                    <button class="btn btn-primary btn-block d-lg-none d-xl-none">All Posts</button>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        <hr>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="top-trending">
            <p class="mb-0">How to Write Nareta Stories?</p>
        </div>
        <p class="desc-text">
            Watch our youtube video to understand the basics of writing and uploading stories on Nareta
        </p>
        <div class="d-flex justify-content-center mt-2">
            <div class="col-lg-6 col-md-8 col-sm-12 col-12">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/GpKLYlIExoQ" class="w-100" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('back-end.main.app')
@section('title')
    Upload Audio File
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('upload.audio.send')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>JSON File</label>
                                <select class="custom-select" name="json_file" required>
                                    <option selected value="">- Select JSON File -</option>
                                    @foreach($JSONData as $data)
                                    <option value="{{$data->json_id}}">{{$data->title}}</option>
                                    @endforeach
                                </select>
                                <small id="emailHelp" class="form-text text-muted">Please make sure the selected JSON file have media data. If don't have the uploaded media data will not appear.</small>
                            </div>
                            <div class="form-group">
                                <label>Audio File</label>
                                <div class="custom-file">
                                    <input type="file" name="audioFile" class="custom-file-input form-control" id="audioFile" required>
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div class="col-lg-12 pr-0">
                                <div class="row">
                                    <div class="col-lg-9 col-md-9 col-6"></div>
                                    <div class="col-lg-3 col-md-3 col-6">
                                        <button class="btn btn-block btn-outline-success" type="submit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                All Rights Reserved &copy; {{date('Y')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://kaigangames.com" class="nav-link" target="__blank">
                                Kaigan Games Entertainment SDN BHD
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(()=>{
        $(".table-hover").DataTable();

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    })
</script>
@endsection

@extends('back-end.main.app')
@section('title')
    Upload JSON
@endsection
@section('style')
    <style>
        .hide{
            display: none;
        }
    </style>
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('json.submit')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="title" aria-describedby="title" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Description</label>
                                <textarea class="form-control" id="description" name="description" rows="3" required></textarea>
                              </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group mb-0">
                                        <label>JSON File</label>
                                        <div class="custom-file">
                                            <input type="file" name="jsonfile" class="custom-file-input form-control" id="jsonfile" required>
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group mb-0">
                                        <label for="CarouselImage">Image / Photo (Smartphone Wallpaper)</label>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image" placeholder="Browse" required>
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-6">
                                    <div class="form-group mb-0">
                                        <label for="CarouselImage">Reply Type</label>
                                        <select class="form-control" id="reply_type" name="reply_type" required>
                                            <option selected value="">Please select</option>
                                            <option value="dynamic">Dynamic</option>
                                            <option value="static">Static</option>
                                          </select>
                                    </div>
                                </div>
                                <div class="col-6 have-file hide">
                                    <div class="form-group mb-0">
                                        <label for="CarouselImage">Have Multimedia files?</label>
                                        <select class="form-control" id="have_media" name="have_media">
                                            <option selected value="">Please select</option>
                                            <option value="yes">Yes</option>
                                            <option value="no">No</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-6"></div>
                                <div class="col-lg-6 col-md-6 col-6">
                                    <div class="row">
                                        <div class="col-6">
                                            <button class="btn btn-block btn-outline-danger" type="reset">Reset</button>
                                        </div>
                                        <div class="col-6">
                                            <button class="btn btn-block btn-outline-success" type="submit">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                All Rights Reserved &copy; {{date('Y')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://kaigangames.com" class="nav-link" target="__blank">
                                Kaigan Games Entertainment SDN BHD
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(()=>{
        $(".table-hover").DataTable();

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        $("#reply_type").on("change", function(){
            let selectValue = $("#reply_type").val();
            if(selectValue == "dynamic"){
                $(".have-file").addClass('hide');
            } else {
                $(".have-file").removeClass('hide');
            }
        })
    })
</script>
@endsection

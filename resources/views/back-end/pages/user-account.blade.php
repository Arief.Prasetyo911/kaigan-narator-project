@extends('back-end.main.app')
@section('title')
User Account
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.0.19/sweetalert2.min.css" integrity="sha512-riZwnB8ebhwOVAUlYoILfran/fH0deyunXyJZ+yJGDyU0Y8gsDGtPHn1eh276aNADKgFERecHecJgkzcE9J3Lg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>User Account</h3>
                        <p>In this page you can manage your account on this website. You can deactivate / delete your account here.</p>
                        <form action="{{route('userAccount.delete', Auth::user()->id)}}" method="POST" class="delete-form">
                            @csrf
                            @method('delete')
                            <button class="btn btn-outline-danger btn-block" id="delete-account">
                                <i class="fas fa-trash"></i> Delete Account
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                All Rights Reserved &copy; {{date('Y')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://kaigangames.com" class="nav-link" target="__blank">
                                Kaigan Games Entertainment SDN BHD
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.0.19/sweetalert2.min.js" integrity="sha512-jfVndX8aB+LSJHnrd4BY42vPo2J6NjccZclbhoXEvP8sFnZrj4umbXNHtdoYvacUAN+4X5su5wXjDGAuz5ceog==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(()=>{
        $("#delete-account").click(function(e) {
            e.preventDefault();
            var self = $(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "You can't restore data after this",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, just delete!'
            })
            .then((result) => {
                if (result.value) {
                    self.parents(".delete-form").submit();

                    Swal.fire(
                        'Deleted!',
                        'The data successfully deleted',
                        'success'
                    )
                }
            })
        });
    })
</script>
@endsection

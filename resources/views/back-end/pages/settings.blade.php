@extends('back-end.main.app')
@section('title')
    Settings
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">

        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
            <li class="nav-item">
                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#personal-information">
                    <span>Personal Information</span>
                </a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#password">
                    <span>Password</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane tabs-animation fade show active" id="personal-information" role="tabpanel">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="main-card mb-3 card">
                            <div class="card-header">
                                <i class="header-icon fas fa-id-card"></i>Your Personal Information
                            </div>
                            <div class="card-body">
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input name="name" id="name" type="text" class="form-control personal-information-data" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="examplePassword" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input name="email" id="email" type="email" class="form-control personal-information-data" value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="position-relative row form-check">
                                    <div class="col-sm-10 offset-sm-2">
                                        
                                        <button class="btn btn-outline-primary float-right edit-btn" data-toggle="modal" data-target="#exampleModal">
                                            <i class="far fa-edit"></i> Edit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="main-card mb-3 card update-data" style="display: none;">
                            <div class="card-header">
                                <i class="header-icon fas fa-id-card"></i>Your Personal Information
                            </div>
                            <div class="card-body">
                                <form action="{{route('setting.updatePersonalInfo')}}" method="POST">
                                    @csrf
                                    @method('patch')
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input name="username" id="name" type="text" class="form-control" value="{{$user->name}}">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="examplePassword" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input name="useremail" id="email" type="email" class="form-control" value="{{$user->email}}">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="position-relative row form-group">
                                        <label for="examplePassword" class="col-sm-2 col-form-label">Current Password</label>
                                        <div class="col-sm-10">
                                            <input name="currPassword" id="currPassword" type="password" class="form-control" placeholder="Enter current password">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-check">
                                        <div class="col-sm-10 offset-sm-2">
                                            <button class="btn btn-outline-primary float-right update-btn" type="submit">
                                                <i class="far fa-edit"></i> Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane tabs-animation fade" id="password" role="tabpanel">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="mb-3 main-card card">
                            <div class="card-header">
                                <i class="header-icon fas fa-envelope-open-text"></i> Password
                            </div>
                            <div class="card-body">
                                <div class="position-relative row form-group">
                                    <label for="examplePassword" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input name="pass" id="pass" type="password" class="form-control password-data" value="{{$user->password}}">
                                    </div>
                                </div>
                                <div class="position-relative row form-check">
                                    <div class="col-sm-10 offset-sm-2">
                                        <button class="btn btn-outline-primary float-right edit-password-btn">
                                            <i class="far fa-edit"></i> Edit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="mb-3 main-card card update-password" style="display: none;">
                            <div class="card-header">
                                <i class="header-icon fas fa-envelope-open-text"></i> Password
                            </div>
                            <div class="card-body">
                                <form action="{{route('setting.updatePassword')}}" method="POST">
                                    @csrf
                                    @method('patch')
                                    <div class="position-relative row form-group">
                                        <label for="examplePassword" class="col-sm-2 col-form-label">New Password</label>
                                        <div class="col-sm-10">
                                            <input name="password" id="password" type="password" class="form-control" placeholder="Enter new password here">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="examplePassword" class="col-sm-2 col-form-label">Password Confirmation</label>
                                        <div class="col-sm-10">
                                            <input name="passConfirm" id="passConfirm" type="password" class="form-control" placeholder="Enter password confirmation">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="position-relative row form-group">
                                        <label for="examplePassword" class="col-sm-2 col-form-label">Current Password</label>
                                        <div class="col-sm-10">
                                            <input name="currPassword2" id="currPassword2" type="password" class="form-control" placeholder="Enter current password" required>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-check">
                                        <div class="col-sm-10 offset-sm-2">
                                            <button class="btn btn-outline-primary float-right update-btn" type="submit">
                                                <i class="far fa-edit"></i> Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                Footer Link 1
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                Footer Link 2
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                Footer Link 3
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <div class="badge badge-success mr-1 ml-0">
                                    <small>NEW</small>
                                </div>
                                Footer Link 4
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(()=>{
        $('.personal-information-data, .password-data').prop("disabled", true);

        $(".edit-btn").on('click', function(){
            // $('input[type=text], input[type=email]').prop('disabled', function(i, v) { return !v; });
            $(".update-data").toggle();
        })

        $(".edit-password-btn").on('click', function(){
            // $('input[type=text], input[type=email]').prop('disabled', function(i, v) { return !v; });
            $(".update-password").toggle();
        })
        
    })
</script>
@endsection

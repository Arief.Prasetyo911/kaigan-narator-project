@extends('back-end.main.app')
@section('title')
    JSON Detail
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="position-relative form-group">
                            <label for="exampleText" class="">JSON Content</label>
                            <textarea name="text" id="exampleText" class="form-control" rows="30vh">
                                {{$getContent}}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                All Rights Reserved &copy; {{date('Y')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://kaigangames.com" class="nav-link" target="__blank">
                                Kaigan Games Entertainment SDN BHD
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(()=>{
        $(".table-hover").DataTable();
    })
</script>
@endsection

@extends('back-end.main.app')
@section('title')
    Picture Data
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Author ID</th>
                                        <th>Author</th>
                                        <th>Picture Name</th>
                                        <th>Picture Size</th>
                                        <th>Created At</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1?>
                                    @foreach ($pictureData as $item)
                                    <tr>
                                        <td class="text-center text-muted">{{$no++}}</td>
                                        <td>{{$item->author_id}}</td>
                                        <td>{{$item->author}}</td>
                                        <td>{{$item->picture_name}}</td>
                                        <td>{{$item->picture_size}}</td>
                                        <td>{{$item->created_at->toDayDateTimeString()}}</td>
                                        <td class="text-center">
                                            {{-- <a href="{{route('json.detail', $item->id)}}">
                                                <button type="button" id="PopoverCustomT-1" class="btn btn-primary btn-sm"><i class="fas fa-info-circle"></i> Detail</button>
                                            </a> --}}

                                            <form action="{{route('media.picture.delete', $item->id)}}" method="POST" class="delete-picture-form">
                                                {{ method_field('delete')}}
                                                {{ csrf_field() }}
                                                <button class="btn btn-sm btn-danger delete-picture-btn mt-2" type="button" aria-pressed="true"><i class="fas fa-trash-alt"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-wrapper-footer">
        <div class="app-footer">
            <div class="app-footer__inner">
                <div class="app-footer-left">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                All Rights Reserved &copy; {{date('Y')}}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="app-footer-right">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="https://kaigangames.com" class="nav-link" target="__blank">
                                Kaigan Games Entertainment SDN BHD
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(()=>{
        $(".table-hover").DataTable();

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        $(".delete-picture-btn").click(function(e) {
            e.preventDefault();
            var self = $(this);
            Swal.fire({
                title: 'Are you sure?',
                text: "You can't restore data after this",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, just delete!'
            })
            .then((result) => {
                if (result.value) {
                    self.parents(".delete-picture-form").submit();

                    Swal.fire(
                        'Deleted!',
                        'The data successfully deleted',
                        'success'
                    )
                }
            })
        });
    })
</script>
@endsection

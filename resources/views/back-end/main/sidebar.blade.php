<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="{{route('home')}}" class="{{Request::segment(2) == '' ? 'mm-active':''}}">
                        <i class="metismenu-icon fas fa-campground"></i>
                        Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Chat JSON</li>
                <li class="{{Request::segment(2) == 'my-json' ? 'mm-active':''}} {{Request::segment(2) == 'upload-json' ? 'mm-active':''}}">
                    <a href="#" aria-expanded="true">
                        <i class="metismenu-icon far fa-comment-alt"></i>
                        Your JSON
                        <i class="metismenu-state-icon fas fa-angle-up"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="{{route('json.index')}}" class="{{Request::segment(2) == 'my-json' ? 'mm-active':''}}">
                                <i class="fas fa-stream"></i>&nbsp;
                                JSON List
                            </a>
                        </li>
                        <li>
                            <a href="{{route('json.upload')}}" class="{{Request::segment(2) == 'upload-json' ? 'mm-active':''}}">
                                <i class="fas fa-cloud-upload-alt"></i>&nbsp;
                                Upload JSON
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{Request::segment(4) == 'audio' ? 'mm-active':''}} {{Request::segment(4) == 'video' ? 'mm-active':''}} {{Request::segment(4) == 'picture' ? 'mm-active':''}}">
                    <a href="#" aria-expanded="true">
                        <i class="metismenu-icon far fa-comment-alt"></i>
                        Upload Media
                        <i class="metismenu-state-icon fas fa-angle-up"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="{{route('upload.audio')}}" class="{{Request::segment(4) == 'audio' ? 'mm-active':''}}">
                                <i class="fas fa-file-audio"></i>&nbsp;
                                Audio Upload
                            </a>
                        </li>
                        <li>
                            <a href="{{route('upload.video')}}" class="{{Request::segment(4) == 'video' ? 'mm-active':''}}">
                                <i class="fas fa-file-video"></i>&nbsp;
                                Upload Video
                            </a>
                        </li>
                        <li>
                            <a href="{{route('upload.picture')}}" class="{{Request::segment(4) == 'picture' ? 'mm-active':''}}">
                                <i class="fas fa-file-image"></i>&nbsp;
                                Upload Picture
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{Request::segment(3) == 'audio-data' ? 'mm-active':''}} {{Request::segment(3) == 'video-data' ? 'mm-active':''}} {{Request::segment(3) == 'picture-data' ? 'mm-active':''}}">
                    <a href="#" aria-expanded="true">
                        <i class="metismenu-icon far fa-comment-alt"></i>
                        Media Data
                        <i class="metismenu-state-icon fas fa-angle-up"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="{{route('audio.data')}}" class="{{Request::segment(3) == 'audio-data' ? 'mm-active':''}}">
                                <i class="fas fa-file-audio"></i>&nbsp;
                                Audio Data
                            </a>
                        </li>
                        <li>
                            <a href="{{route('video.data')}}" class="{{Request::segment(3) == 'video-data' ? 'mm-active':''}}">
                                <i class="fas fa-file-video"></i>&nbsp;
                                Video Data
                            </a>
                        </li>
                        <li>
                            <a href="{{route('picture.data')}}" class="{{Request::segment(3) == 'picture-data' ? 'mm-active':''}}">
                                <i class="fas fa-file-image"></i>&nbsp;
                                Picture Data
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

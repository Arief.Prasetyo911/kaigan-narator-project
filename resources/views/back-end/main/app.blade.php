<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Kaigan Games Narrator Dashboard">
    <meta name="msapplication-tap-highlight" content="no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Kaigan Games Narrator - @yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/architectUI.css')}}">
    <link rel="stylesheet" href="{{asset('css/architectUI_var.css')}}">
    @yield('style')
    <script src="https://kit.fontawesome.com/34921e17f0.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.21/cr-1.5.2/kt-2.5.2/sc-2.0.2/sp-1.1.1/datatables.min.css"/>
    @notify_css
</head>
<body>
    <div id="app"></div>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        @include('back-end.main.header')
        @include('back-end.main.ui-setting')
        <div class="app-main">
            @include('back-end.main.sidebar')
            {{-- content --}}
            @yield('content')
        </div>
    </div>
</body>
@notify_js
@notify_render
<script src="{{asset('js/architectUI.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/cr-1.5.2/kt-2.5.2/sc-2.0.2/sp-1.1.1/datatables.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.4/dist/sweetalert2.all.min.js"></script>
@yield('script')
</html>

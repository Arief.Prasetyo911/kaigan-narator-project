-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 03:59 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kaiganeditorweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `audio_files`
--

CREATE TABLE `audio_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `json_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `audio_size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `audio_files`
--

INSERT INTO `audio_files` (`id`, `json_id`, `author_id`, `author`, `audio_name`, `audio_size`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Arief Prasetyo', 'Arief_Prasetyo_file_example_MP3_1MG.mp3', 1087849, '2021-12-05 19:10:03', '2021-12-05 19:10:03'),
(2, 1, 1, 'Arief Prasetyo', 'Arief_Prasetyo_file_example_MP3_1MG.mp3', 1087849, '2021-12-05 20:05:06', '2021-12-05 20:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `json_identifier`
--

CREATE TABLE `json_identifier` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `json_id` int(255) NOT NULL,
  `author_id` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_size` int(11) NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_size` int(11) NOT NULL,
  `have_media` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `counter` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `json_identifier`
--

INSERT INTO `json_identifier` (`id`, `json_id`, `author_id`, `author`, `title`, `description`, `json_name`, `json_size`, `image_name`, `image_size`, `have_media`, `counter`, `color`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Arief Prasetyo', 'First JSON Example', 'This is descrtiption from this JSON file', '1_1_Example.json', 15849, '1_Nareta-Design-Recovered_0001s_0003s_0010_Layer-10.png', 47308, 'yes', 222, 'gold', '2021-11-01 05:20:38', '2021-11-01 05:20:38'),
(2, 2, 1, 'Arief Prasetyo', 'Marguerite DWLA', 'Conversation JSON from Marguerite DWLA', '1_1_Marguerite_DWLA.json', 11183, '1_Nareta-Design-Recovered_0001s_0003s_0012_Layer-8.png', 23259, 'no', 100, 'bronze', '2021-11-01 05:57:26', '2021-11-01 05:57:26'),
(3, 3, 2, 'Arief Prasetyo', 'First JSON Example', 'This is descrtiption from this JSON file', '1_1_Example.json', 15849, '1_Nareta-Design-Recovered_0001s_0003s_0010_Layer-10.png', 47308, 'yes', 55, NULL, '2021-11-01 05:20:38', '2021-11-01 05:20:38'),
(4, 4, 1, 'Arief Prasetyo', 'Marguerite DWLA', 'Conversation JSON from Marguerite DWLA', '1_1_Marguerite_DWLA.json', 11183, '1_Nareta-Design-Recovered_0001s_0003s_0012_Layer-8.png', 23259, 'no', 50, NULL, '2021-11-01 05:57:26', '2021-11-01 05:57:26'),
(5, 5, 2, 'Arief Prasetyo', 'First JSON Example', 'This is descrtiption from this JSON file', '1_1_Example.json', 15849, '1_Nareta-Design-Recovered_0001s_0003s_0010_Layer-10.png', 47308, 'yes', 111, 'silver', '2021-11-01 05:20:38', '2021-11-01 05:20:38'),
(6, 6, 1, 'Arief Prasetyo', 'Marguerite DWLA', 'Conversation JSON from Marguerite DWLA', '1_1_Marguerite_DWLA.json', 11183, '1_Nareta-Design-Recovered_0001s_0003s_0012_Layer-8.png', 23259, 'no', 99, NULL, '2021-11-01 05:57:26', '2021-11-01 05:57:26'),
(7, 7, 2, 'Arief Prasetyo', 'First JSON Example', 'This is descrtiption from this JSON file', '1_1_Example.json', 15849, '1_Nareta-Design-Recovered_0001s_0003s_0010_Layer-10.png', 47308, 'yes', 45, NULL, '2021-11-01 05:20:38', '2021-11-01 05:20:38'),
(8, 8, 1, 'Arief Prasetyo', 'Marguerite DWLA', 'Conversation JSON from Marguerite DWLA', '1_1_Marguerite_DWLA.json', 11183, '1_Nareta-Design-Recovered_0001s_0003s_0012_Layer-8.png', 23259, 'no', 52, NULL, '2021-11-01 05:57:26', '2021-11-01 05:57:26'),
(9, 9, 2, 'Arief Prasetyo', 'Marguerite DWLA', 'Conversation JSON from Marguerite DWLA', '1_1_Marguerite_DWLA.json', 11183, '1_Nareta-Design-Recovered_0001s_0003s_0012_Layer-8.png', 23259, 'no', 95, NULL, '2021-11-01 05:57:26', '2021-11-01 05:57:26'),
(10, 10, 1, 'Arief Prasetyo', 'First JSON Example', 'This is descrtiption from this JSON file', '1_1_Example.json', 15849, '1_Nareta-Design-Recovered_0001s_0003s_0010_Layer-10.png', 47308, 'yes', 35, NULL, '2021-11-01 05:20:38', '2021-11-01 05:20:38'),
(11, 11, 2, 'Arief Prasetyo', 'Marguerite DWLA', 'Conversation JSON from Marguerite DWLA', '1_1_Marguerite_DWLA.json', 11183, '1_Nareta-Design-Recovered_0001s_0003s_0012_Layer-8.png', 23259, 'no', 55, NULL, '2021-11-01 05:57:26', '2021-11-01 05:57:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(33, '2014_10_12_000000_create_users_table', 1),
(34, '2014_10_12_100000_create_password_resets_table', 1),
(35, '2019_08_19_000000_create_failed_jobs_table', 1),
(36, '2021_06_03_022538_create__j_s_o_n_identifier_table', 1),
(37, '2021_07_14_065914_create_settings_table', 1),
(38, '2021_09_07_072826_create_audio_files_table', 1),
(39, '2021_09_07_072951_create_video_files_table', 1),
(40, '2021_09_07_073000_create_picture_files_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `picture_files`
--

CREATE TABLE `picture_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `json_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture_size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `picture_files`
--

INSERT INTO `picture_files` (`id`, `json_id`, `author_id`, `author`, `picture_name`, `picture_size`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Arief Prasetyo', 'Arief_Prasetyo_Arief_Prasetyo_wp-2014-08-milky-way-1023340_1280.jpg', 168677, '2021-12-05 19:11:11', '2021-12-05 19:11:11'),
(2, 1, 1, 'Arief Prasetyo', 'Arief_Prasetyo_Arief_Prasetyo_wp-2014-08-milky-way-1023340_1280.jpg', 168677, '2021-12-05 20:05:53', '2021-12-05 20:05:53');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Arief Prasetyo', 'arief.prasetyo911@gmail.com', NULL, '$2y$10$mZ4UhZLwb4elF1SONZdGB.LPOdZW3dsmP.OKE.7gpLRVdB7G8LJ3S', NULL, '2021-11-01 05:07:08', '2021-11-01 05:07:08'),
(2, 'Arief Budi Prasetyo', 'arief.bepe@gmail.com', NULL, '$2y$10$cG1wwIMXtsPp492N1mlolOjMCcKsPhBUF3VdJ8tTPIw8HT05wSwD2', NULL, '2021-12-21 01:28:50', '2021-12-21 01:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `video_files`
--

CREATE TABLE `video_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `json_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_size` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `video_files`
--

INSERT INTO `video_files` (`id`, `json_id`, `author_id`, `author`, `video_name`, `video_size`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Arief Prasetyo', 'Arief_Prasetyo_file_example_MP4_480_1_5MG.mp4', 1570024, '2021-12-05 20:04:22', '2021-12-05 20:04:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audio_files`
--
ALTER TABLE `audio_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `json_identifier`
--
ALTER TABLE `json_identifier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `picture_files`
--
ALTER TABLE `picture_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video_files`
--
ALTER TABLE `video_files`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audio_files`
--
ALTER TABLE `audio_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `json_identifier`
--
ALTER TABLE `json_identifier`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `picture_files`
--
ALTER TABLE `picture_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `video_files`
--
ALTER TABLE `video_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use PhpParser\Node\Stmt\Enum_;

class CreateJSONIdentifierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('json_identifier', function (Blueprint $table) {
            $table->id();
            $table->integer('json_id');
            $table->integer('author_id');
            $table->string('author');
            $table->string('title');
            $table->text('description');
            $table->string('json_name');
            $table->integer('json_size');
            $table->string('image_name');
            $table->integer('image_size');
            $table->integer('counter')->nullable();
            $table->enum('have_media', ['yes', 'no']);
            $table->enum('reply_type', ['dynamic', 'static']);
            $table->string('color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('json_identifier');
    }
}

<?php

namespace Database\Factories;

use App\Models\JSONBackend;
use Illuminate\Database\Eloquent\Factories\Factory;

class JSONBackendFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JSONBackend::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

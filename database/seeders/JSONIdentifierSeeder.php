<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class JSONIdentifierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('json_identifier')->insert([
            'author_id' => 1,
            'author' => "Arief",
            'title' => "First JSON Example",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra diam ut venenatis posuere. Nullam sed dictum arcu, in egestas orci. Duis quis tristique augue. Quisque lobortis felis ut faucibus maximus. Ut ex lorem, feugiat nec eros et, pharetra sollicitudin sapien. Morbi vulputate est vitae quam dapibus molestie. Fusce efficitur accumsan risus, quis semper risus interdum vitae. Suspendisse sit amet dapibus libero, sed tempor ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque imperdiet libero eu erat laoreet cursus.",
            'cover' => "1_bloody_monday.png",
            'json_name' =>'custom.json',
            'json_size' => 12345,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('json_identifier')->insert([
            'author_id' => 1,
            'author' => "Arief",
            'title' => "First JSON Example",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra diam ut venenatis posuere. Nullam sed dictum arcu, in egestas orci. Duis quis tristique augue. Quisque lobortis felis ut faucibus maximus. Ut ex lorem, feugiat nec eros et, pharetra sollicitudin sapien. Morbi vulputate est vitae quam dapibus molestie. Fusce efficitur accumsan risus, quis semper risus interdum vitae. Suspendisse sit amet dapibus libero, sed tempor ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque imperdiet libero eu erat laoreet cursus.",
            'cover' => "1_bloody_monday.png",
            'json_name' =>'Arya.json',
            'json_size' => 12345,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('json_identifier')->insert([
            'author_id' => 1,
            'author' => "Arief",
            'json_name' =>'Mina.json',
            'json_size' => 12345,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}

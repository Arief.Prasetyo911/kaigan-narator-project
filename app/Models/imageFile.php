<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imageFile extends Model
{
    use HasFactory;

    protected $table = 'picture_files';
    protected $fillable = [
        'json_id',
        'author_id',
        'author',
        'picture_name',
        'picture_size'
    ];
}

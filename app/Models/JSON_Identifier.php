<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JSON_Identifier extends Model
{
    use HasFactory;

    protected $table = 'json_identifier';
    protected $fillable = ['author_id', 'author','json_name', 'json_size'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class audioFile extends Model
{
    use HasFactory;

    protected $table = 'audio_files';
    protected $fillable = [
        'json_id',
        'author_id',
        'author',
        'audio_name',
        'audio_size'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class videoFile extends Model
{
    use HasFactory;

    protected $table = 'video_files';
    protected $fillable = [
        'json_id',
        'author_id',
        'author',
        'video_name',
        'video_size'
    ];
}

<?php

namespace App\Http\Controllers;

use App\Models\audioFile;
use App\Models\imageFile;
use App\Models\videoFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaDataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function audioData(){
        $audioData = audioFile::where('author_id', Auth::user()->id)->get();
        return view('back-end/pages/audioData', compact('audioData'));
    }

    public function videoData(){
        $videoData = videoFile::where('author_id', Auth::user()->id)->get();
        return view('back-end/pages/videoData', compact('videoData'));
    }

    public function pictureData(){
        $pictureData = imageFile::where('author_id', Auth::user()->id)->get();
        return view('back-end/pages/pictureData', compact('pictureData'));
    }
}

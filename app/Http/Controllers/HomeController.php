<?php

namespace App\Http\Controllers;

use App\Models\JSON_Identifier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $setColor = JSON_Identifier::orderBy('counter', 'desc')->take(1)->get();
        foreach($setColor as $gold){
            $firstJsonId = $gold->json_id;

            DB::table('json_identifier')->where('json_id', $firstJsonId)->update(
                [
                    'color' => 'gold'
                ]
            );
        }

        $setColorSilver = JSON_Identifier::orderBy('counter', 'desc')->skip(1)->take(1)->get();
        foreach($setColorSilver as $silver){
            $firstJsonId = $silver->json_id;

            DB::table('json_identifier')->where('json_id', $firstJsonId)->update(
                [
                    'color' => 'silver'
                ]
            );
        }

        $setColorBronze = JSON_Identifier::orderBy('counter', 'desc')->skip(2)->take(1)->get();
        foreach($setColorBronze as $bronze){
            $firstJsonId = $bronze->json_id;

            DB::table('json_identifier')->where('json_id', $firstJsonId)->update(
                [
                    'color' => 'bronze'
                ]
            );
        }

        $count = JSON_Identifier::count();
        $popularStory1  = JSON_Identifier::orderBy('counter', 'desc')->take(3)->get();
        $popularStory4 = JSON_Identifier::orderBy('counter', 'desc')->skip(3)->take(7)->get();
        $popularStory10 = JSON_Identifier::orderBy('counter', 'desc')->skip(10)->take($count)->get();

        $userID = Auth::user()->id;
        $myStory = JSON_Identifier::where('author_id', $userID)->orderBy('counter', 'desc')->get();
        
        return view('home', compact('popularStory1', 'popularStory4', 'popularStory10', 'myStory'));
    }

    public function getFormID(Request $request){
        $select_json = $request->select_json;
        return $select_json;
    }
}

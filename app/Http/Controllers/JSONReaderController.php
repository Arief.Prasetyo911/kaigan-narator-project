<?php

namespace App\Http\Controllers;

use App\Models\audioFile;
use App\Models\imageFile;
use App\Models\JSON_Identifier;
use App\Models\JSONReader;
use App\Models\videoFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Response;


class JSONReaderController extends Controller
{
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    public function index(Request $request)
    {
        $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data = JSON_Identifier::select("id", "json_name")
                    ->where('json_name','LIKE',"%$search%")
            		->get();
        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JSONReader  $jSONReader
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request){
    }

    public function frontPage(){
        $data = JSON_Identifier::orderBy('counter', 'desc')->take(6)->get();
        $dataMDSM = JSON_Identifier::orderBy('counter', 'desc')->take(3)->get();
        $dataXS = JSON_Identifier::orderBy('counter', 'desc')->take(2)->get();
        return view('welcome', compact('data', 'dataMDSM', 'dataXS'));
    }

    public function playJson($json_name){
        $path = public_path().'/data/'.$json_name;
        $json = json_decode(file_get_contents($path), true);
        return response()->json($json);
    }

    public function documentation(){
        return view('documentation');
    }

    public function checkMediaFiles($json_name){
        $getFile = JSON_Identifier::where('json_name', $json_name)->get();
        return response()->json($getFile);
    }

    public function checkMediaAudio($json_id){
        $getAudioFile = audioFile::where('json_id', $json_id)->get();
        return response()->json($getAudioFile);
    }

    public function checkMediaVideo($json_id){
        $getVideoFile = videoFile::where('json_id', $json_id)->first();
        $fileContents = Storage::disk('public')->get("/multimedia/videos/".$getVideoFile->video_name);
        $response = Response::make($fileContents, 200);
        $response->header('Content-Type', "video/mp4");
        return $response;
    }

    public function checkMediaPicture($json_id){
        $getPictureFile = imageFile::where('json_id', $json_id)->get();
        return response()->json($getPictureFile);
    }

    public function getJsonId($json_name){
        $getJsonIDs = JSON_Identifier::where('json_name', $json_name)->get();
        return response()->json($getJsonIDs);
    }

    public function detailData($json_id, $title, Request $request){
        $titleString = str_replace('-', ' ', $title);
        $detailData = JSON_Identifier::where('json_id', $json_id)->where('title', $titleString)->first();
        $carbonDate = Carbon::createFromFormat('Y-m-d H:i:s', $detailData->created_at)->format('d/m/Y');

        // $detailAudio = audioFile::where('json_id', $json_id)->first();
        $detailVideo = videoFile::where('json_id', $json_id)->first();
        // $detailPicture = imageFile::where('json_id', $json_id)->first();

        //increment counter
        DB::table('json_identifier')->where('json_id', $json_id)->where('title', $titleString)->increment('counter');

        if($detailData->reply_type == "dynamic"){
            return view('detail-dynamic', compact('detailData', 'detailVideo', 'carbonDate')); 
        } else {
            return view('detail-static', compact('detailData', 'detailVideo', 'carbonDate')); 
        }
        // return view('detail-page', compact('detailData', 'detailVideo', 'carbonDate'));
    }

    public function allPosts(){
        $postData = JSON_Identifier::paginate(12);
        $postData_6 = JSON_Identifier::paginate(6);
        $postData_4 = JSON_Identifier::paginate(4);
        // $postData_sm = JSON_Identifier::paginate();
        // $postData_xs = ;
        return view('all-posts', compact('postData', 'postData_6', 'postData_4'));
    }

    public function testVideo(){
        return view('video');
    }
}


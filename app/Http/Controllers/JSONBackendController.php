<?php

namespace App\Http\Controllers;

use App\Models\audioFile;
use App\Models\imageFile;
use App\Models\JSON_Identifier;
use App\Models\videoFile;
// use App\Models\JSONBackend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class JSONBackendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_user = Auth::user();
        $json_data = JSON_Identifier::where('author', $auth_user->name)->get();
        return view('back-end.pages.json-list', compact('json_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JSONBackend  $jSONBackend
     * @return \Illuminate\Http\Response
     */
    public function show(JSON_Identifier $JSON_Identifier, $id)
    {
        $detail = JSON_Identifier::where('id', $id)->where('author_id', Auth::user()->id)->first();
        $path = public_path('data/'.$detail->json_name);
        $getContent = file_get_contents($path);
        return view('back-end.pages.json-detail', compact('getContent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JSON_Identifier  $JSON_Identifier
     * @return \Illuminate\Http\Response
     */
    public function edit(JSON_Identifier $JSON_Identifier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JSON_Identifier  $JSON_Identifier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JSON_Identifier $JSON_Identifier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JSON_Identifier  $JSON_Identifier
     * @return \Illuminate\Http\Response
     */
    public function destroy(JSON_Identifier $JSON_Identifier, $id)
    {
        $detail = $JSON_Identifier::where('id', $id)->where('author_id', Auth::user()->id)->first();
        unlink(public_path('data/'.$detail->json_name));
        $detail->delete();

        notify()->success('Successfully delete JSON file', 'Success');
        return back();
    }

    public function uploadJSON(){
        return view('back-end.pages.json-upload');
    }

    public function submitJSON(Request $request){
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'jsonfile' => 'mimetypes:application/json',
            'image' => 'required|mimes:jpg,bmp,png,jpeg,tif',
            'reply_type' => 'required'
        ]);

        // json file
        $jsonfile = $request->jsonfile;
        $filename = str_replace(' ', '_', Auth::user()->id).'_'.$jsonfile->getClientOriginalName();
        $filesize = $jsonfile->getSize();

        //move file
        $jsonfile->move(public_path('data/'), $filename);

        //image / photo
        $imageFileName = $request->image;
        $imageName = str_replace(' ', '_', Auth::user()->id).'_'.$imageFileName->getClientOriginalName();
        $imageSize = $imageFileName->getSize();

        //move file
        $imageFileName->move(public_path('images/'), $imageName);
        
        $reply_type = $request->reply_type;
        $have_media = $request->have_media;

        // insert to database
        $savedata = new JSON_Identifier();

        //check data from database to get json_id value
        $getJSONIDVal = JSON_Identifier::first();
        if($getJSONIDVal){
            $getLastData = JSON_Identifier::all()->last();
            $lastJSONID = $getLastData->json_id;

            //save json id to database with 1 increment
            $savedata->json_id = $lastJSONID+1;
        } else {
            $savedata->json_id = 1;
        }
        
        $savedata->author_id = Auth::user()->id;
        $savedata->author = Auth::user()->name;
        $savedata->title = $request->title;
        $savedata->description = $request->description;
        $savedata->json_name = $filename;
        $savedata->json_size = $filesize;
        $savedata->image_name = $imageName;
        $savedata->image_size = $imageSize;
        $savedata->have_media = $have_media;
        $savedata->reply_type = $reply_type;
        $savedata->counter = 0;
        $savedata->save();

        notify()->success('Successfully add the JSON file', 'Success');
        return back();
    }   

    //audio file

    public function uploadAudio(){
        $authUserID = Auth::user()->id;
        $JSONData = JSON_Identifier::where('author_id', $authUserID)->where('have_media', 'yes')->get();
        // return $JSONData;
        return view('back-end.pages.uploadAudio', compact('JSONData'));
    }

    public function uploadAudio_send(Request $request){
        // $validated = $request->validate([
        //     'audioFile' => 'required|mimes:mpga,wav'
        // ]);

        $audioFile = $request->audioFile;
        $filename  = str_replace(' ', '_', Auth::user()->name).'_'.$audioFile->getClientOriginalName();
        $filesize  = $audioFile->getSize();

        //move file
        $audioFile->move(public_path('multimedia/audio/'), $filename);

        //get json_id from form field
        $json_file = $request->json_file;
        //insert to database
        $saveAudio = new audioFile();
        $saveAudio->json_id = $json_file;
        $saveAudio->author_id = Auth::user()->id;
        $saveAudio->author = Auth::user()->name;
        $saveAudio->audio_name = $filename;
        $saveAudio->audio_size = $filesize;
        $saveAudio->save();

        notify()->success('Successfully add Audio File', 'Success');
        return back();
    }

    public function uploadAudio_delete($id){
        $findData = audioFile::find($id);
        unlink(public_path('multimedia/audio/'.$findData->audio_name));
        $findData->delete();

        notify()->success('Success deleted Audio Data');
        return back();
    }

    //video file

    public function uploadVideo(){
        $authUserID = Auth::user()->id;
        $JSONData = JSON_Identifier::where('author_id', $authUserID)->where('have_media', 'yes')->get();
        return view('back-end.pages.uploadVideo', compact('JSONData'));
    }

    public function uploadVideo_send(Request $request){
        // $validated = $request->validate([
        //     'videoFile' => 'required|size:10240'
        // ]);

        $videoFile = $request->videoFile;
        $filename  = str_replace(' ', '_', Auth::user()->name).'_'.$videoFile->getClientOriginalName();
        $filesize  = $videoFile->getSize();

        //move file
        // $videoFile->move(public_path('multimedia/videos/'), $filename);
        $saveVideo = $request->file('videoFile')->storeAs('public/multimedia/videos/', $filename, 'local');

        //get json_id from form field
        $json_file = $request->json_file;
        //insert to database
        $saveVideo = new videoFile();
        $saveVideo->json_id = $json_file;
        $saveVideo->author_id = Auth::user()->id;
        $saveVideo->author = Auth::user()->name;
        $saveVideo->video_name = $filename;
        $saveVideo->video_size = $filesize;
        $saveVideo->save();

        notify()->success('Successfully add Video File', 'Success');
        return back();
    }


    public function uploadVideo_delete($id){
        $findData = videoFile::find($id);
        unlink(public_path('multimedia/videos/'.$findData->video_name));
        $findData->delete();

        notify()->success('Success deleted Video Data');
        return back();
    }

    //upload image

    public function uploadImage(){
        $authUserID = Auth::user()->id;
        $JSONData = JSON_Identifier::where('author_id', $authUserID)->where('have_media', 'yes')->get();
        return view('back-end.pages.uploadPicture', compact('JSONData'));
    }

    public function uploadImage_send(Request $request){
        $validated = $request->validate([
            'imageFile' => 'required|image|max:5210'
        ]);

        $imageFile = $request->imageFile;
        $filename  = str_replace(' ', '_', Auth::user()->name).'_'.$imageFile->getClientOriginalName();
        $filesize  = $imageFile->getSize();

        //move file
        $imageFile->move(public_path('multimedia/pictures/'), $filename);

        $getJSONID = JSON_Identifier::where('author_id', Auth::user()->id)->where('author', Auth::user()->name)->first();

        //insertto database
        $savePicture = new imageFile();
        $savePicture->json_id = $getJSONID->id;
        $savePicture->author_id = Auth::user()->id;
        $savePicture->author = Auth::user()->name;
        $savePicture->picture_name = $filename;
        $savePicture->picture_size = $filesize;
        $savePicture->save();

        notify()->success('Successfully add Image File', 'Success');
        return back();
    }

    public function uploadImage_delete($id){
        $findData = imageFile::find($id);
        unlink(public_path('multimedia/pictures/'.$findData->picture_name));
        $findData->delete();

        notify()->success('Success deleted Picture Data');
        return back();
    }
}

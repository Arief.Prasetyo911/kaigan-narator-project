<?php

namespace App\Http\Controllers;

use App\Models\setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('back-end.pages.settings', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, setting $setting)
    {
        $validated = $request->validate([
            'username' => 'required|max:50',
            'useremail' => 'required',
        ]);
        
        $currPassword = $request->currPassword;
        if(Hash::check($currPassword, Auth::user()->password)){
            DB::table('users')->where('id', Auth::user()->id)->where('email', Auth::user()->email)->update(
                [
                    'name' => $request->username,
                    'email' => $request->useremail
                ]
            );
    
            notify()->success('Successfully change Personal Information data', 'Success');
            return back();
        }

        notify()->error('Data validation error', 'Error');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(setting $setting)
    {
        //
    }

    public function updatePassword(Request $request){
        $request->validate([
            'password' => 'required',
            'currPassword2' => 'required'
        ]);

        if($request->password == $request->passConfirm){
            if(Hash::check($request->currPassword2, Auth::user()->password)){
                DB::table('users')->where('id', Auth::user()->id)->where('email', Auth::user()->email)->update(
                    [
                        'password' => Hash::make($request->password)
                    ]
                );
        
                notify()->success('Successfully change Password', 'Success');
                return back();
            }
        }

        notify()->error('Data validation error', 'Error');
        return back();
    }

    public function userAccount(){
        return view('back-end.pages.user-account');
    }

    public function userAccountDelete($id){
        $deleteUser = User::find($id);
        $deleteUser->delete();
    }
}

<?php

use App\Http\Controllers\JSONReaderController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [JSONReaderController::class, 'frontPage'])->name('/');
Route::get('/play/{json_id}/{title}', [JSONReaderController::class, 'detailData'])->name('detailData');
Route::get('/get-json', [JSONReaderController::class, 'index'])->name('getJson');
Route::get('/get-json-id/{json_name}', [JSONReaderController::class, 'getJsonId'])->name('getJsonId');
// Route::post('/show-json', [JSONReaderController::class, 'show'])->name('showJson');
Route::get('/play-json/{json_name}', [JSONReaderController::class, 'playJson'])->name('playJson');
Route::get('/check-media-file/{json_name}', [JSONReaderController::class, 'checkMediaFiles'])->name('checkMediaFiles');
Route::get('/check-media-audio/{json_id}', [JSONReaderController::class, 'checkMediaAudio'])->name('checkMediaAudio');
Route::get('/check-media-video/{json_id}', [JSONReaderController::class, 'checkMediaVideo'])->name('checkMediaVideo');
Route::get('/check-media-picture/{json_id}', [JSONReaderController::class, 'checkMediaPicture'])->name('checkMediaPicture');

Route::get('/test-video', [JSONReaderController::class, 'testVideo'])->name('testVideo');

Route::get('/documentation',  [App\Http\Controllers\JSONReaderController::class, 'documentation'])->name('documentation');
Route::get('/all-posts', [App\Http\Controllers\JSONReaderController::class, 'allPosts'])->name('allPosts');

Auth::routes();
Route::middleware(['auth', 'web'])->prefix('home')->group(function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('my-json', [App\Http\Controllers\JSONBackendController::class, 'index'])->name('json.index');
    Route::get('my-json/detail/{id}', [App\Http\Controllers\JSONBackendController::class, 'show'])->name('json.detail');
    Route::delete('my-json/delete/{id}', [App\Http\Controllers\JSONBackendController::class, 'destroy'])->name('json.delete');

    Route::get('media/audio-data', [App\Http\Controllers\MediaDataController::class, 'audioData'])->name('audio.data');
    Route::get('upload/media/audio', [App\Http\Controllers\JSONBackendController::class, 'uploadAudio'])->name('upload.audio');
    Route::post('upload/media/audio/send', [App\Http\Controllers\JSONBackendController::class, 'uploadAudio_send'])->name('upload.audio.send');
    Route::delete('media/audio/delete/{id}', [App\Http\Controllers\JSONBackendController::class, 'uploadAudio_delete'])->name('media.audio.delete');    //belum ada fungsi

    Route::get('media/video-data', [App\Http\Controllers\MediaDataController::class, 'videoData'])->name('video.data');
    Route::get('upload/media/video', [App\Http\Controllers\JSONBackendController::class, 'uploadVideo'])->name('upload.video');
    Route::post('upload/media/video/send', [App\Http\Controllers\JSONBackendController::class, 'uploadVideo_send'])->name('upload.video.send');
    Route::delete('media/video/delete/{id}', [App\Http\Controllers\JSONBackendController::class, 'uploadVideo_delete'])->name('media.video.delete');    //belum ada fungsi

    Route::get('media/picture-data', [App\Http\Controllers\MediaDataController::class, 'pictureData'])->name('picture.data');
    Route::get('upload/media/picture', [App\Http\Controllers\JSONBackendController::class, 'uploadImage'])->name('upload.picture');
    Route::post('upload/media/picture/send', [App\Http\Controllers\JSONBackendController::class, 'uploadImage_send'])->name('upload.picture.send');
    Route::delete('media/picture/delete/{id}', [App\Http\Controllers\JSONBackendController::class, 'uploadImage_delete'])->name('media.picture.delete');    //belum ada fungsi

    Route::get('upload-json', [App\Http\Controllers\JSONBackendController::class, 'uploadJSON'])->name('json.upload');
    Route::post('upload-json/upload', [App\Http\Controllers\JSONBackendController::class, 'submitJSON'])->name('json.submit');

    Route::get('user-account', [App\Http\Controllers\SettingController::class, 'userAccount'])->name('userAccount');
    Route::delete('user-account/delete/{id}', [App\Http\Controllers\SettingController::class, 'userAccountDelete'])->name('userAccount.delete');

    Route::get('settings', [App\Http\Controllers\SettingController::class, 'index'])->name('setting');
    Route::patch('settings/update-personal-information', [App\Http\Controllers\SettingController::class, 'update'])->name('setting.updatePersonalInfo');
    Route::patch('settings/update-password', [App\Http\Controllers\SettingController::class, 'updatePassword'])->name('setting.updatePassword');
});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
